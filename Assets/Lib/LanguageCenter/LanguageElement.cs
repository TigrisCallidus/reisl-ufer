﻿using UnityEngine;
using System.Collections;

namespace GameLab.LanguageCenter
{

public class LanguageElement {

	public LanguageType languageType = LanguageType.DE ; // de/en/fr
	public string key = "text.anonymous"; //  
	public string translation = "translation"; // 

}

}