﻿using UnityEngine;
using System.Collections;
using System;

namespace GameLab.HackAndSlashFramework.UnityExtentions {

    public static class Helper {

        public static bool IsInLayerMask(GameObject obj, LayerMask layerMask) {

            if ((layerMask.value & (1 << obj.layer)) > 0) {
                return true;
            }

            return false;
        }

        public static void Log(GameObject gameObject, string text, string state = "") {
            Debug.Log(gameObject.name + ":" + text + " (state:"+ state + ": frame:" + Time.frameCount + ": time:" + DateTime.UtcNow.Millisecond +")");
        }
    }
}
