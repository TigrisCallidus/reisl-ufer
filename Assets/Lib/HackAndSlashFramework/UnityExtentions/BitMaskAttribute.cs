﻿using UnityEngine;
using System.Collections;

namespace GameLab.HackAndSlashFramework.UnityExtentions {

    public class BitMaskAttribute : PropertyAttribute {
        public System.Type propType;
        public BitMaskAttribute(System.Type aType) {
            propType = aType;
        }
    }
}


