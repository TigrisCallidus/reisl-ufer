﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GameLab.HackAndSlashFramework.UnityExtentions;


namespace GameLab.HackAndSlashFramework {

    [System.Flags]
    public enum AIBehaviour {
        Idle = 1 << 0, // 1
        Walk = 1 << 1, // 2
        Block = 1 << 2, // 4
        Attack = 1 << 3,  // 8
		FightIdle = 1 << 4 // 16 -> Useful when the enemy sees you (is in fight mode) but doesn't do a thing or player is too far away
    }

    [RequireComponent(typeof(NavMeshAgent))]
    public class CharacterEngineAI : MonoBehaviour {

		//[HideInInspector]
		[BitMask(typeof(AIBehaviour))]
		public AIBehaviour behaviour;
        // inspector vars

        [Tooltip("The character is only calculating AI if it has a minimum distance to the camera")]
        public float minCameraDistance = 20;

        [Tooltip("NPC looks at the Character for the first time it comes into the LookRadius")]
        public bool rotateOnDetect = true;

        public bool stareOnTarget = false;
        public bool focusOnOne = false;

        [Tooltip("Defines, if the Character follows the white rabbit")]
        public bool followWhiteRabbit = false;

        [Tooltip("Defines, if the Character follows the enemy")]
		public bool followEnemy = false;

		[Tooltip("Defines, if the Character follows the player")]
		public bool followFriend = false;

        public int hitCooldownMin = 1500;
        public int hitCooldownMax = 3000;
        public AISensors sensors;
		public Transform[] patrolWayPoints;
		public float patrolWaitTime = 1f;
		public bool patrol = false;

		public bool callFriends = false;

		[Tooltip("Maximal number of Friends this npc can call")]
		public int maxNumberOfFriends = 0;

		public BehaviourMode behaviourMode;
		public float minWaitTimeForBehaviourSwitch = 2f;
		
        // private vars
		private float timeSinceBehaviourSwitch;
		private bool readyForSwitch = true;
		private int counter = 0;
        private CharacterEngine character;
        private NavMeshAgent agent;
        private CharacterEngine target;
        private DateTime lastHit;
		private bool helpingOther = false;
		private bool foundOther = false;
        private bool foundRabbit = false;

		// Only for NPC, not NPC_Enemy
		private bool foundPlayer = false;

		// Patrolling vars
		private float patrolTimer;
		private int wayPointIndex;                              

        // cached vars
        private float _targetDistance;
        private int _hitCooldown;
        private Collider[] _hits;
        private Vector3 _targetDir;
        private YieldInstruction _yieldFixedUpdate = new WaitForFixedUpdate();
        private CharacterEngine _targetCharacter;
        private float _tmpDistance;
        private float _distanceToCamera;
        private float _shortestDistance;

		private NearbyCharacters lastCharacters;


        void Start() {

            agent = GetComponentInChildren<NavMeshAgent>();
            character = GetComponent<CharacterEngine>();

            if (character.type == CharacterType.Player1 || character.type == CharacterType.Player2) {
                Debug.LogWarning(gameObject.name+": CharacterEngineAI can only be applied to NPC or NPCEnemy character!");
            }

            character.onCharacterSearch += FindCharacters;

            lastHit = DateTime.UtcNow;

        }

        void OnDisable() {

            if (this.isActiveAndEnabled) {
                this.target = null;

                if (this.agent.isActiveAndEnabled) {
                    this.agent.SetDestination(transform.position);
                }

                StartCoroutine(StopMovement());
            }
        }

	  void Update()
		{
            if (this.character.IsPaused()) {
                return;
            }


			if ((Time.time - timeSinceBehaviourSwitch) > minWaitTimeForBehaviourSwitch)
			{
				readyForSwitch = true;
			}
			if (readyForSwitch)
			{
				//Debug.Log("Behaviour switched!");
				switch (behaviourMode)
				{
					case BehaviourMode.Aggressive:
						AggressiveMode();
						break;

					case BehaviourMode.Defensive:
						DefensiveMode();
						break;

					case BehaviourMode.Passive:
						// Passive behaviour method (Patrolling?)
						break;

					case BehaviourMode.Supportive:
						// Supportive behaviour method
						break;

					case BehaviourMode.Balanced:
						SequentialMode(AIBehaviour.Attack, AIBehaviour.FightIdle, AIBehaviour.Block);
						break;

					case BehaviourMode.AttackBlock:
						SequentialMode(AIBehaviour.Attack, AIBehaviour.Block);
						break;

					case BehaviourMode.PartiallyAttack:
						SequentialMode(AIBehaviour.Attack, AIBehaviour.FightIdle);
						break;
				}
			}
		}

      void FixedUpdate() {

        _distanceToCamera = FastMath.Distance(transform.position, this.character.GetCameraPos());

        if (_distanceToCamera > this.minCameraDistance) {
            this.character.SetPause(true);
        } else {

            if (this.character.IsPaused()) {
                this.character.SetPause(false);
            }
        }

        // stop if Character is not NPC
        if (character.IsNPC() == false || !enabled || character.IsPaused()) {
            return;
        }

        DoAIBehaviour();
      }

        private void DoAIBehaviour()
        {

            if (this.target != null)
            {
                if (this.target.gameObject.GetComponent<WhiteRabbit>() != null)
                {
                    _targetDistance = FastMath.Distance(transform.position, this.target.transform.position);
                    if (_targetDistance > 3) { 
                    FollowMode();
                } else
                    {
                        agent.SetDestination(transform.position);
                        character.Move(Vector3.zero, false, false);
                    }
                }
                else {

                    _targetDistance = FastMath.Distance(transform.position, this.target.transform.position);

                    if (!foundPlayer)
                    {

                        // blocking behaviour
                        if ((behaviour & AIBehaviour.Block) == AIBehaviour.Block)
                        {
                            DoBlockBehaivour();
                        }

                        /*
                                  if ((behaviour & (AIBehaviour.Attack | AIBehaviour.Block)) == (AIBehaviour.Attack | AIBehaviour.Block)) {

                                  }*/

                        // attacking behaviour
                        if ((behaviour & AIBehaviour.Attack) == AIBehaviour.Attack)
                        {
                            character.ReleaseActionBlock();
                            DoAttackBehaivour();
                        }

                        if ((behaviour & AIBehaviour.FightIdle) == AIBehaviour.FightIdle)
                        {
                            character.ReleaseActionBlock();
                            DoFightIdleBehaivour();
                        }

                    }
                    else {
                        FollowMode();
                    }
                }

            }
            else {

                // release block
                if ((behaviour & AIBehaviour.Block) == AIBehaviour.Block)
                {
                    character.ReleaseActionBlock();
                }

                // release attack
                if ((behaviour & AIBehaviour.Attack) == AIBehaviour.Attack)
                {
                    character.ReleaseActionAttackSlow();
                }

                character.Move(Vector3.zero, false, false);

                if (patrol)
                {
                    Patrol();
                }
            }

        }

		public void SwitchBehaviourMode(BehaviourMode mode)
		{
			behaviourMode = mode;
		}

		public CharacterEngine GetCharacterEngine()
		{
			return character;
		}

		public void SetTarget(CharacterEngine targetCharacter)
		{
			if (!helpingOther && !foundOther && !foundRabbit)
			{
				helpingOther = true;
				_targetCharacter = targetCharacter;
				StartCoroutine(FirstContact(_targetCharacter));
			}
		}

		private void FollowMode()
		{
			// Should we use another radius?
			if (_targetDistance >= this.sensors.followDistanceToPlayer)
			{
				agent.SetDestination(target.transform.position);
				character.Move(agent.desiredVelocity, false, false);
			}
			else
			{
				agent.SetDestination(transform.position);
				character.Move(Vector3.zero, false, false);
			}
		}

		private void AggressiveMode()
		{
			behaviour = AIBehaviour.Attack;
			timeSinceBehaviourSwitch = Time.time;
			readyForSwitch = false;
		}

		private void DefensiveMode()
		{
			behaviour = AIBehaviour.Block;
			timeSinceBehaviourSwitch = Time.time;
			readyForSwitch = false;
		}

		private void SupportiveMode()
		{

		}

		private void SequentialMode(params AIBehaviour[] desiredBehaviour)
		{
			behaviour = desiredBehaviour[counter];

			counter = (counter < desiredBehaviour.Length - 1) ? ++counter : 0;

			timeSinceBehaviourSwitch = Time.time;
			readyForSwitch = false;
		}

		private void AlternateMode(params AIBehaviour[] desiredBehaviours)
		{
			int randomIndex = UnityEngine.Random.Range(0, desiredBehaviours.Length);
			behaviour = desiredBehaviours[randomIndex];

			timeSinceBehaviourSwitch = Time.time;
			readyForSwitch = false;
		}

      private void DoBlockBehaivour()
      {
        _targetDir = FastMath.Normalize(this.target.transform.position - transform.position) * 0.00001f;

        if (this.stareOnTarget) {
          character.Rotate(_targetDir);
        }

        if (_targetDistance <= this.sensors.blockRadius) {
          character.ActionBlock();
        } else {
          character.ReleaseActionBlock();
        }
      }

      private void DoFightIdleBehaivour()
      {
        _targetDir = FastMath.Normalize(this.target.transform.position - transform.position) * 0.00001f;

        if (this.stareOnTarget) {
          character.Rotate(_targetDir);
        }
      }

      private void DoAttackBehaivour()
	  {
		_targetDir = FastMath.Normalize(this.target.transform.position - transform.position) * 0.00001f;
        if (_targetDistance <= this.sensors.attackRadius) {
	
          _hitCooldown = UnityEngine.Random.Range(this.hitCooldownMin, this.hitCooldownMax);

          if ((DateTime.UtcNow - lastHit).TotalMilliseconds > _hitCooldown) {

            character.ActionAttackSlow();
            lastHit = DateTime.UtcNow;

          } else {
            character.ReleaseActionAttackSlow();
          }

		  if (this.stareOnTarget)
		  {
			  character.Rotate(_targetDir);
		  }

          agent.SetDestination(transform.position);
          character.Move(Vector3.zero, false, false);

        } else {

          character.ReleaseActionAttackSlow();

          if (character.state == CharacterState.Grounded) {
            if (followEnemy) {
              agent.SetDestination(target.transform.position);
              character.Move(agent.desiredVelocity, false, false);
            }
          }
        }
    }

		void Patrol()
		{

            if (this.patrolWayPoints.Length > 0) {

                //agent.speed = patrolSpeed;

                if (agent.remainingDistance < agent.stoppingDistance) {
                    patrolTimer += Time.deltaTime;

                    if (patrolTimer >= patrolWaitTime) {
                        if (wayPointIndex == patrolWayPoints.Length - 1) {
                            wayPointIndex = 0;
                        } else {
                            wayPointIndex++;
                        }

                        patrolTimer = 0;
                    }
                } else {
                    patrolTimer = 0;
                }

                agent.SetDestination(patrolWayPoints[wayPointIndex].position);
                character.Move(agent.desiredVelocity, false, false);

            }
		}

        void OnDrawGizmos() {

            if (enabled) {
                Gizmos.color = this.sensors.blockColor;
                Gizmos.DrawWireSphere(transform.position + Vector3.up, this.sensors.blockRadius);
                Gizmos.color = this.sensors.attackColor;
                Gizmos.DrawWireSphere(transform.position + Vector3.up, this.sensors.attackRadius);
            }
        }

        void FindCharacters(NearbyCharacters characters)
        {
            lastCharacters = characters;

            if (followWhiteRabbit && characters.whiteRabbit != null)
            {
                foundRabbit = true;
                StartCoroutine(FirstContact(characters.whiteRabbit));

            }
            else {
                foundRabbit = false;

                if (characters.enemies.Count == 0 && characters.friends.Count == 0)
                {
                    foundOther = false;
                    if (!helpingOther && !foundPlayer)
                    {
                        this.target = null;
                        this.agent.SetDestination(transform.position);
                    }

                }
                else if (characters.enemies.Count > 0)
                {

                    // choose the one with the shortest distance
                    helpingOther = false;
                    foundPlayer = false;
                    foundOther = true;

                    if (this.focusOnOne)
                    {
                        _targetCharacter = characters.enemies[0];

                    }
                    else {

                        // check shortest distance
                        _shortestDistance = float.MaxValue;

                        for (int i = 0; i < characters.enemies.Count; i++)
                        {

                            _tmpDistance = (this.transform.position - characters.enemies[i].transform.position).sqrMagnitude;

                            if (_shortestDistance > _tmpDistance)
                            {

                                _shortestDistance = _tmpDistance;
                                _targetCharacter = characters.enemies[i];
                            }
                        }

                    }
                    if (callFriends)
                    {
                        int maxCount = (characters.friendsAI.Count < maxNumberOfFriends) ? characters.friendsAI.Count : maxNumberOfFriends;
                        for (int i = 0; i < maxCount; i++)
                        {
                            characters.friendsAI[i].SetTarget(_targetCharacter);
                        }
                    }

                    if (this.target != _targetCharacter)
                    {
                        StartCoroutine(FirstContact(_targetCharacter));
                    }

                }
                else
                {
                    if (character.type == CharacterType.NPC && characters.friends.Count >= 1 && followFriend && !foundPlayer)
                    {
                        for (int i = 0; i < characters.friends.Count; i++)
                        {
                            if (characters.friends[i].type == CharacterType.Player1 || characters.friends[i].type == CharacterType.Player2)
                            {
                                _targetCharacter = characters.friends[i];
                            }

                            StartCoroutine(FirstContact(_targetCharacter));
                            foundPlayer = true;
                            StartCoroutine(CheckForPlayer());
                        }
                    }

                }
            }
        }

        IEnumerator FirstContact(CharacterEngine foreignCharacter) {

            if (foreignCharacter != null) {
                if (this.rotateOnDetect) {
                    yield return StartCoroutine(LookAtTarget(foreignCharacter.transform.position));
                }

                this.target = foreignCharacter;
            }
            
            yield return null;
        }

        IEnumerator LookAtTarget(Vector3 target, int samples = 20) {

            _targetDir = (target - transform.position).normalized * 0.00001f;

            for (int i = 0; i < samples; i++) {
                character.Rotate(_targetDir);
                yield return _yieldFixedUpdate;
            }
        }

        IEnumerator StopMovement() {

            int i = 0;

            while(i < 20){

                character.Move(Vector3.zero);
                i++;
                yield return _yieldFixedUpdate;
            }
        }

		IEnumerator CheckForPlayer() {


			yield return new WaitForSeconds(5.0f);
			if (lastCharacters != null) {
				foundPlayer = false; 
				FindCharacters (lastCharacters);
			}
		}

    }
}