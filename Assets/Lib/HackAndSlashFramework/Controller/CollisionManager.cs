﻿using UnityEngine;
using System.Collections;
using System;
using GameLab.HackAndSlashFramework.UnityExtentions;

namespace GameLab.HackAndSlashFramework {

    [System.Serializable]
    public class CollisionManager {

        // inspector vars
        public LayerMask weaponMask;
        public LayerMask blockMask;
        public LayerMask bodyMask;
        public LayerMask itemMask;
        public Collider blockCollider;

        // cached vars
        private Collider _characterCollider;
        private Collider _enemyCollider;
        private CharacterEngine _enemy;


        public void OnCollisionEnter(Collision collision, CharacterEngine character) {

            // declare colliders
            _characterCollider = collision.contacts[0].thisCollider;
            _enemyCollider = collision.contacts[0].otherCollider;

            // check if character hitted with weapon
            if (Helper.IsInLayerMask(_characterCollider.gameObject, this.weaponMask)) {

                _enemy = collision.gameObject.GetComponent<CharacterEngine>();

                if (_enemy != null && character.friendlyFire == false) {

                    // players & npcs are not fighting against players & npcs
                    if (character.type != CharacterType.NPCEnemy && _enemy.type != CharacterType.NPCEnemy) {
                        return;
                    }

                    // enemy is not fighting against enemy
                    if (character.type == CharacterType.NPCEnemy && _enemy.type == CharacterType.NPCEnemy) {
                        return;
                    }
                }
               
                // add collider to CharacterAttack object
                character.currentAttack.SetCollision(collision);

                // check if hitted a body
                if (Helper.IsInLayerMask(_enemyCollider.gameObject, this.bodyMask)) {

                    // check if character didn't get hit already and if weapon hitted allowed maxHitPerAttack value
                    if (character.GetState() != CharacterState.Recovering && character.currentAttack.AllowedToHit()) {

                        character.currentAttack.AddHit(1);
                        character.OnCharacterHitBody(_enemy);
                    }

                } else {

                    // check if hitted a blocking object
                    if (Helper.IsInLayerMask(_enemyCollider.gameObject, this.blockMask)) {

                        if (character.GetState() != CharacterState.Recovering && character.currentAttack.AllowedToHit()) {
                            character.currentAttack.AddHit(1);
                            character.OnCharacterHitBlock(_enemy);
                        }
                    }

                    // check if hitted a weapon
                    if (Helper.IsInLayerMask(_enemyCollider.gameObject, this.weaponMask)) {

                        if (character.GetState() != CharacterState.Recovering && character.currentAttack.AllowedToHit()) {
                            character.currentAttack.AddHit(1);
                            character.OnCharacterHitWeapon(character.currentAttack);
                        }
                    }

                }

                // check if hitted an item
                if (Helper.IsInLayerMask(_enemyCollider.gameObject, this.itemMask)) {

                    if (character.GetState() != CharacterState.Recovering && character.currentAttack.AllowedToHit()) {
                        character.currentAttack.AddHit(1);
                        character.OnCharacterHitItem(character.currentAttack);
                    }
                }


            }

            // check if character got hitted by weapon
            if (Helper.IsInLayerMask(_enemyCollider.gameObject, this.weaponMask)) {

                // check if character body got hitted
                if (Helper.IsInLayerMask(_characterCollider.gameObject, this.bodyMask)) {
                    //Debug.Log(gameObject.name + ": I've got hitted by a WEAPON");
                }
            }
        }
    }
}