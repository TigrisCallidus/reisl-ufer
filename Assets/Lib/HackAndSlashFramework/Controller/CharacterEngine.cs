﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using GameLab.HackAndSlashFramework.UnityExtentions;

namespace GameLab.HackAndSlashFramework {

    public enum AnimationState {
        AttackSlow,
        AttackFast,
        Block,
        BlockStart,
        BlockEnd,
        Backstep,
        Damage,
        Grounded
    }

    public enum CharacterState {
        Grounded,
        Striking,
        Recovering,
        Blocking
    }

    public enum CharacterType {
        Player1,
        Player2,
        NPC,
        NPCEnemy,
        WhiteRabbit
    }

    public delegate void OnCharacterDamage(CharacterAttack attack);
	public delegate void OnImmovableDamage(ImmovableAttack attack);
    public delegate void OnCharacterHitBody(CharacterAttack attack);
    public delegate void OnCharacterHitWeapon(CharacterAttack attack);
    public delegate void OnCharacterHitBlock(CharacterAttack attack);
    public delegate void OnCharacterHitItem(CharacterAttack attack);
    public delegate void OnCharacterDefend(CharacterAttack attack);
    public delegate void OnCharacterSearch(NearbyCharacters characters);
	public delegate void OnCharacterWeaponSwitch();
	public delegate void OnCharacterActionAttackSlow(Weapon attackingWeapon);
	public delegate void OnCharacterActionAttackFast(Weapon attackingWeapon);
	public delegate void OnCharacterBlock();
    public delegate void OnCharacterSlide(Vector3 destination);
	public delegate void OnCharacterBackStep(Vector3 distance);
	public delegate void OnCharacterGodRage();
    public delegate void OnCharacterActivate();
    public delegate void OnUsePotion();


    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class CharacterEngine : MonoBehaviour {

        #region var declaration

        // constants
        public const string ANIMATION_PARAMETER_ATTACK_SPEED = "VarAttackSpeed";
        public const string ANIMATION_PARAMETER_DAMAGE_SPEED = "VarDamageSpeed";

        // inspector vars
        public CharacterType type;
        public CharacterState state;
        public bool friendlyFire = false;

		[Tooltip("Sets a delay when hitting something before returning the attack.")]
		public float delayOnAttackHit=0.05f;
		[Tooltip("Can the character switch his weapon sets.")]
		public bool canSwitchWeapons=true;

        public float baseAttackSpeed = 1f;
        public float baseAttackDamage = 1f;
        public float slideDuration = 0.15f;
        public float slideDistance = 0.5f;
        public float slideAngle = 45f;
		public float backStepDistance = 2.0f;
		public float backStepDuration = 0.15f;
        public WeaponManager weaponManager;
        public CharacterSensors sensors;
        public CollisionManager collisionManager;

        // public vars
        public CharacterAttack currentAttack = new CharacterAttack();
        public InputHandler controls;

        // events
        public event OnCharacterHitBody onCharacterHitBody;
        public event OnCharacterHitBlock onCharacterHitBlock;
        public event OnCharacterHitWeapon onCharacterHitWeapon;
        public event OnCharacterHitItem onCharacterHitItem;
        public event OnCharacterDamage onCharacterDamage;
		public event OnImmovableDamage onImmovableDamage;
        public event OnCharacterDefend onCharacterDefend;
        public event OnCharacterSearch onCharacterSearch;
		public event OnCharacterWeaponSwitch onCharacterWeaponSwitch;
		public event OnCharacterActionAttackSlow onCharacterActionAttackSlow;
		public event OnCharacterActionAttackFast onCharacterActionAttackFast;
		public event OnCharacterBlock onCharacterBlock;
        public event OnCharacterSlide onCharacterSlide;
		public event OnCharacterBackStep onCharacterBackStep;
		public event OnCharacterGodRage onCharacterGodRage;
        public event OnCharacterActivate onCharacterActivate;
        public event OnUsePotion onUsePotion;


        // private vars
        private bool pauseCharacter;
        private NearbyCharacters nearbyCharacters;
        private Animator animator;
        private int currentAnimationLayer;
        private ThirdPersonCharacter characterController;
        private Transform cam;
        private float h;
        private float v;

        // cached vars
        private Vector3 _camForwardDir;
        private Vector3 _move;
        private Vector3 _helpVector = new Vector3(1, 0, 1);
        private Collider[] _searchHits;
        private CharacterEngine _foreignCharacter;
		private CharacterEngineAI _foreignCharacterAI;
        private WhiteRabbit _foreignWhiteRabbit;
        private bool _started;
        private Rigidbody _rigidbody;
        private bool _sliding = false;
		private bool _backStepping = false;
        private bool _inTransition = false;

        #endregion

		public enum ButtonSequence {
			YBUTTON,
			ABUTTON,
			XBUTTON,
			BBUTTON,
			NONE
		}
		
		public ButtonSequence currentSeq;

        #region Monobehaviour Methods

        protected virtual void Awake() {
			currentSeq = ButtonSequence.NONE;

            characterController = GetComponent<ThirdPersonCharacter>();
            animator = GetComponent<Animator>();

            _rigidbody = GetComponent<Rigidbody>();

            if (Camera.main != null) {
                cam = Camera.main.transform;
            } else {
                Debug.LogWarning(gameObject.name + ": Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.");
            }
        }

        protected virtual void Start() {

            // set deviceid
            if (this.type == CharacterType.Player1) {
                this.controls = new InputHandler(0);
            }

            if (this.type == CharacterType.Player2) {
                this.controls = new InputHandler(1);
            }

            nearbyCharacters = new NearbyCharacters(this.type);

            // set animation layer weight to 1 on start
            if (type != CharacterType.WhiteRabbit)
            {
                this.animator.SetLayerWeight(this.weaponManager.GetCurrent().layerIndex, 1);
            }

            StartCoroutine(CharacterSearch());
        }
        protected virtual void Update() {

            if (this.pauseCharacter) {
                return;
            }

            // get current state from mecanim animator
            this.state = this.GetState();

            // always clear attack states if grounded
            if (this.state == CharacterState.Grounded) {

                this.currentAttack.Clear();
                this.ResetMass();
                this.RemoveConstraints(RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ);
            }
            
            // leave if no player controls character
            if (IsNPC() || type==CharacterType.WhiteRabbit ) {
                return;
            }

            ProcessInput();
        }



        protected virtual void FixedUpdate() {

            if (this.pauseCharacter) {
                return;
            }

            // update weapon manager
            this.weaponManager.Update(this);

            if (IsNPC()) {
                return;
            }

            // calculate move direction to pass to character
            if (cam != null) {
                // calculate camera relative direction to move:
                _camForwardDir = Vector3.Scale(cam.forward, _helpVector).normalized;
                _move = v * _camForwardDir + h * cam.right;
            } else {
                // we use world-relative directions in the case of no main camera
                _move = v * Vector3.forward + h * Vector3.right;
            }

            if (this.state == CharacterState.Blocking) {
                this.Rotate(_move);
            }

            this.Move(_move, false, false);  

        }

        void OnCollisionEnter(Collision collision) {

            // forward collision to collisionManager
            this.collisionManager.OnCollisionEnter(collision, this);
        }

        void OnDrawGizmos() {

            if (enabled) {
                Gizmos.color = this.sensors.lookColor;
                Gizmos.DrawWireSphere(transform.position, this.sensors.lookRadius);
            }
        }

        #endregion

        #region State Handling

        public bool IsNPC() {

            if (this.type == CharacterType.NPC || this.type == CharacterType.NPCEnemy) {
                return true;
            }

            return false;
        }

        public bool IsInWeaponState(bool ignoreTransition = false) {

            if (this.IsInState(AnimationState.AttackFast.ToString(), ignoreTransition) || this.IsInState(AnimationState.AttackSlow.ToString(), ignoreTransition)) {
                return true;
            }

            return false;
        }

        public bool IsInTransition() {
            return animator.IsInTransition(this.weaponManager.GetCurrent().layerIndex);
        }

        private bool IsInState(string state, bool ignoreTransition = false) {

            if (ignoreTransition == false && this.IsInTransition()) {
                return animator.GetNextAnimatorStateInfo(this.weaponManager.GetCurrent().layerIndex).IsName(state);
            }

            return animator.GetCurrentAnimatorStateInfo(this.weaponManager.GetCurrent().layerIndex).IsName(state);
        }

        public CharacterState GetState(bool ignoreTransition = false) {

            // convert Mecanim states to PlayerState
            if (!this.IsInState("Grounded", ignoreTransition)) {

                if (this.IsInWeaponState(ignoreTransition)) {
                    return CharacterState.Striking;
                }

                if (this.IsInState(AnimationState.Block.ToString(), ignoreTransition) || this.IsInState(AnimationState.BlockStart.ToString(), ignoreTransition) || this.IsInState(AnimationState.BlockEnd.ToString(), ignoreTransition)) {
                    return CharacterState.Blocking;
                }

                if (this.IsInState(AnimationState.Damage.ToString(), ignoreTransition)) {
                    return CharacterState.Recovering;
                }

            }

            return CharacterState.Grounded;
        }

        IEnumerator SetState(string name, bool state, float time) {
            yield return new WaitForSeconds(time);
            animator.SetBool(name, state);
        }

        public NearbyCharacters GetNearbyCharacters()
        {
          return this.nearbyCharacters;
        }

        private void ProcessInput() {

            _inTransition = this.IsInTransition();

            if (this.state == CharacterState.Grounded && this.controls.GetAttackFast() && _inTransition == false) {
                this.ActionAttackFast();

            } else {
                this.ReleaseActionAttackFast();
            }

            if (this.state == CharacterState.Grounded && this.controls.GetAttackSlow() && _inTransition == false) {
                this.ActionAttackSlow();
            } else {
                this.ReleaseActionAttackSlow();
            }

			if (this.state == CharacterState.Grounded && this.controls.GetSwitchWeaponSet() && _inTransition == false && canSwitchWeapons) {
                this.ActionSwitchWeapon();
            }

			if ((this.state == CharacterState.Grounded || this.state == CharacterState.Recovering) && this.controls.GetBackStep()) {
				this.ActionBackStep();

			} else if ((this.state == CharacterState.Grounded || this.state == CharacterState.Recovering) && this.controls.GetSlideLeft()) {
                this.ActionSlideLeft();

			} else if ((this.state == CharacterState.Grounded || this.state == CharacterState.Recovering) && this.controls.GetSlideRight()) {
                this.ActionSlideRight();
                    
            } else {
                this.ReleaseActionBackStep();
            }

            // manage blocking
            if (this.controls.GetBlock()) {
                this.ActionBlock();
            } else {
                this.ReleaseActionBlock();
            }

			//Manage manual God-Rage Trigger
			//Is now handled via Button Combination
			if (this.controls.GetGodRage ()) {
				this.ActivateGodRage();
			}

            // Manually pressing on Shops
            if (this.controls.GetActivate())
            {
                this.ActivateActivate();
            }

            if (this.controls.GetUsePotion())
            {
                this.UsePotion();
            }


            // read walk inputs if player idles
            if (this.state != CharacterState.Striking && this.state != CharacterState.Recovering && _sliding == false) {
                h = this.controls.GetHorizontal();
                v = this.controls.GetVertical();
            }

			//GodRage Sequence checker
            /*
			if (currentSeq == ButtonSequence.NONE && this.controls.GetSwitchWeaponSet ()) {
				currentSeq = ButtonSequence.YBUTTON;
			} else if (currentSeq != ButtonSequence.NONE && this.controls.GetSwitchWeaponSet ()) {
				currentSeq = ButtonSequence.NONE;
			}
			
			if (currentSeq == ButtonSequence.YBUTTON && this.controls.GetAttackFast ()) {
				currentSeq = ButtonSequence.ABUTTON;
			} else if (currentSeq != ButtonSequence.YBUTTON && this.controls.GetAttackFast ()) {
				currentSeq = ButtonSequence.NONE;
			}

			if (currentSeq == ButtonSequence.ABUTTON && this.controls.GetBackStep ()) {
				currentSeq = ButtonSequence.XBUTTON;
			} else if (currentSeq != ButtonSequence.ABUTTON && this.controls.GetBackStep ()) {
				currentSeq = ButtonSequence.NONE;
			}

			if (currentSeq == ButtonSequence.XBUTTON && this.controls.GetAttackSlow ()) {
				this.ActivateGodRage();
				currentSeq = ButtonSequence.NONE;
			}else if (currentSeq != ButtonSequence.XBUTTON && this.controls.GetAttackSlow ()) {
				currentSeq = ButtonSequence.NONE;
			}
			
            */
        }

        IEnumerator CharacterSearch() {

            if (this.sensors.lookFrequency < 0.016f) {
                Debug.LogError(gameObject.name + "lookFrequency for the sensor can't be less than 0.016f!");
                yield break;
            }

            YieldInstruction _loopYield = new WaitForSeconds(this.sensors.lookFrequency);

            // start character search after a random time bettwen 0-1s to have a better seed
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.0f, 1.0f));

            while (true) {

                nearbyCharacters.Clear();

                if (enabled && this.IsPaused() == false) {

                    _searchHits = Physics.OverlapSphere(transform.position, this.sensors.lookRadius, this.sensors.characterLayerMask);

                    for (int i = 0; i < _searchHits.Length; i++) {

                        if (_searchHits[i].gameObject != this.gameObject)
                        {
                           // print(_searchHits[i].name);
                            _foreignWhiteRabbit = _searchHits[i].GetComponent<WhiteRabbit>();
                            if (_foreignWhiteRabbit != null)
                            {
                                nearbyCharacters.whiteRabbit=_foreignWhiteRabbit;
                            }
                            else {
                                _foreignCharacter = _searchHits[i].GetComponent<CharacterEngine>();
                                _foreignCharacterAI = null;


                                if (_foreignCharacter != null)
                                {
                                    if (_foreignCharacter.type == CharacterType.NPC || _foreignCharacter.type == CharacterType.NPCEnemy)
                                    {
                                        _foreignCharacterAI = _searchHits[i].GetComponent<CharacterEngineAI>();
                                    }

                                    if (this.sensors.visualContact)
                                    {

                                        /*
                                        Vector3 dir = FastMath.Normalize((_foreignCharacter.transform.position + Vector3.up) - (transform.position + Vector3.up));
                                        Debug.DrawRay(transform.position + Vector3.up, dir, Color.red, 0.25f);
                                        */

                                        if (!Physics.Linecast(transform.position + Vector3.up, _foreignCharacter.transform.position + Vector3.up, this.sensors.nonCharacterLayerMask))
                                        {
                                            nearbyCharacters.Add(_foreignCharacter);
                                            if (_foreignCharacterAI != null)
                                            {
                                                nearbyCharacters.Add(_foreignCharacterAI);
                                            }
                                        }

                                    }
                                    else {
                                        nearbyCharacters.Add(_foreignCharacter);
                                        if (_foreignCharacterAI != null)
                                        {
                                            nearbyCharacters.Add(_foreignCharacterAI);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (this.onCharacterSearch != null) {
                    this.onCharacterSearch(nearbyCharacters);
                }

                yield return _loopYield;
            }

        }

        public Vector3 GetCameraPos() {
            return cam.position;
        }

        public Animator GetAnimator() {
            return this.animator;
        }

        public void SetPause(bool state) {

            this.animator.enabled = !state;
            this.pauseCharacter = state;
        }

        public bool IsPaused() {
            return this.pauseCharacter;
        }

        #endregion

        #region Character Events

        public void OnCharacterHitBody(CharacterEngine enemy) {

            if (this.state == CharacterState.Recovering) {
                return;
            }

            //Helper.Log(gameObject, "hit body / hit:" + attack.GetHits(), this.state.ToString());

            if (this.onCharacterHitBody != null) {
                this.onCharacterHitBody(this.currentAttack);
            }

            // trigger OnCharacterDamage on enemy
			enemy.OnCharacterDamage(this.currentAttack);

            // stop attack animation if it is allowed
            StartCoroutine(StopAttackAnimation(this.currentAttack, IgnoreAttackStop.OnCharacterHitBody));
        }

        public void OnCharacterHitBlock(CharacterEngine enemy) {

            //Helper.Log(gameObject, "hit block / hit:" + attack.GetHits(), this.state.ToString());

            if (this.onCharacterHitBlock != null) {
                this.onCharacterHitBlock(this.currentAttack);
            }

            // trigger OnCharacterDefend on enemy
            StartCoroutine(enemy.OnCharacterDefend(this.currentAttack));

            // stop attack animation if it is allowed
            StartCoroutine(StopAttackAnimation(this.currentAttack, IgnoreAttackStop.OnCharacterHitBlock));
        }

        public void OnCharacterHitWeapon(CharacterAttack attack) {

            //Helper.Log(gameObject, "hit weapon / hit:" + attack.GetHits(), this.state.ToString());

            if (this.onCharacterHitWeapon != null) {
                this.onCharacterHitWeapon(this.currentAttack);
            }

            if (this.state == CharacterState.Recovering) {
                return;
            }

            float waitTime = 0f;

            if (attack.GetWeapon().maxHitsPerAttack > 1) {
                waitTime = 0.1f;
            }
            
            // stop attack animation if it is allowed
            StartCoroutine(StopAttackAnimation(this.currentAttack, IgnoreAttackStop.OnCharacterHitWeapon, waitTime));
        }

        public void OnCharacterHitItem(CharacterAttack attack) {

            //Helper.Log(gameObject, "hit item", this.state.ToString());

            if (this.onCharacterHitItem != null) {
                this.onCharacterHitItem(this.currentAttack);
            }

            // check if collision is IItem
            IItem item = attack.GetCollision().gameObject.GetComponent<IItem>();
            if (item != null) {
                item.OnCharacterHit(attack);
            }


            // stop attack animation if it is allowed
            StartCoroutine(StopAttackAnimation(this.currentAttack, IgnoreAttackStop.OnCharacterHitItem));
        }


		public void OnCharacterWeaponSwitch(){
			this.onCharacterWeaponSwitch ();
		}

        private IEnumerator StopAttackAnimation(CharacterAttack attack, IgnoreAttackStop ignoreElement, float waitTime = 0f) {

            if (_started) {
                yield break;
            }

            _started = true;

            if (waitTime != 0f) {
                yield return new WaitForSeconds(waitTime);
            }

            // check if attack animation stop is ignored for this event
            if ((this.weaponManager.ignoreAttackStop & ignoreElement) != ignoreElement) {

                // reset attack animation if weapon allows this
                if (attack.GetWeapon().stopAttackOnHit) {

					yield return new WaitForSeconds(delayOnAttackHit);
                    if (this.state != CharacterState.Recovering && this.state != CharacterState.Grounded && attack.IsRunning()) {
                        //Helper.Log(gameObject, "stop attack animation", this.state.ToString());
                        this.animator.CrossFadeInFixedTime(AnimationState.Grounded.ToString(), attack.GetWeapon().stopAttackSpeed, this.weaponManager.GetCurrent().layerIndex);
                    }
                }
            }

            _started = false;
        }
		

        public void OnCharacterDamage(CharacterAttack attack) {

            //Helper.Log(gameObject, "got hitted / hit:"+attack.GetHits(), this.state.ToString());

            this.state = CharacterState.Recovering;

            // trigger and release damage animation
            this.animator.SetFloat(ANIMATION_PARAMETER_DAMAGE_SPEED, attack.GetWeapon().damageRecoveringSpeed);
            this.animator.SetBool(AnimationState.Damage.ToString(), true);
            StartCoroutine(SetState(AnimationState.Damage.ToString(), false, 0.3f));

            if (this.onCharacterDamage != null) {
                this.onCharacterDamage(attack);
            }
        }

		public void OnImmovableDamage (ImmovableAttack attack)
		{

			//Helper.Log(gameObject, "got hitted / hit:"+attack.GetHits(), this.state.ToString());

			this.state = CharacterState.Recovering;

			// trigger and release damage animation
			this.animator.SetFloat(ANIMATION_PARAMETER_DAMAGE_SPEED, attack.GetImmovableDamageDealer().damageRecoveringSpeed);
			this.animator.SetBool(AnimationState.Damage.ToString(), true);
			StartCoroutine(SetState(AnimationState.Damage.ToString(), false, 0.3f));

			if (this.onImmovableDamage != null)
			{
				this.onImmovableDamage(attack);
			}
		}

        public IEnumerator OnCharacterDefend(CharacterAttack attack) {

            yield return new WaitForSeconds(0.2f);

            if (this.GetState() != CharacterState.Recovering) {

                //Helper.Log(gameObject, "defended attack / hit:" + attack.GetHits(), this.state.ToString());

                if (this.onCharacterDefend != null) {
                    this.onCharacterDefend(attack);
                }
            }
        }

        #endregion

        #region Character Actions

        public void ActionBlock() {


            this.SetMass(100);
            this.AddConstraints(RigidbodyConstraints.FreezePositionY);

            animator.SetBool(AnimationState.Block.ToString(), true);

            if (this.state == CharacterState.Blocking && !this.collisionManager.blockCollider.enabled) {
                this.collisionManager.blockCollider.enabled = true;

                if (onCharacterBlock != null) {
                    OnCharacterBlock();
                }
            }
        }

        public void ReleaseActionBlock() {

            animator.SetBool(AnimationState.Block.ToString(), false);

            if (this.collisionManager.blockCollider.enabled) {
                this.collisionManager.blockCollider.enabled = false;
            }
        }

        public void ActionAttackSlow() {

            this.AddConstraints(RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ);

            if (weaponManager.GetCurrent().slowWeapon != null) {

                animator.SetFloat(ANIMATION_PARAMETER_ATTACK_SPEED, this.baseAttackSpeed * weaponManager.GetCurrent().slowWeapon.attackSpeed);
                animator.SetBool(AnimationState.AttackSlow.ToString(), true);

                this.currentAttack.Run(this, weaponManager.GetCurrent().slowWeapon, weaponManager.GetCurrent().slowWeapon.name+".heavy");
				OnCharacterActionAttackSlow(weaponManager.GetCurrent().slowWeapon);
            }

        }

        public void ReleaseActionAttackSlow() {

            if (weaponManager.GetCurrent().slowWeapon != null) {
                animator.SetBool(AnimationState.AttackSlow.ToString(), false);
            }

        }

		public void OnCharacterActionAttackSlow(Weapon attackingWeapon){

            if (this.onCharacterActionAttackSlow != null) {
                this.onCharacterActionAttackSlow(attackingWeapon);
            }

		}

        public void ActionAttackFast() {

            this.AddConstraints(RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ);

            if (weaponManager.GetCurrent().fastWeapon != null) {
                animator.SetFloat(ANIMATION_PARAMETER_ATTACK_SPEED, this.baseAttackSpeed * weaponManager.GetCurrent().fastWeapon.attackSpeed);
                animator.SetBool(AnimationState.AttackFast.ToString(), true);
                this.currentAttack.Run(this, weaponManager.GetCurrent().fastWeapon, weaponManager.GetCurrent().slowWeapon.name + ".light");
				OnCharacterActionAttackFast(weaponManager.GetCurrent().fastWeapon);
            }
        }

        public void ReleaseActionAttackFast() {

            if (weaponManager.GetCurrent().fastWeapon != null) {
                animator.SetBool(AnimationState.AttackFast.ToString(), false);
            }

        }

        public void OnCharacterActionAttackFast(Weapon attackingWeapon) {
            if (this.onCharacterActionAttackFast != null) {
                this.onCharacterActionAttackFast(attackingWeapon);
            }
        }

        public void ActionSwitchWeapon() {

            // set current animation layer to 0
            this.animator.SetLayerWeight(this.weaponManager.GetCurrent().layerIndex, 0);

            this.weaponManager.SwitchSet();
            OnCharacterWeaponSwitch();

            // set new animation layer to 1
            this.animator.SetLayerWeight(this.weaponManager.GetCurrent().layerIndex, 1);
        }

        public void ActionSlideLeft(bool left = true) {

            if (_sliding == false && _rigidbody.velocity.sqrMagnitude < 0.05f) {
                
                Vector3 _destination;
                float _slideAngle = this.slideAngle;

                if (left) {
                    _destination = transform.position - (transform.right * this.slideDistance);
                } else {
                    _destination = transform.position + (transform.right * this.slideDistance);
                    _slideAngle = -_slideAngle;
                }

                animator.SetBool(AnimationState.Backstep.ToString(), true);

                StartCoroutine(ISlide(_destination, this.slideDuration, _slideAngle));

                if (this.onCharacterSlide != null) {
                    this.onCharacterSlide(_destination);
                }
            }

        }

		public void ActionBackStep()
		{

			if (_backStepping == false && _rigidbody.velocity.sqrMagnitude < 5f)
			{
				Vector3 _distance = Vector3.Scale(-transform.forward, new Vector3(backStepDistance, backStepDistance, backStepDistance));

                animator.SetBool(AnimationState.Backstep.ToString(), true);

				StartCoroutine(IBackStep(_distance, this.backStepDuration));

				if (this.onCharacterBackStep != null)
				{
					this.onCharacterBackStep(_distance);
				}
			}
		}

        public void ReleaseActionBackStep() {
            animator.SetBool(AnimationState.Backstep.ToString(), false);
        }

		public void ActivateGodRage() {

			if (this.onCharacterGodRage != null) {
				this.onCharacterGodRage();
			}
		}

        public void ActivateActivate()
        {
            if (this.onCharacterActivate != null)
            {
                this.onCharacterActivate();
            }
        }

        public void UsePotion()
        {
            if (this.onUsePotion != null)
            {
                this.onUsePotion();
            }
        }

        public void ActionSlideRight() {
            this.ActionSlideLeft(false);
        }


        private IEnumerator ISlide(Vector3 target, float duration, float angle) {

            _sliding = true;

            Vector3 _startPos = transform.position;
            Quaternion _startRot = transform.rotation;
            Quaternion _targetRot = transform.rotation * Quaternion.Euler(0f, angle, 0f);

            if (!Physics.CheckCapsule(_startPos + Vector3.up, target + Vector3.up, 0.5f, this.sensors.nonCharacterLayerMask)) {
                for (float t = 0f; t < 1f; t += Time.deltaTime / duration) {

                    _rigidbody.MovePosition(Vector3.Lerp(_startPos, target, t));
                    _rigidbody.MoveRotation(Quaternion.Slerp(_startRot, _targetRot, t));

                    yield return null;
                }
            }

            _sliding = false;
        }

		private IEnumerator IBackStep(Vector3 distance, float duration)
		{
			_backStepping = true;

			Vector3 _startPos = transform.position;
			Vector3 _endPos = _startPos + distance;

            if (!Physics.CheckCapsule(_startPos + Vector3.up, _endPos + Vector3.up, 0.5f, this.sensors.nonCharacterLayerMask)) {

                for (float t = 0f; t < 1f; t += Time.deltaTime / duration) {
                    _rigidbody.MovePosition(Vector3.Lerp(_startPos, _endPos, t));

                    yield return null;
                }
            }

			_backStepping = false;
		}

		public void OnCharacterBlock(){
			this.onCharacterBlock ();
		}

        public void Move(Vector3 dir, bool crouch = false, bool jump = false) {

            if (this.state != CharacterState.Grounded) {
                return;
            }

            if (dir == Vector3.zero && _rigidbody.velocity == Vector3.zero) {
                return;
            } 

            if (characterController != null) {
                if(this.type == CharacterType.Player1)
                {
                    characterController.Move2(dir, crouch, jump);
                } else
                {
                    characterController.Move(dir, crouch, jump);
                }
            }
        }

        public void Rotate(Vector3 dir) {

            if (characterController != null) {
                characterController.Move(dir * 0.00001f, false, false);
            }

        }

        public void AddConstraints(RigidbodyConstraints constraint) {
            characterController.AddConstraints(constraint);
        }

        public void RemoveConstraints(RigidbodyConstraints constraint) {
            characterController.RemoveConstraints(constraint);
        }

        public void SetMass(float mass) {
            characterController.SetMass(mass);
        }

        public void ResetMass() {
            characterController.ResetMass();
        }

        #endregion

    }
}
