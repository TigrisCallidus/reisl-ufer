﻿using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework.UnityExtentions;

namespace GameLab.HackAndSlashFramework {

    public enum WeaponSets {
        Set1,
        Set2
    }

    [System.Flags]
    public enum IgnoreAttackStop {
        OnCharacterHitBody = 1 << 0,
        OnCharacterHitBlock = 1 << 1,
        OnCharacterHitWeapon = 1 << 2,
        OnCharacterHitItem = 1 << 3
    }

    [System.Serializable]
    public class WeaponManager {

        public WeaponSets startWeaponSet;
        [BitMask(typeof(IgnoreAttackStop))]
        public IgnoreAttackStop ignoreAttackStop;
        public WeaponSet weaponSet1;
        public WeaponSet weaponSet2;


        private bool initDone = false;

        public void Init() {

            if (!initDone) {
                initDone = true;

                if (startWeaponSet == WeaponSets.Set1) {
                    weaponSet1.SetActive(true);
                    weaponSet2.SetActive(false);
                } else {
                    weaponSet1.SetActive(false);
                    weaponSet2.SetActive(true);
                }
            }

        }


        public void Update(CharacterEngine character) {

            this.Init();

            if (character.state == CharacterState.Striking) {

                if (this.GetCurrent().fastWeapon != null) {
                    this.GetCurrent().fastWeapon.GetCollider().isTrigger = false;
                }

                if (this.GetCurrent().slowWeapon != null) {
                    this.GetCurrent().slowWeapon.GetCollider().isTrigger = false;
                }

            } else {

                if (GetCurrent().fastWeapon != null) {
                    this.GetCurrent().fastWeapon.GetCollider().isTrigger = true;
                }

                if (this.GetCurrent().slowWeapon != null) {
                    this.GetCurrent().slowWeapon.GetCollider().isTrigger = true;
                }
            }
        }

        public WeaponSet GetCurrent() {

            if (startWeaponSet == WeaponSets.Set1) {
                return weaponSet1;
            } else {
                return weaponSet2;
            }
        }

        public void SwitchSet() {

            if (startWeaponSet == WeaponSets.Set1) {
                startWeaponSet = WeaponSets.Set2;
                weaponSet1.SetActive(false);
                weaponSet2.SetActive(true);
            } else {
                startWeaponSet = WeaponSets.Set1;
                weaponSet1.SetActive(true);
                weaponSet2.SetActive(false);
            }
        }

      public void UnparentWeapons(float autoDestoryTime = 60)
      {
        Rigidbody r;
        BoxCollider c;

        GetCurrent().slowWeapon.transform.parent = null;
        if (GetCurrent().slowWeapon.gameObject.GetComponent<Rigidbody>() == null){
          r = GetCurrent().slowWeapon.gameObject.AddComponent<Rigidbody>();
        } else {
          r = GetCurrent().slowWeapon.gameObject.GetComponent<Rigidbody>();
        }
        r.useGravity = true;

        c = GetCurrent().slowWeapon.GetComponent<BoxCollider>();
        c.isTrigger = false;
        AutoDestroy autoDestroy = GetCurrent().slowWeapon.gameObject.AddComponent<AutoDestroy>();
        autoDestroy.secToDestroy = autoDestoryTime;

        GetCurrent().fastWeapon.transform.parent = null;
        if (GetCurrent().fastWeapon.gameObject.GetComponent<Rigidbody>() == null)
        {
          r = GetCurrent().fastWeapon.gameObject.AddComponent<Rigidbody>();
        } else {
          r = GetCurrent().fastWeapon.gameObject.GetComponent<Rigidbody>();
        }
        r.useGravity = true;

        c = GetCurrent().fastWeapon.GetComponent<BoxCollider>();
        c.isTrigger = false;

        autoDestroy = GetCurrent().fastWeapon.gameObject.AddComponent<AutoDestroy>();
        autoDestroy.secToDestroy = autoDestoryTime;
    }

  }
}