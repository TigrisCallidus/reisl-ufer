﻿using UnityEngine;
using System.Collections;

namespace GameLab.HackAndSlashFramework {

    [System.Serializable]
    public class CharacterSensors {
        public float lookFrequency = 0.1f;
        public float lookRadius = 10f;
        public bool visualContact = false;
        public Color lookColor;
        public LayerMask characterLayerMask;
        public LayerMask nonCharacterLayerMask;
    }
}
