﻿using UnityEngine;
using System.Collections;

namespace GameLab.HackAndSlashFramework {

    [System.Serializable]
    public class WeaponSet {

        public int layerIndex;
        public Weapon slowWeapon;
        public Weapon fastWeapon;

        public void SetActive(bool state) {

            if (slowWeapon != null) {
                slowWeapon.gameObject.SetActive(state);
            }

            if (fastWeapon != null) {
                fastWeapon.gameObject.SetActive(state);
            }

        }

    }
}
