﻿using UnityEngine;
using System.Collections;

namespace GameLab.HackAndSlashFramework {

    public interface IItem {

        void OnCharacterHit(CharacterAttack attack);

    }
}
