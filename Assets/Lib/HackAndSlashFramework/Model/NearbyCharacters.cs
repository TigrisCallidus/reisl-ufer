﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GameLab.HackAndSlashFramework {

    public class NearbyCharacters {

        public List<CharacterEngine> friends = new List<CharacterEngine>();
		public List<CharacterEngineAI> friendsAI = new List<CharacterEngineAI>();
        public List<CharacterEngine> enemies = new List<CharacterEngine>();
        public WhiteRabbit whiteRabbit;

        private CharacterType myType;

        public NearbyCharacters(CharacterType type) {
            myType = type;
        }

        public void Clear() {
            this.friends.Clear();
            this.enemies.Clear();
            this.whiteRabbit = null;
        }

        public void Add(CharacterEngine character) {

            // handle stuff if I am player or NPC
            if (myType != CharacterType.NPCEnemy){

                if(character.type == CharacterType.NPCEnemy){
                    enemies.Add(character);
                } else {
                    friends.Add(character);
                }
            }
            
            // handle stuff if I am enemy
            if(myType == CharacterType.NPCEnemy){
                
                if(character.type != CharacterType.NPCEnemy) {
                    enemies.Add(character);
                } else {
                    friends.Add(character);
                }

            }

        }

		public void Add(CharacterEngineAI character)
		{
			if (myType != CharacterType.NPCEnemy && character.GetCharacterEngine() != null && character.GetCharacterEngine().type == CharacterType.NPC)
			{
				friendsAI.Add(character);
			}

			if (myType == CharacterType.NPCEnemy && character.GetCharacterEngine() != null && character.GetCharacterEngine().type == CharacterType.NPCEnemy)
			{
				friendsAI.Add(character);
			}
		}

    }

}
