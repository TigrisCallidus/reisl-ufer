﻿using UnityEngine;
using System.Collections;
using System;

namespace GameLab.HackAndSlashFramework {
    
    public struct CharacterAttack {

        private CharacterEngine character;
        private bool isRunning;
        private string name;
        //private DateTime start;
        private float start;
        private int hits;
        private Weapon weapon;
        private Weapon lastWeapon;
        private Collision collision;

        public bool IsRunning() {
            return this.isRunning;
        }

        public CharacterEngine GetCharacter() {
            return this.character;
        }

        public Weapon GetWeapon() {
            return this.weapon;
        }

        public Weapon GetLastWeapon() {
            return this.lastWeapon;
        }

        public float GetStart() {
            return this.start;
        }

        // returns milliseconds since start
        public int GetDuration() {
            //Debug.Log("Duration " + ((Time.time - this.start) * 1000));
            return (int)((Time.time - this.start) * 1000);
        }

        public string GetName() {
            return this.name;
        }

        public float GetDamage() {
            return this.weapon.damagePerHit * character.baseAttackDamage;
        }

        public void Run(CharacterEngine character, Weapon weapon, string attackName) {
            this.isRunning = true;
            this.character = character;
            this.weapon = weapon;
            this.start = Time.time;
            this.name = attackName;
        }

        public void Clear() {
            this.isRunning = false;
            this.name = "";
            this.hits = 0;
            this.weapon = null;
            this.lastWeapon = this.weapon;
            this.collision = null;
        }

        public void SetCollision(Collision collision) {
            this.collision = collision;
        }

        public Collision GetCollision() {
            return this.collision;
        }

        public void AddHit(int amount) {
            this.hits += amount;
        }

        public int GetHits() {
            return this.hits;
        }

        public bool AllowedToHit() {

            if (this.weapon == null) {
                return false;
            }

            // check if minimumAttackTime is over
            if (this.weapon.minimumAttackTime > this.GetDuration()) {
                return false;
            }

            // check if player didn't reach maxHitsPerAttack
            if (this.hits < this.weapon.maxHitsPerAttack) {
                return true;
            }

            return false;
        }
    }
}
