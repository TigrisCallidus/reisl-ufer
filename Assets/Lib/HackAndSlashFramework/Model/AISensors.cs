﻿using UnityEngine;
using System.Collections;

namespace GameLab.HackAndSlashFramework {

    [System.Serializable]
    public class AISensors {

        public float blockRadius = 2f;
        public Color blockColor;
		[Tooltip("Distance it keeps to the player when following him")]
		public float followDistanceToPlayer = 2f;
        public float attackRadius = 0.8f;
        public Color attackColor;
        public LayerMask characterLayerMask;
    }
}
