﻿using UnityEngine;
using System.Collections;
using System;
using GameLab.HackAndSlashFramework;

public class WhiteRabbit : CharacterEngine {

    public GameObject target;
    public float speed=1;

    //protected override void Awake()
    //{
        
    //}

    protected override void FixedUpdate()
    {
        if (target != null)
        {
            Vector3 tmp = target.transform.position - this.transform.position;
            tmp = tmp * speed / 300;
            this.transform.position += tmp;
        }
    }

    //protected override void Start()
    //{

    //}

    protected override void Update()
    {

    }
}
