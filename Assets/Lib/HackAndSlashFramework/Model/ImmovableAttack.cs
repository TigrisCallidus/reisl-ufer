﻿using UnityEngine;
using System.Collections;
using System;

namespace GameLab.HackAndSlashFramework {
    
    public struct ImmovableAttack {

		ImmovableDamageDealer immovableDamageDealer;
        private bool isRunning;
        private string name;
        //private DateTime start;
        private float start;
        private int hits;
        private Collision collision;

        public bool IsRunning() {
            return this.isRunning;
        }

		public ImmovableDamageDealer GetImmovableDamageDealer()
		{
            return this.immovableDamageDealer;
        }

        public float GetStart() {
            return this.start;
        }

        // returns milliseconds since start
        public int GetDuration() {
            //Debug.Log("Duration " + ((Time.time - this.start) * 1000));
            return (int)((Time.time - this.start) * 1000);
        }

        public string GetName() {
            return this.name;
        }

        public float GetDamage() {
            return this.immovableDamageDealer.Damage;
        }

		public void Run(ImmovableDamageDealer immovableDamageDealer)
		{
            this.isRunning = true;
			this.immovableDamageDealer = immovableDamageDealer;
            this.start = Time.time;
        }

        public void Clear() {
            this.isRunning = false;
            this.name = "";
            this.hits = 0;
            this.collision = null;
        }

        public void SetCollision(Collision collision) {
            this.collision = collision;
        }

        public Collision GetCollision() {
            return this.collision;
        }

        public void AddHit(int amount) {
            this.hits += amount;
        }

        public int GetHits() {
            return this.hits;
        }

        public bool AllowedToHit() {

			return true;
        }
    }
}
