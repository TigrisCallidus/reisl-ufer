﻿using UnityEngine;
using System.Collections;
using Xft;

namespace GameLab.HackAndSlashFramework {

    public class Weapon : MonoBehaviour {

        //public string name;
        public float attackSpeed = 1f;
        public bool stopAttackOnHit = true;
        public int minimumAttackTime = 250;
        [Range(0.0f, 1.0f)]
        public float stopAttackSpeed = 0.5f;
        public float damagePerHit = 5;
        public float damageRecoveringSpeed = 1f;
        public int maxHitsPerAttack = 1;
        public Transform handBone;
        private Collider _collider;
		private bool showingTrail = false;
		public XWeaponTrail trail;

        void Start() {
            _collider = GetComponent<Collider>();
            this.AttachToHand();
			if (trail != null) {
				trail.Deactivate ();
			}
        }

        public void AttachToHand() {
            this.transform.parent = handBone;
        }

        public Collider GetCollider() {
            return _collider;
        }

		void Update(){
			if (GetComponent<BoxCollider> ().isTrigger == true) {
				if (showingTrail == true) {
					if (trail != null) {
						trail.StopSmoothly (0.5f);
						showingTrail = false;
					}
				}
			} else {
				if (showingTrail == false) {
					if (trail != null) {
						trail.Activate();
						showingTrail = true;
					}
				}
			}
		}

    }

}
