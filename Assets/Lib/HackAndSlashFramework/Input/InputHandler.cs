﻿using UnityEngine;
using System.Collections;
using InControl;

namespace GameLab.HackAndSlashFramework {

    public class DefaultCharacterControls : PlayerActionSet {

        public PlayerAction Left;
        public PlayerAction Right;
        public PlayerAction Up;
        public PlayerAction Down;
        public PlayerAction Jump;
        public PlayerOneAxisAction Vertical;
        public PlayerOneAxisAction Horizontal;

        public PlayerAction AttackSlow;
        public PlayerAction AttackFast;
        public PlayerAction Block;
        public PlayerAction SwitchWeaponSet;
        public PlayerAction SlideLeft;
        public PlayerAction SlideRight;
		public PlayerAction BackStep;
		public PlayerAction GodRage;
        public PlayerAction Activate;
        public PlayerAction UsePotion;

		public PlayerAction StartPause;

		
		public DefaultCharacterControls() {

            Left = CreatePlayerAction("Left");
            Right = CreatePlayerAction("Right");
            Up = CreatePlayerAction("Up");
            Down = CreatePlayerAction("Down");
            Vertical = CreateOneAxisPlayerAction(Down, Up);
            Horizontal = CreateOneAxisPlayerAction(Left, Right);
            Jump = CreatePlayerAction("Jump");

            AttackSlow = CreatePlayerAction("AttackSlow");
            AttackFast = CreatePlayerAction("AttackFast");
            Block = CreatePlayerAction("Block");
            Activate = CreatePlayerAction("Activate");
            UsePotion = CreatePlayerAction("UsePotion");
            SwitchWeaponSet = CreatePlayerAction("SwitchWeaponSet");
            SlideLeft = CreatePlayerAction("SlideLeft");
            SlideRight = CreatePlayerAction("SlideRight");
			BackStep = CreatePlayerAction("BackStep");
			GodRage = CreatePlayerAction("GodRage");

			StartPause = CreatePlayerAction("StartPause");

		}
    }

    public class InputHandler {

        public DefaultCharacterControls controller;
        private int deviceId;

        public InputHandler(int deviceId) {

            controller = new DefaultCharacterControls();

            controller.Up.AddDefaultBinding(Key.W);
            controller.Up.AddDefaultBinding(Key.UpArrow);
            controller.Up.AddDefaultBinding(InputControlType.LeftStickUp);

            controller.Left.AddDefaultBinding(Key.A);
            controller.Left.AddDefaultBinding(Key.LeftArrow);
            controller.Left.AddDefaultBinding(InputControlType.LeftStickLeft);

            controller.Down.AddDefaultBinding(Key.S);
            controller.Down.AddDefaultBinding(Key.DownArrow);
            controller.Down.AddDefaultBinding(InputControlType.LeftStickDown);

            controller.Right.AddDefaultBinding(Key.D);
            controller.Right.AddDefaultBinding(Key.RightArrow);
            controller.Right.AddDefaultBinding(InputControlType.LeftStickRight);

            //controller.AttackFast.AddDefaultBinding(Key.E);
			controller.AttackFast.AddDefaultBinding(Key.I);
            controller.AttackFast.AddDefaultBinding(InputControlType.Action4);

            //controller.AttackSlow.AddDefaultBinding(Key.R);
			controller.AttackSlow.AddDefaultBinding(Key.U);
            controller.AttackSlow.AddDefaultBinding(InputControlType.Action3);

            controller.Block.AddDefaultBinding(Key.O);
            controller.Block.AddDefaultBinding(InputControlType.RightTrigger);
			//controller.Block.AddDefaultBinding (InputControlType.LeftTrigger);

            controller.SwitchWeaponSet.AddDefaultBinding(Key.Tab);
			controller.SwitchWeaponSet.AddDefaultBinding(Key.P);
            controller.SwitchWeaponSet.AddDefaultBinding(InputControlType.Action2);

            controller.Activate.AddDefaultBinding(Key.E);
            controller.Activate.AddDefaultBinding(InputControlType.Action1);

            controller.UsePotion.AddDefaultBinding(Key.H);
            controller.UsePotion.AddDefaultBinding(InputControlType.LeftTrigger);



            //controller.SlideLeft.AddDefaultBinding(Key.Y);
            //controller.SlideLeft.AddDefaultBinding(InputControlType.LeftBumper);

            //controller.SlideRight.AddDefaultBinding(Key.X);
            //controller.SlideRight.AddDefaultBinding(InputControlType.RightBumper);

            controller.BackStep.AddDefaultBinding(Key.Space);
			controller.BackStep.AddDefaultBinding(InputControlType.RightBumper);

            controller.GodRage.AddDefaultBinding(Key.R);
            controller.GodRage.AddDefaultBinding(InputControlType.LeftBumper);




            controller.StartPause.AddDefaultBinding (Key.Escape);
			controller.StartPause.AddDefaultBinding (InputControlType.Start);

            this.deviceId = deviceId;
            this.LinkDevice();

            InputManager.OnDeviceAttached += inputDevice => this.LinkDevice();
            InputManager.OnDeviceDetached += inputDevice => this.LinkDevice();
            InputManager.OnActiveDeviceChanged += inputDevice => this.LinkDevice();
        }

        private void LinkDevice() {
            if (InputManager.Devices.Count > deviceId) {

                controller.Device = InputManager.Devices[deviceId];
                controller.Enabled = true;

            } else {

                if (deviceId != 0) {
                    controller.Enabled = false;
                }
            }
        }

        public void SetDevice(int deviceId) {

            this.deviceId = deviceId;
            LinkDevice();
        }

        public bool IsConnected() {
            return controller.Enabled;
        }

        public float GetHorizontal() {
            return controller.Horizontal.Value;
        }

        public float GetVertical() {

            return controller.Vertical.Value;
        }

        public bool GetJump() {
            return controller.Jump.WasPressed;
        }

        public bool GetAttackFast() {
            return controller.AttackFast.WasPressed;
        }

        public bool GetAttackSlow() {
            return controller.AttackSlow.WasPressed;
        }

        public bool GetBlock() {
            return controller.Block.IsPressed;
        }

        public bool GetSwitchWeaponSet() {
            return controller.SwitchWeaponSet.WasPressed;
        }

        public bool GetSlideLeft() {
            return controller.SlideLeft.WasPressed;
        }

        public bool GetSlideRight() {
            return controller.SlideRight.WasPressed;
        }

		public bool GetBackStep()
		{
			return controller.BackStep.WasPressed;
		}

		public bool GetGodRage()
		{
			return controller.GodRage.WasPressed;
		}

        public bool GetActivate()
        {
            return controller.Activate.WasPressed;
        }

        public bool GetUsePotion()
        {
            return controller.UsePotion.WasPressed;
        }

        public bool GetStartPause() {
			return controller.StartPause.WasPressed;
		}

        public void Clear(bool state = true) {
            controller.Clear();
        }

    }
}
