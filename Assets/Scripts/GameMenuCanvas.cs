﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class GameMenuCanvas : MonoBehaviour {


	public GameObject MenuPanel,StartMenuPanel,CreditsPanel,LevelSelectionPanel,SequencePanel,TitleScreenPanel,ControlsPanel;
	private List<GameObject> Panels;

	/// <summary>
	/// The _instance.
	/// </summary>
	private static GameMenuCanvas _instance;
	
	/// <summary>
	/// Gets the instance.
	/// </summary>
	/// <value>The instance.</value>
	public static GameMenuCanvas instance
	{
		get
		{
			if(_instance == null)
			{
				if(GameObject.FindObjectOfType<GameMenuCanvas>() != null) {
				_instance = GameObject.FindObjectOfType<GameMenuCanvas>();
					if(_instance != null) {
						DontDestroyOnLoad(_instance.gameObject);
					}
				}

			}
			
			return _instance;
		}
	}

	void Awake() {
		Ini();

	}

	void Ini() {
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}

		Panels = new List<GameObject> ();
		Panels.Clear ();
		Panels.Add (MenuPanel);
		Panels.Add (StartMenuPanel);
		Panels.Add (CreditsPanel);
		Panels.Add (LevelSelectionPanel);
		Panels.Add (SequencePanel);
		Panels.Add (TitleScreenPanel);
		Panels.Add (ControlsPanel);
	}


	public void ShowPanel(GameObject Panel) {
		foreach (GameObject P in Panels) {
			if(P.activeSelf) {
				P.SetActive(false);
			}
		}
		Panel.SetActive (true);
	}

	public void HideAllPanels() {
		foreach (GameObject P in Panels) {
			P.SetActive(false);
		}
	}

	// Use this for initialization
	void OnLevelWasLoaded (int index) {
		Ini ();
	}
	

}
