﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Utils.Testing {

	public class TestHelper : MonoBehaviour {

		/// <summary>
		/// The testing player.
		/// </summary>
		private GameObject Player;

		/// <summary>
		/// The reset key.
		/// </summary>
		public KeyCode ResetKey;

		/// <summary>
		/// The save position toggle key.
		/// </summary>
		public KeyCode SavePositionToggleKey;

		/// <summary>
		/// The health cheat key.
		/// </summary>
		public KeyCode HealthCheatKey;

		/// <summary>
		/// The health per key press.
		/// </summary>
		public float HealthPerKeyPress = 0.5F;

		/// <summary>
		/// The faith cheat key.
		/// </summary>
		public KeyCode FaithCheatKey;
		
		/// <summary>
		/// The faith per key press.
		/// </summary>
		public float FaithPerKeyPress = 0.5F;


		/// <summary>
		/// The faith cheat key.
		/// </summary>
		public KeyCode MoneyCheatKey;
		
		/// <summary>
		/// The faith per key press.
		/// </summary>
		public int MoneyPerKeyPress = 10;

		/// <summary>
		/// The saved position.
		/// </summary>
		private static Vector3 savedPosition;


		/// <summary>
		/// Tells wether the is position saved or not.
		/// </summary>
		private bool isPositionSaved = false;

		/// <summary>
		/// The _instance.
		/// </summary>
		private static TestHelper _instance;

		/// <summary>
		/// Gets the instance.
		/// </summary>
		/// <value>The instance.</value>
		public static TestHelper instance
		{
			get
			{
				if(_instance == null)
				{
					_instance = GameObject.FindObjectOfType<TestHelper>();
					
					//Tell unity not to destroy this object when loading a new scene!
					DontDestroyOnLoad(_instance.gameObject);
				}
				
				return _instance;
			}
		}


		void Ini() {
			if(_instance == null)
			{
				//If I am the first instance, make me the Singleton
				_instance = this;
				DontDestroyOnLoad(this);
			}
			else
			{
				//If a Singleton already exists and you find
				//another reference in scene, destroy it!
				if(this != _instance)
					Destroy(this.gameObject);
			}

			if (Player == null) {
				Player = GameObject.Find ("Walti");
			}

			if (HealthCheatKey == KeyCode.None) {
				HealthCheatKey = KeyCode.H;
			}
		}


		void Awake() {
			Ini ();
		}


		void Start() {

            ResetKey = KeyCode.F5;
            SavePositionToggleKey = KeyCode.F4;
            HealthCheatKey = KeyCode.F1;
            FaithCheatKey = KeyCode.F2;
            MoneyCheatKey = KeyCode.F3;

            if (ResetKey == KeyCode.None) {
				ResetKey = KeyCode.F5;
			}
			if (SavePositionToggleKey == KeyCode.None) {
				SavePositionToggleKey = KeyCode.F4;
			}
			if (HealthCheatKey == KeyCode.None) {
				HealthCheatKey = KeyCode.F1;
			}

			if (FaithCheatKey == KeyCode.None) {
				FaithCheatKey = KeyCode.F2;
			}

			if (MoneyCheatKey == KeyCode.None) {
				MoneyCheatKey = KeyCode.F3;
			}
		}
	

		void Update() {
			if (Input.GetKeyDown (ResetKey)) {
				//Reset Level
				Application.LoadLevel(Application.loadedLevel);
			}

			if (Input.GetKeyDown (SavePositionToggleKey)) {
				//Toggle the boolean
				isPositionSaved = !isPositionSaved;

				if(isPositionSaved) {
					//Save position
					savedPosition = Player.transform.position;
				} else {
					//Clear saved position
					savedPosition = Vector3.zero;
				}

			
			}

			if (Input.GetKeyDown (HealthCheatKey)) {
				//Add Health
				Player.GetComponent<LogicPlayer>().AddHealth(HealthPerKeyPress);
			}

			if (Input.GetKeyDown (FaithCheatKey)) {
				//Add Health
				Player.GetComponent<LogicPlayer>().AddFaith(FaithPerKeyPress);
			}

			if (Input.GetKeyDown (MoneyCheatKey)) {
				//Add Health
				Player.GetComponent<LogicPlayer>().AddMoney(MoneyPerKeyPress);
			}
		}


		void OnLevelWasLoaded(int index) {
			Ini ();

			if (isPositionSaved) {
				//Move Character to saved Position
				Player.transform.position = savedPosition;
			}
		}

		void OnGUI() {
			if (isPositionSaved) {
				//Display that position is Saved
				GUI.Label(new Rect(Screen.width-100,10,100,30),"Position Saved");
			} 
		}
	}
}