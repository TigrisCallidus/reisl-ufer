﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RaycastObstacleFinder : MonoBehaviour {

	Camera cam;
	public float SeeThroughTime;
	public float CameraElevation = 6F;
	public float CameraDistance = 12F;
	public float ElevationDuration = 2F;
	private float originalHeight, originalDistance;

	private bool cameraMoving = false;

	private CameraManager cManager;


	//private bool IsCameraUp = false;

	// Use this for initialization
	void Awake () {
		cam = Camera.main;
	}

	void Start() {
		cManager = cam.GetComponent<CameraManager>();
		originalHeight = cManager.height;
		originalDistance = cManager.distance;
	}


	GameObject[] CheckForObstacle(GameObject TargetObject) {
		List<GameObject> res =  new List<GameObject>();
		Vector3 Dir = TargetObject.transform.position - transform.position;

		Ray R = new Ray (transform.position,Dir);
		Debug.DrawRay (transform.position, Dir);
		float MaxDistance = Vector3.Distance (transform.position, TargetObject.transform.position);
		//Vector3 Dir = TargetObject.transform.position - transform.position;
		RaycastHit[] RHits = Physics.RaycastAll (R, MaxDistance);

		foreach (RaycastHit RHit in RHits) {
			if(RHit.collider.gameObject.layer == 14) {
				res.Add(RHit.collider.gameObject);	
				//Debug.Log ("Found "+RHit.collider.gameObject.name);
			}
		}

		return res.ToArray ();
	}

	/*
	void AddSeeThroughScript(GameObject[] Hits, float SeeThroughTime) {
		foreach (GameObject G in Hits) {
			if(G.GetComponent<SeeThrough>() == null) {
				G.AddComponent<SeeThrough>();
				G.GetComponent<SeeThrough>().SeeThroughTime = SeeThroughTime;
			}
		}
	}
	*/

	IEnumerator MoveCameraUp() {
		cameraMoving = true;
		for (float f = 0; f < ElevationDuration; f+=Time.deltaTime) {
			float newHeight = Mathf.Lerp(originalHeight,CameraElevation,f/ElevationDuration);
			float newDistance = Mathf.Lerp(originalDistance,CameraDistance,f/ElevationDuration);

			cManager.height = newHeight;
			cManager.distance = newDistance;

			yield return null;
		}
		//IsCameraUp = true;
		cameraMoving = false;
	}

	IEnumerator MoveCameraDown() {
		cameraMoving = true;
		float currentHeight = cManager.height;
		float currentDistance = cManager.distance;
		for (float f = 0; f < ElevationDuration; f+=Time.deltaTime) {
			float newHeight = Mathf.Lerp(currentHeight,originalHeight,f/ElevationDuration);
			float newDistance = Mathf.Lerp(currentDistance,originalDistance,f/ElevationDuration);

			cManager.height = newHeight;
			cManager.distance = newDistance;
		
			yield return null;
		}
		//IsCameraUp = false;
		cameraMoving = false;
	}
	

	// Update is called once per frame
	void FixedUpdate () {
		if (CheckForObstacle (cam.gameObject).Length > 0) {
			//Has obstacle
			if(cameraMoving) {
				StopCoroutine("MoveCameraDown");
				cameraMoving = false;
				StartCoroutine("MoveCameraUp");
			} else {
				if(cManager.height != CameraElevation || cManager.distance != CameraDistance) {
					StopCoroutine("MoveCameraDown");
					StartCoroutine("MoveCameraUp");
				}
			}
		} else {
			if(cameraMoving) {
				StopCoroutine("MoveCameraUp");
				cameraMoving = false;
				StartCoroutine("MoveCameraDown");
			} else {
				if(cManager.height != originalHeight || cManager.distance != originalDistance) {
					StopCoroutine("MoveCameraUp");
					StartCoroutine("MoveCameraDown");
				}
			}
		}
		//AddSeeThroughScript (CheckForObstacle (cam.gameObject),SeeThroughTime);
	}
}
