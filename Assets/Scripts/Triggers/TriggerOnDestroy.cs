﻿using UnityEngine;
using System.Collections;
using GameLab.NotficationCenter;

/*
 * exit now ... 
 * 
 * */


public class TriggerOnDestroy: TriggerBase {

	Barrel barrel;

    void OnDestroy()
    {
        if (Application.isPlaying && gameLogic.notificationCenter!=null) {
            AddNotification(NotificationName, NotificationArgument, transform.position, GameLab.NotficationCenter.NotificationPriority.ThrowAwayAfterProcessing);
        }
    }

}
