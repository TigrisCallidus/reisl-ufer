﻿using UnityEngine;
using System.Collections;
using GameLab.NotficationCenter;
using GameLab.HackAndSlashFramework;



public class ActivateScript : MonoBehaviour
{

    bool enterFirstTime = false;
    bool exitFirstTime = false;

    // game logic
    private NotificationCenter notificationCenter;
    private LogicPlayer player1;
    private LogicPlayer player2;
    public CharacterEngineAI characterWhichFollows;
    public bool aCharacterShouldFollow = false;
    public bool goneAfterUse = false;


    // argument < GameElement.argument

    public int price;
    public float healthgain;
    public float faithgain;
    public int healthPotionGain;
    public GameObject Text;
    int numberOfPlayers = 0;

    public bool addMercenary = false;
    public string MercenaryName = "";


    void Start()
    {
        this.notificationCenter = GameObject.FindObjectOfType<NotificationCenter>();
        LogicPlayer[] gos = GameObject.FindObjectsOfType<LogicPlayer>();
        foreach (LogicPlayer go in gos)
        {
            if (go.GetCharacter().type == CharacterType.Player1)
            {
                this.player1 = go;
            }

            if (go.GetCharacter().type == CharacterType.Player2)
            {
                this.player2 = go;
            }

        }

        //Debug.Log(this + " found gameLogic " + gameLogic);
    }

    /*
       * Triggers 
       * */

    void OnTriggerEnter(Collider other)
    {

        // check for player
        LogicPlayer logicPlayer = other.GetComponent<LogicPlayer>();
        if (logicPlayer != null)
        {
            numberOfPlayers += 1;
            Text.SetActive(true);
            if (logicPlayer==player1)  {
                notificationCenter.onPlayer1Activate += doChangesPlayer1;
            } else if (logicPlayer==player2)  {
                notificationCenter.onPlayer2Activate += doChangesPlayer2;
            }

        }

    }

    void OnTriggerExit(Collider other)
    {


        // check for player
        LogicPlayer logicPlayer = other.GetComponent<LogicPlayer>();
        if (logicPlayer != null)
        {
            numberOfPlayers -= 1;
            if (numberOfPlayers <= 0)
            {
                Text.SetActive(false);
            }
            if (logicPlayer == player1)
            {
                notificationCenter.onPlayer1Activate -= doChangesPlayer1;

            }
            else if (logicPlayer == player2)
            {
                notificationCenter.onPlayer2Activate -= doChangesPlayer2;

            }

        }

    }

    public void doChangesPlayer1()
    {
        doChanges(player1);

    }

    public void doChangesPlayer2()
    {
        doChanges(player1);
    }

    public void doChanges(LogicPlayer player)
    {
        if (player.Money < price)
        {

        } else
        {
            player.Money -= price;
            player.Health += healthgain/100;
            player.Faith += faithgain/100;
            player.numberOfPotions += healthPotionGain;
        }
        if (aCharacterShouldFollow)
        {
            characterWhichFollows.followFriend = true;
        }
        if (goneAfterUse)
        {
            if (player == player1)
            {
                notificationCenter.onPlayer1Activate -= doChangesPlayer1;
                if (numberOfPlayers == 2)
                {
                    notificationCenter.onPlayer2Activate -= doChangesPlayer2;

                }
            }
            else
            {
                notificationCenter.onPlayer2Activate -= doChangesPlayer2;
                if (numberOfPlayers == 2)
                {
                    notificationCenter.onPlayer1Activate -= doChangesPlayer1;

                }
            }
            this.gameObject.SetActive(false);
        }
        if (addMercenary)
            AddMercenary();
        {

        }
    }

  public void AddMercenary()
    {
        SceneStateManager.instance.AddMercenarie(MercenaryName);
    }
}
