﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameLab.HackAndSlashFramework;
using GameLab.NotficationCenter;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleDamageDealer : LogicBase {
	
	public List<int> AllowedLayers;
	public float PushBackForce;
	public float Damage;
	//private BoxCollider col;

	void Start() {
		//col = GetComponent<BoxCollider> ();
	}
	
	void PushBack(GameObject victim, Vector3 pushDir) {
		Rigidbody r = victim.GetComponent<Rigidbody> ();
		
		//Push victim back
		r.AddRelativeForce (pushDir * r.mass * PushBackForce, ForceMode.Impulse);
	}
	
	void DealDamage(LogicPlayer victim) {
		victim.AddHealth (-Damage);
	}
	
	
	void OnParticleCollision(GameObject other) {
		Rigidbody body = other.GetComponent<Rigidbody>();
		Debug.Log ("Particle hit by " + body.gameObject.name);
		if (body != null) {
			Vector3 victimPos, victimDir, myPos;
			myPos = transform.position;
			
			
			if (AllowedLayers.Contains (body.gameObject.layer)) {
				//Check if Collider should take demage
				victimPos = body.gameObject.transform.position;
				victimDir = victimPos - myPos;
				Debug.Log ("Damage dealt to " + body.gameObject.name);
				PushBack (body.gameObject, victimDir);
				DealDamage (body.gameObject.GetComponent<LogicPlayer> ());
			}
		}
	}
	

	
}
