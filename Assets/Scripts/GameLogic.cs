﻿using UnityEngine;
using System.Collections;
using GameLab.NotficationCenter;
using GameLab.HackAndSlashFramework;
using DarkTonic.MasterAudio;

public class GameLogic : MonoBehaviour
{

  #region Singleton

  private static GameLogic instance = null;

  public static GameLogic Instance {
    get {
      return instance;
    }
  }

  #endregion

  public LogicPlayer playerInShop;

	public float player1Friends;
	public float player2Friends;

	public bool showStats=true;
	public int positionCounter=0;


	private float fightingSpirit=1f;
	private float baseFightingSpirit=1f;
	private float bonusFightingSpirit=0f;

	// debug & gui
	public bool debugVisual = true;
	public GUIStyle guiDebug;

#region figthingSpirit

  [Tooltip("Sets how much the fightingSpirit decays all 5 seconds")]
	public float fightingSpiritDecay=0.01f;

	[Tooltip("Sets how much fightingSpirit is added, when scoring kills without getting hit")]
	public float fightingSpiritOnKill=0.1f;

	[Tooltip("Sets how much fightingSpirit is added, when scoring kills without getting hit")]
	public float fightingSpiritOnFrenzy=0.1f;

	[Tooltip("Sets how much fightingSpirit is lost, when a friendly NPC gets hit.")]
	public float fightingSpiritOnNPCHit=0.01f;

	[Tooltip("Sets how much fightingSpirit is lost, when you get hitted.")]
	public float fightingSpiritOnPlayerHit=0.1f;

	[Tooltip("Sets how much fightingSpirit is gained, when a friendly NPC hits with the team.")]
	public float fightingSpiritTeamhitNPC=0.01f;
	
	[Tooltip("Sets how much fightingSpirit is gained, when you hit with the team.")]
	public float fightingSpiritTeamhitPlayer=0.1f;

	[Tooltip("Maximum Bonus Fighting Spirit.")]
	public float MaxBonusFightingSpirit=1.0f;

	[Tooltip("Minimum Bonus Fighting Spirit.")]
	public float MinBonusFightingSpirit= 0.0f;

	private bool fightingSpiritWasHigh = false;

#endregion

  public GameObject prefab_MasterAudio;

  #region Shop_variables

  public int FaithPrice = 100;
  public int HealthPrice = 10;
  public int SpiritPrice = 50;

  [Tooltip("How much % of the Maximum Faith should the player get for buying 'Faith' once")]
  public float FaithMultiplierOfMax = 0.25f;

  [Tooltip("How much % of the Maximum Health should the player get for buying 'Health' once")]
  public float HealthMultiplierOfMax = 0.25f;

  [Tooltip("How much % of the Maximum Spirit should the player get for buying 'Spirit' once")]
  public float SpiritMultiplierOfMax = 0.25f;

  #endregion

  // state
  //bool versionBloody = true; 

  // state maschine

  public GameLogicState state; // state
	public enum GameLogicState {
		Intro,
		Menu,
		Ingame,
		GameOver
	}

		// sub state
		public GameLogicStateSub statesub;
		public enum GameLogicStateSub {
			Fight,
			Plunder
		}

	// modal
	public GameLogicModal modal;
	public enum GameLogicModal {
		Running,
		Pause,
		Preferences,
		Editor
	}


	public void SetGameState( GameLogicModal newmodal ) {

		// Debug.Log ("[GameLogic]SetGameState("+newmodal.ToString ()+")");

		// deactivate in the beginning
		DeactivateEditor();
		DeactivateRunning();

		// Attention: running > leveleditor
		modal = newmodal;


		// ACTIVATE NEW MODEL
		if (modal == GameLogic.GameLogicModal.Running) {
			ActivateRunning();
			LoadActualLevel();
		}
		if (modal == GameLogic.GameLogicModal.Editor) {
			ActivateEditor();
			LoadActualLevel();
		}



	}

		// Deactivate & Activate
		public void DeactivateRunning() {
			//Debug.Log ("[GameLogic]DeactivateRunning()");
			
			if (gameCamera != null) {
				gameCamera.SetActive (false);
			}
			if (gameHUD != null) {
				gameHUD.SetActive (false);
			}
			// GameObject.Find ("_Ingame").gameObject.transform.Find ("IngameCamera").gameObject.SetActive(false);	
		}
		public void DeactivateEditor() {
			// Debug.Log ("[GameLogic]DeactivateEditor()");
			
			// search camera and deactivate it ...
			// gameHUD.SetActive ( false );
            try {
                GameObject.Find("_LevelEditor").gameObject.transform.Find("Container").gameObject.SetActive(false);
            }
            catch {
                Debug.LogWarning("Can't find _LevelEditor or the child 'Container'");
            }
			
		}

		// activate
		public void ActivateRunning() {

			// Debug.Log ("[GameLogic]ActivateRunning()");

			if (gameCamera != null) {
				gameCamera.SetActive (true);
			}
			if (gameHUD != null) {
				gameHUD.SetActive (true);
			}
			// GameObject.Find ("_Ingame").gameObject.transform.Find ("IngameCamera").gameObject.SetActive(true);	
		}
		public void ActivateEditor() {

			// Debug.Log ("[GameLogic]ActivateEditor()");
			GameObject.Find ("_LevelEditor").gameObject.transform.Find ("Container").gameObject.SetActive(true);	

		}

		// Register Player 
		public void RegisterPlayerForRunning( GameObject playerObj, int playerNr ) {
			
			// Debug.Log ("[GameLogic]RegisterPlayerForRunning("+playerNr+")" );

	// 	return; 
			// Register: HUD

			// @todo: dirty .. better code

			if (modal == GameLogicModal.Running) {

			if (playerObj != null) {

				// player 1
				if (playerNr == 1) {
					// Register: Camera

					//if (gameHUD != null) {
					//	gameHUD.SetActive (true);
					//	if (gameHUDScript!=null) {
					//		gameHUDScript.player1 = playerObj; // .GetComponent<LogicPlayer>();
					//		gameHUDScript.RegisterPlayerScripts ();
					//	}
					//}

				}

				// player 2
				if (playerNr == 2) {
					// Register: Camera
					if (gameCamera != null) {
						gameCamera.SetActive (true);
					}
					//if (gameHUD != null) {
					//	gameHUD.SetActive (true);
					//	if (gameHUDScript!=null) {
					//		gameHUDScript.player2 = playerObj; // .GetComponent<LogicPlayer>();
					//		gameHUDScript.RegisterPlayerScripts ();
					//	}
					//}

				}
				
			}

		}

	}

		

	// Levels (Load etc.)
	LevelEditor levelEditor;
	public int level = 1;

	public void SetGameLevel( int ilevel ) {

		level = ilevel;
		LoadGameLevel (level);

	}

	public void LoadActualLevel(  ) {
		LoadGameLevel (level);
	}

	public void ReloadLevel(){
		Application.LoadLevel (Application.loadedLevel);
	}


	public void LoadMenu()
	{
		SceneStateManager.instance.changeState(new MenuScreenState());
	}

	public void LoadGameLevel( int level ) {

        try {

            if (levelEditor == null) {
                GameObject levelEditorObject = GameObject.Find("_LevelEditor");
                levelEditor = levelEditorObject.GetComponent<LevelEditor>();
            }
            levelEditor.LoadGameLevel(level);

        }
        catch {
            Debug.LogWarning("can't load _LevelEditor");
        }


	}
	public void LoadGameNextLevel( ) {

		level++;
		LoadGameLevel (level);
	}

	// GameElements
	// 
	// Management in _LevelEditor!
	// 
	// < types etc defined in _LevelEditor
	// 
	// 
	public GameElement AddGameElement ( string type, string subtype, string name, string argument, Vector3 pos ) {

		return AddGameElement ( "",  type,  subtype,  name,  argument,  pos, 0.0f, ""  );

	}

	public GameElement AddGameElement ( string release, string type, string subtype, string name,  string argument, Vector3 posx, float rot, string stringNotification ) {

		GameElement ge = new GameElement ();

		ge.release = release;

		ge.type = type;
		ge.subtype = subtype;

		ge.name= name;
		ge.argument = argument;

		ge.position = posx;
		ge.rotation = rot;

		GameElement geinArray = AddGameElement( ge );

		// add notification there
		// stringNotifcation
		// not notification needed
		if (!stringNotification.Equals ("")) {

			Vector3 pos = new Vector3 ( geinArray.position.x, geinArray.position.y, geinArray.position.z );

			AddNotification (stringNotification, pos, NotificationPriority.ThrowAwayAfterProcessing);
		}

		
		return ge;

	}

	// Add Element > levelEditor
	public GameElement AddGameElement( GameElement newelement ) {

		return levelEditor.AddElement ( newelement  );
	}

	
	public ArrayList GetGameElementsByName( string name ) {
		
		return levelEditor.GetGameElementsByName( name );
		
	}

	public void AddGameElements ( ArrayList arr ) {

		for (int i=0; i<arr.Count; i++) {
			GameElement ge = (GameElement) arr[i];
			// Debug.Log (i+". "+ge.name+" "+ge.type);
			AddGameElement( ge );
		}

	}

	// Remove Element 
	public void RemoveGameElement( GameElement deleteThisGameElement ) {

		//Vector3 pos = new Vector3 (deleteThisGameElement.position.x, deleteThisGameElement.position.y, deleteThisGameElement.position.z );

		levelEditor.RemoveElement ( deleteThisGameElement);

		// not notification needed
		/*
		 * if (!stringNotification.Equals ("")) {
			AddNotification (stringNotification, pos, NotificationPriority.ThrowAwayAfterProcessing);
		}
		*/
	}



	// HUD
	public GameObject gameHUD;
	//PlayerHUD gameHUDScript;

	// GAME CAMERA
	public GameObject gameCamera;
	CameraManager gameCameraScript;

  void Awake()
  {
    instance = this;
    MasterAudio Ma = GameObject.FindObjectOfType<MasterAudio>();
    if (Ma == null){
      InitMasterAudio();
    }
  }

	// Use this for initialization
	void Start () {

    // init
    if (notificationCenter == null) {
      GameObject gameObj = GameObject.Find("_NotificationCenter");
      notificationCenter = gameObj.GetComponent<NotificationCenter>();
    }

    // state maschine
    state = GameLogicState.Ingame;
		statesub = GameLogicStateSub.Fight;

		// modal = GameLogicModal.Running;
		// HUD
		if (gameHUD!=null) {
			//gameHUDScript = gameHUD.GetComponent<PlayerHUD>();
		}
		// Camera
		if (gameCamera != null) {
			gameCameraScript = gameCamera.GetComponent<CameraManager>();
		}

		// running
		SetGameState ( GameLogicModal.Running );
	}

	public float FightingSpirit {
		get {
			baseFightingSpirit=(player2Friends+player1Friends)*0.1f+1f;
			fightingSpirit=baseFightingSpirit+bonusFightingSpirit;
            //return fightingSpirit;
            // HACK
            return 1;
		}
	}



	public void AddBonusFightingSpirit(float addSpirit){
		//print ("Spirit added " + addSpirit);
		bonusFightingSpirit = bonusFightingSpirit + addSpirit;
		if (bonusFightingSpirit<MinBonusFightingSpirit) {
			bonusFightingSpirit=MinBonusFightingSpirit;
			if(fightingSpiritWasHigh){
				AddNotification("[spirit.low]");


			}
		}
		if (bonusFightingSpirit > MaxBonusFightingSpirit) {
			bonusFightingSpirit=MaxBonusFightingSpirit;
		}
		if (bonusFightingSpirit >= MaxBonusFightingSpirit * 0.8) {
			AddNotification("[spirit.high]");
		}
		if (bonusFightingSpirit >= 0.1*MaxBonusFightingSpirit) {
			fightingSpiritWasHigh=true;
		}
		AddNotification ("[changed.spirit]", addSpirit.ToString(), new Vector3 (), NotificationPriority.ThrowAwayAfterProcessing);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

  private void InitMasterAudio()
  {
    Vector3 mAPos = this.gameCamera.transform.position + new Vector3(0, -1, 0);
    GameObject mA = Instantiate(this.prefab_MasterAudio, this.transform.position, Quaternion.identity) as GameObject;
    DontDestroyOnLoad(mA);
    mA.transform.position = mAPos;
    //Debug.Log("Init from MasterAudio from " + this );
    //mA.transform.parent = this.gameCamera.transform;
    //this.gameCamera.GetComponent<CameraManager>().MasterAudioGO = mA;
  }

	// old gui
	void OnGUI() {

		if (debugVisual) {
			GUI.Label (new Rect (0, 0, 200, 20), " // SWISS MERCENARY: [" + state.ToString () + "/" + statesub + "]", guiDebug);
		}

	}

	/*
	 * 
	 *  state
	 * 
	 * */
	public bool CheckPaused() {

		if (modal==GameLogicModal.Running) {
			return false;
		}
		return true;

	}
	
	/*
	 * 
	 * notification system
	 * 
	 * */

	// notification center
	public NotificationCenter notificationCenter;

	// priority - not correct!

	public void AddNotification( string key ) {
		
		AddNotification ( key, "", "", new Vector3(), NotificationPriority.ThrowAwayAfterProcessing ); 
		
	}

	public void AddNotification( string key, Vector3 point ) {
		
		AddNotification ( key, "", "", point, NotificationPriority.ThrowAwayAfterProcessing ); 
		
	}

	public void AddNotification( string key, Vector3 point, NotificationPriority np ) {
		
		AddNotification ( key, "", "", point, np ); 
		
	}

	public void AddNotification( string key, string notificationArgument, Vector3 point, NotificationPriority np ) {
	
		AddNotification ( key, notificationArgument, "", point, np ); 
		
	}

	public void AddNotification( string key, string notificationArgument, string notificationOrigin, Vector3 point, NotificationPriority np ) {
		// add
		notificationCenter.AddNotification ( key, notificationArgument, notificationOrigin, point, np ); 

	}

  public static float GetRoundFloat(float number, int digits) {
    return ((int)(number * Mathf.Pow(10, (float)digits))) / Mathf.Pow(10, (float)digits);
    //Debug.Log ("number: " + number + " multipler: " + multipler + " roundedNumber: " + roundedNumber);
  }

  void OnLevelWasLoaded(int lvl)
  {
    if (SceneStateManager.instance.Is2Player){
      GameObject[] gos = GameObject.FindGameObjectsWithTag("1PlayerOnly");
      foreach (var VARIABLE in gos) {
        VARIABLE.SetActive(false);
      }
    } else { 
      GameObject[] gos = GameObject.FindGameObjectsWithTag("2PlayerOnly");
      foreach (var VARIABLE in gos){
        VARIABLE.SetActive(false);
      }
    }
  }

}
