﻿using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework;
using GameLab.HackAndSlashFramework.UnityExtentions;

public class CameraManager : MonoBehaviour {

    public float height = 3f;
    public float distance = 3f;
    public float speed = 1f;
    public float maxSpeed = 10f;

  private CharacterEngine player1 = null;
  private CharacterEngine player2 = null;

    public bool isShaking = false;
    private float shakeIntensity;
    private float shakeDuration=0;
    private float startTime;
    private float shakeDecay;
    private Vector3 theShake;

  //public GameObject MasterAudioGO;
  //public GameObject PlayListGO;

  private Vector3 velocity;
	private Transform CamTargetP1;
    // cached vars
    private Vector3 _cameraCentrum;
    private Vector3 _targetPos;
    private Vector3 _targetRotDir;
    private Quaternion _targetRot;

	// Use this for initialization
	void Start ()
	{
	  FindPlayers();
    CamTargetP1 = player1.transform.FindChild ("CamTarget").transform;
	}

  private void FindPlayers()
  {
    CharacterEngine[] engs = GameObject.FindObjectsOfType<CharacterEngine>();
    foreach (CharacterEngine eng in engs)
    {
      if (eng.type == CharacterType.Player1){
        player1 = eng;
      }
      if (eng.type == CharacterType.Player2) {
        player2 = eng;
      }
    }
  }

	// Update is called once per frame
	void LateUpdate () {


        if (player1.controls.IsConnected() && (player2 != null && player2.controls.IsConnected())) {
            _cameraCentrum = (CamTargetP1.position + this.player2.transform.position) / 2;

        } else if(player1.controls.IsConnected()) {
            _cameraCentrum = CamTargetP1.position;

        } else if (player2.controls.IsConnected()) {
            _cameraCentrum = this.player2.transform.position;
        }

        //if (player1.state == CharacterState.Grounded) {

            _targetPos = _cameraCentrum + (Vector3.up * this.height) + (-transform.forward * this.distance);
            transform.position = Vector3.SmoothDamp(transform.position, _targetPos, ref velocity, this.speed, this.maxSpeed, Time.smoothDeltaTime);
        if (isShaking)
        {
            transform.position += theShake;
        }

            /* rotate camera with player view
             *
            _targetRotDir = FastMath.Normalize((this.target.transform.position - transform.position) + Vector3.up);
            _targetRot = Quaternion.LookRotation(_targetRotDir);
            transform.rotation = Quaternion.Slerp(transform.rotation, _targetRot, 0.5f);
            */
           
        //}
	}

    void Update()
    {
        if (isShaking)
        {
            if (-startTime + Time.time > shakeDuration)
            {
                isShaking = false;
            } else {
                theShake = Random.insideUnitSphere*shakeIntensity/10;
            }
        }
    }

    public void Shake(float intensity, float duration, float decay)
    {
        isShaking = true;
        startTime = Time.time;
        shakeDuration = duration;
        shakeDecay = decay;
        shakeIntensity = intensity;
    }

}
