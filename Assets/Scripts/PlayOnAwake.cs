﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayOnAwake : MonoBehaviour {

	public MovieTexture movTexture;
	public float SequenceDuration;

	void Awake() {

	}

	// Use this for initialization
	void Start () {

		movTexture = GetComponent<RawImage> ().texture as MovieTexture;

	}
	
	// Update is called once per frame
	void Update () {
		if (!movTexture.isPlaying) {
			movTexture.Play ();
		}

		if (Time.timeSinceLevelLoad >= SequenceDuration){//SceneStateManager.instance.state.SequenceDuration) {
			SceneState newState = SceneStateManager.instance.state.AutomaticNextScene;
			SceneStateManager.instance.changeState(newState);
		}

		if (Input.anyKeyDown) {
			SceneState newState = SceneStateManager.instance.state.AutomaticNextScene;
			SceneStateManager.instance.changeState(newState);
		}
	}
}
