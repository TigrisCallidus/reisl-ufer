﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameLab.NotficationCenter;

public class SaveCheckpoint : MonoBehaviour {

	bool checkpointSaved = false;

	public Vector3 RespawnOffset;
	public List<int> AllowedLayers;

	public GameObject VisualFeedback;

	public void Save(Vector3 pos) {
		if (!checkpointSaved) {

			VisualFeedback.SetActive(true);
			VisualFeedback.GetComponent<ParticleSystem>().Play();
			SceneStateManager.LastCheckpoint = pos + RespawnOffset;
			SceneStateManager.LastCheckpointCamera = Camera.main.transform.position + RespawnOffset;
			checkpointSaved = true;

			GameObject.FindObjectOfType<GameLogic>().AddNotification ("sound.checkpoint.triggerenter", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
		}
	}


	void OnTriggerEnter(Collider Col) {
		//Check if Object is allwed to teleport
		if (AllowedLayers.Contains (Col.gameObject.layer)) {
			Save (transform.position+RespawnOffset);
		}
	}
}
