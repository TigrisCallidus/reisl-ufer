﻿using System;
using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework;
using GameLab.NotficationCenter;

public class LogicHuman : LogicBase
{

  private bool isDead = false;

	//Fighting Spirit

	[Tooltip("Maximum attack speed you can get when fighting spirit is full.")]
	public float fightingSpiritAttackSpedModifier=2;
	protected float currentSpeedModification = 1;
	public bool fightingSpiritBoostsAttackSpeed = false;


	[Tooltip("Changes the damage bonus you get from fighting spirit. 1 Means 10% per friend around.")]
	public float fightingSpiritDamageModifier=0.5f;
	protected float currentDamageModification=1;
	public bool fightingSpiritBoostsDamage = true;

	[Tooltip("Changes the movement speed bonus you get from fighting spirit. 1 Means 10% per friend around.")]
	public float fightingSpiritMovementModifier=0.5f;
	protected float currentMovementModification=1;
	public bool fightingSpiritBoostsMovementSpeed = true;
	protected ThirdPersonCharacter thirdPersonCharacter;




	// temp variable for setting the boost of a single attack
	protected bool attackIsBoosted = false;
	protected float lastAddedBonus = 0.0f;
	protected float lastBaseBoost = 0.0f;

	// hold the references for the boost check
	protected LogicPlayer _player1 = null;
  protected LogicPlayer _player2 = null;
  protected LogicPlayer _currenBoostPlayerRef = null;
	//private bool isAlone = true;
	//private int combatEdge=0;

	bool debugHits = false;
	protected CharacterEngine character;

  [Tooltip("If TRUE the 'HumanColor' is applied start")]
  public bool UseColor = false;

  [Tooltip("The Color whichs will be applied")]
  public Color HumanColor;

  protected Color originalColor;

  [Tooltip("The Name of the child which holds the SkinedMeshRenderer")]
  public string CharacterMeshChildName = "EthanBody";
  protected Transform childWithMesh = null;
  protected Renderer characterMeshRenderer;

	public bool convertToRagdoll = false;
	public GameObject ragdollPrefab;


	// Needs to be here because of inheritance reasons -> Start() can't be used in logicNPC
	public CharacterEngineAI npcBehaviour;

	// playerNr
	// if player than 
	// public int playerNr=1;

	// health & faith 
	// (from base class)


	protected override void Awake()
	{
    base.Awake();

		character = GetComponent<CharacterEngine> ();
    npcBehaviour = GetComponent<CharacterEngineAI> ();
	  this.childWithMesh = this.transform.FindChild(CharacterMeshChildName);

	  if (this.childWithMesh != null)
	  {
	    
	    if (this.UseColor)
	    {
			characterMeshRenderer = childWithMesh.GetComponent<SkinnedMeshRenderer>();
		    SetColor(this.HumanColor);
	    }
	  } else {
	    Debug.LogWarning("Could fint child with Mesh for " + this.CharacterMeshChildName);
	  }

	  // register all character events
    	character.onCharacterHitBody += OnCharacterHitBody;
		character.onCharacterHitBlock += OnCharacterHitBlock;
		character.onCharacterHitWeapon += OnCharacterHitWeapon;
		character.onCharacterHitItem += OnCharacterHitItem;
		character.onCharacterDefend += OnCharacterDefend;
		character.onCharacterDamage += OnCharacterDamage;
		character.onCharacterSearch += OncharacterSearch;
		character.onCharacterWeaponSwitch += OnCharacterWeaponSwitch;
		character.onCharacterActionAttackFast += OnCharacterActionAttackFast;
		character.onCharacterActionAttackSlow += OnCharacterActionAttackSlow;
		character.onCharacterBlock += OnCharacterBlock;
		character.onImmovableDamage += OnImmovableDamage;
		character.onCharacterGodRage += OnCharacterGodRage;
        character.onCharacterActivate += OnCharacterActivate;
        character.onUsePotion += OnCharacterUsePotion;

	}

  protected virtual void Start()
  {
    originalColor = GetColor();
		thirdPersonCharacter = this.gameObject.GetComponent<ThirdPersonCharacter>();
  }

  // gets called when character hits antoher character
  protected virtual void OnCharacterHitBody (CharacterAttack attack)
	{
        if (attack.GetName() == "halberd.heavy")
        {
            AddNotification("[shake]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);

        }
        AddNotification("sound."+attack.GetName()+".body", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
        //string tmp = attack.GetName();
		if (debugHits) {
			Debug.Log ("[" + this.character.type + " hitted a body");
		}

	}

	public override void AddNotification( string key, string notificationArgument, Vector3 point, NotificationPriority np ) {
		
		gameLogic.AddNotification( key, notificationArgument, character.type.ToString() , point, np );
	}

	// gets called when another character defended 
	// your attack in block position
	void OnCharacterHitBlock (CharacterAttack attack)
	{
        if (attack.GetName()=="halberd.heavy")
        {
            AddNotification("[shake]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);

        }
        AddNotification("sound." + attack.GetName() + ".block", transform.position, NotificationPriority.ThrowAwayAfterProcessing);

        AddNotification("[block.hit]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);


		if (debugHits) {
			Debug.Log ("[" + this.character.type + " hitted a block");
		}

	}

	// gets called when character hits a weapon of enemy
	void OnCharacterHitWeapon (CharacterAttack attack)
	{
        if (attack.GetName() == "halberd.heavy")
        {
            AddNotification("[shake]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);

        }
        if (debugHits) {
			Debug.Log ("[" + this.character.type + " hitted a weapon");
		}
        AddNotification("sound." + attack.GetName() + ".block", transform.position, NotificationPriority.ThrowAwayAfterProcessing);

        AddNotification("[block.hit]", attack.GetCollision().contacts[0].point , NotificationPriority.ThrowAwayAfterProcessing);
	}

	// gets called when character hits an Item
	void OnCharacterHitItem (CharacterAttack attack)
	{
        if (attack.GetName() == "halberd.heavy")
        {
            AddNotification("[shake]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);

        }
        if (debugHits) {
		  Debug.Log ("[" + this.character.type + " hitted a item");
		}

        AddNotification("sound." + attack.GetName() + ".wood", transform.position, NotificationPriority.ThrowAwayAfterProcessing);

        AddNotification("[item.hit]", attack.GetCollision().contacts[0].point , NotificationPriority.ThrowAwayAfterProcessing);

	  if (debugHits)
	  {
	    // Process Item Hitting
	    Debug.Log("ITEMHITTING " + attack.GetCollision().gameObject.name);
	  }

	}

	// gets called when character defends an attack
	void OnCharacterDefend (CharacterAttack attack)
	{
		AddNotification ("[block.success]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);

		if (debugHits) {
			Debug.Log ("[" + this.character.type + " blocked an attack");
		}
	}

	// gets called when an character got hit
	protected virtual void OnCharacterDamage (CharacterAttack attack)
	{

		if (debugHits) {
			Debug.Log ("[" + this.character.type + " got attacked and got damage:" + attack.GetDamage ());
		}
		// add some particles here
		AddNotification ("[body.hit]", attack.GetCharacter ().type.ToString(), attack.GetCollision().contacts[0].point , NotificationPriority.ThrowAwayAfterProcessing);

		// example damage: 5
		AddHealth (-attack.GetDamage () * 0.05f);
		
		// Debug.Log("got attacked and got damage:"+attack.GetDamage()+" "+GetHealth());


	}


  // get all characters that are in your look radius
  protected virtual void OncharacterSearch(NearbyCharacters characters) {

    int friendcount, enemycount;
    friendcount = characters.friends.Count;
    enemycount = characters.enemies.Count;

    if (friendcount + enemycount == 0) {
      AddNotification("[being.alone]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
    } else {
      AddNotification("[people.around]", (friendcount - enemycount).ToString(), transform.position, NotificationPriority.ThrowAwayAfterProcessing);
    }
  //	if (friendcount + enemycount == 0) {
  //		if (isAlone == false) {
  //			isAlone = true;
  //			AddNotification ("[being.alone]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
  //		}
  //	} else {
  //		if (isAlone == true) {
  //			isAlone = false;
  //			AddNotification ("[people.around]", (friendcount - enemycount).ToString (), transform.position, NotificationPriority.ThrowAwayAfterProcessing);
  //		}
  //	}
  //	if (!(friendcount - enemycount == combatEdge)) {
  //		combatEdge=friendcount-enemycount;
  //		AddNotification("[edge.changed]", (friendcount - enemycount).ToString (), transform.position, NotificationPriority.ThrowAwayAfterProcessing);
  //	}

    CheckNearbyPlayers(friendcount, characters);
  }

  private void CheckNearbyPlayers(int friendcount, NearbyCharacters characters)
  {
    if (friendcount > 0 && this.character.type == CharacterType.NPC) {

      for(int i = 0; i < characters.friends.Count; i++)
      {
        if (characters.friends[i].type == CharacterType.Player1)
        {
          _player1 = characters.friends[i].GetComponent<LogicPlayer>();
        }

        if (characters.friends[i].type == CharacterType.Player2) {
          _player2 = characters.friends[i].GetComponent<LogicPlayer>();
        }

      }

    } else {
      _player1 = null;
      _player2 = null;
    }
  }

  void OnCharacterWeaponSwitch() {
    AddNotification("[weapon.switch]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
  }

	void OnCharacterActionAttackFast (Weapon attackingWeapon)
	{
		GetFightingSpirit ();
		AddNotification ("[fastattack." + attackingWeapon.name + "]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);

		CheckSimultanAttack ();
  }

	void OnCharacterActionAttackSlow (Weapon attackingWeapon)
	{
		GetFightingSpirit ();
		AddNotification ("[slowattack." + attackingWeapon.name + "]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
		CheckSimultanAttack ();
	}

  void OnCharacterBlock ()
	{
		AddNotification ("[block.start]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
	}

	void OnImmovableDamage (ImmovableAttack attack)
  {
	  AddHealth(-attack.GetImmovableDamageDealer().Damage);
  }

	public virtual void OnCharacterGodRage() {

	}

    public virtual void OnCharacterActivate()
    {


    }

    public virtual void OnCharacterUsePotion()
    {


    }




    public override void OnAddHealth (float healthDifference)
	{
		// (problem: coming directly from a prefab (logicbase)
		if (! (healthDifference < 0.001f && healthDifference > -0.001f) ) {
			if (healthDifference < 0.0f) {
				int health = (int)(healthDifference * 100.0f);
				AddNotification ("[changed.health]", "" + health, transform.position, NotificationPriority.ThrowAwayAfterProcessing);
			} else {
				int health = (int)(healthDifference * 100.0f);
				AddNotification ("[gained.health]", "+" + health, transform.position, NotificationPriority.ThrowAwayAfterProcessing);
				
			}
		} else {
			AddNotification ("[gained.health]", "Health full ", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
		}
	}

  // die? based on die
  public override void OnDie() {

    if (!isDead)
    {
      isDead = true;
      if (debugHits) {
        Debug.Log("[" + this.character.type + " OnDie()");
      }
      AddNotification("[" + this.character.type + ".death]", transform.position, NotificationPriority.Top);

      if (convertToRagdoll){
        InstantiateRagdoll();
      }

      this.GetCharacter().weaponManager.UnparentWeapons();
      this.GetCharacter().enabled = false;

      // hack: on player death restart level
      if (character.type == CharacterType.Player1 || character.type == CharacterType.Player2)
      {
        StartCoroutine(DelayedRestart());
      } else {
        Destroy(this.gameObject);
      }

    }

  }

  private IEnumerator DelayedRestart()
  {
    SkinnedMeshRenderer[] renderers = this.GetComponentsInChildren<SkinnedMeshRenderer>();
    foreach (var VARIABLE in renderers){
      VARIABLE.enabled = false;
    }

    Collider[] colls = this.GetComponentsInChildren<Collider>();
    foreach (var VARIABLE in colls) {
      VARIABLE.enabled = false;
    }

    Rigidbody[] rigids = this.GetComponentsInChildren<Rigidbody>();
    foreach (var VARIABLE in rigids) {
      VARIABLE.useGravity = false;
    }

    yield return new WaitForSeconds(3);
    gameLogic.ReloadLevel();
  }


  private void InstantiateRagdoll(){

		GameObject rD = Instantiate(ragdollPrefab, transform.position, transform.rotation) as GameObject;
    rD.GetComponent<Ragdoll>().timeToDelete = 60;

		Transform tempChildWithMesh = rD.transform.FindChild(CharacterMeshChildName);
		
		if (tempChildWithMesh != null)
		{
			if (this.UseColor)
			{
				SkinnedMeshRenderer tempCharacterMeshRenderer = tempChildWithMesh.GetComponent<SkinnedMeshRenderer>();
				Material tempMat; 
				if(tempCharacterMeshRenderer.materials.Length>1){
					tempMat= tempCharacterMeshRenderer.materials [1];
					tempMat.SetColor ("_Color", this.HumanColor);
				}
				else
				{
					tempMat= tempCharacterMeshRenderer.materials [0];
					tempMat.SetColor ("_EmissionColor", this.HumanColor);
				}
				//tempMat.SetColor ("_Color", this.HumanColor);

			}
		} else {
			Debug.LogWarning("Could fint child with Mesh for ragdoll of " + this.CharacterMeshChildName);
		}
	}

  /// <summary>
  /// Check if friends (which are players) are attacking and boost myself!
  /// </summary>
  protected virtual void CheckSimultanAttack()
  {
    //override in subclasses!
    throw new NotImplementedException();
  }

  protected void boostMyAttack(LogicPlayer player){
    if (!attackIsBoosted) {
			lastBaseBoost = player.BoostAttackAddition;
			lastAddedBonus = player.BoostAttackAddition * (currentDamageModification);
			this.character.baseAttackDamage += lastAddedBonus;
			this.attackIsBoosted = true;
			_currenBoostPlayerRef = player;
		}
  }

  protected void unBoostMyAttack(LogicPlayer player) {
    if (attackIsBoosted)
    {
      this.character.baseAttackDamage -= player.BoostAttackAddition;
      this.attackIsBoosted = false;
      _currenBoostPlayerRef = null;
    }
  }

  public CharacterEngine GetCharacter()
  {
    return this.character;
  }

  protected virtual void FixedUpdate ()
	{
		if (!this.character.currentAttack.IsRunning()
      && this.character.GetState () == CharacterState.Grounded
      && attackIsBoosted) {
			unBoostMyAttack (_currenBoostPlayerRef);
		}
	}

  protected virtual void SetColor(Color color)
  {
    if (this.characterMeshRenderer != null) 
	  {
		  Material mat = this.characterMeshRenderer.materials [0];
		  mat.SetColor ("_Color", color);
		  mat.SetColor ("_EmissionColor", color);
	  }

  }

  protected virtual Color GetColor(int index = 0) {
	  if (this.characterMeshRenderer != null) 
	  {
		    Material mat = this.characterMeshRenderer.materials[index];
	      return mat.GetColor("_Color");
	  }
		return Color.black;
  }



	protected virtual void GetFightingSpirit(){
		if (character.type != CharacterType.NPCEnemy) {
			if(fightingSpiritBoostsMovementSpeed){
				thirdPersonCharacter.m_MoveSpeedMultiplier=gameLogic.FightingSpirit*fightingSpiritMovementModifier;
			}
			if (fightingSpiritBoostsDamage) {
				float addToAttack = 0;
				currentDamageModification = 1 + fightingSpiritDamageModifier * (gameLogic.FightingSpirit - 1);

				if (attackIsBoosted) {
					character.baseAttackDamage = character.baseAttackDamage - lastAddedBonus;
					addToAttack = lastBaseBoost * currentDamageModification;
					lastAddedBonus = addToAttack;
				}
				character.baseAttackDamage = currentDamageModification + addToAttack;
			}
			if (fightingSpiritBoostsAttackSpeed) {
                currentSpeedModification = (gameLogic.FightingSpirit) * fightingSpiritAttackSpedModifier; //only when fightingspirit is used / 2;
				character.baseAttackSpeed = currentSpeedModification;

			}
		}
	}

  void OnGUI ()
	{

		// GUI.Label( new Rect(0,20,200,20), "H:"+GetHealth (),gui);
		if (Camera.main != null && gameLogic.showStats) {
			Vector3 screenPos = Camera.main.WorldToScreenPoint (this.transform.position + new Vector3 (0.0f, 2.5f, 0.0f));
			GUI.Label (new Rect (screenPos.x, Screen.height - screenPos.y, 200, 20), GetStateDebug (), guiDebug);
		}
	}

}
