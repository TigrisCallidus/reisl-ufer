﻿using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework;

public class arrowscript : MonoBehaviour {

	public CharacterEngine charactertarget;
	public float speed = 1;
	public float damage = 1;
	public Vector3 target;
	private bool hitground = false;
	public float removetime = 3;
	private float removecount = 0;
	public bool debugmode = false;

	// Use this for initialization
	void Start () {
		this.transform.LookAt(target);
	}

	void Fly (){
		//this.transform.Translate (-this.transform.forward * speed * Time.smoothDeltaTime);
		if(hitground != true){
			this.GetComponent<Rigidbody> ().AddForce(this.transform.forward * speed);
			//this.GetComponent<Rigidbody> ().MovePosition (Vector3.MoveTowards (this.GetComponent<Rigidbody> ().position, target, speed * Time.smoothDeltaTime));
		}
	}
	
	// Update is called once per frame
	void Update () {
		Fly ();
		if(hitground == true){
			removecount += Time.smoothDeltaTime;
			if (removetime == removecount) {
				Destroy(this.gameObject);
			}

		}
	}
	void OnCollisionEnter(Collision collision) {
		if (debugmode == true) {
			Debug.Log ("arrow collision: " + collision.gameObject);
		}
		if (hitground != true && collision != null && collision.gameObject == charactertarget.gameObject) {
			charactertarget.GetComponent<LogicHuman>().AddHealth (-damage);
			Destroy (this.gameObject);
		} else if (collision != null) {
			hitground = true;
			this.GetComponent<Rigidbody>().velocity = Vector3.zero;
		  this.gameObject.isStatic = true;
		}
	}
}
  