﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameLab.HackAndSlashFramework;
using GameLab.NotficationCenter;

public class LogicPlayer : LogicHuman
{


  [Tooltip("The Addition which is added when a player attacks when NPC 'friends' are attacking")]
  public float BoostAttackAddition = 0.25f;

  [Tooltip("How long since the Start of an attack is it possbile to Trigger the Boost")]
  //public float BoostTriggerDuration = 0.2f;
  public int BoostTriggerDuration = 500;

  [Tooltip("The Color the players should get when boosted")]
  public Color boostColor;


	[Tooltip("Attack speed you get during Godly Rage.")]
	public float rageAttackSpeedModifier=1;
	public bool rageBoostsAttackSpeed = true;
	
	[Tooltip("Attack damage you get during Godly Rage.")]
	public float rageAttackDamageModifier=1;
	public bool rageBoostsAttackDamage = true;
	
	[Tooltip("Movement speed you get during Godly Rage.")]
	public float rageMovementSpeedModifier=1;
	public bool rageBoostsMovementSpeed = true;

	public bool godlyRage=false;

	public float godlyRageDuration=20f;
	public CharacterEngine[] logicPlayers;

	public GameObject HolyRage;

	protected override void Start() {
		base.Start();
		logicPlayers = GetFriendsFromCharacters (GameObject.FindObjectsOfType<CharacterEngine> ());
		IgnoreCollisionWithFriends (logicPlayers);
        this.money = SceneStateManager.instance.money;
        this.numberOfPotions = SceneStateManager.instance.numberOfHealthpots;

    }

	CharacterEngine[] GetFriendsFromCharacters(CharacterEngine[] chars) {
		List<CharacterEngine> res = new List<CharacterEngine> ();
		for (int i = 0; i < chars.Length; i++) {
			if(chars[i].type == CharacterType.NPC || chars[i].type == CharacterType.Player1 || chars[i].type == CharacterType.Player2) {
				res.Add(chars[i]);
			}
		}

		return res.ToArray();
	}


	void IgnoreCollisionWithFriends(CharacterEngine[] Friends) {
		GameObject A, B;
		for(int i = 0; i < Friends.Length;i++) {
			for(int u = 0; u < Friends.Length;u++) {
				A = Friends[i].gameObject;
				B = Friends[u].gameObject;

				Friends[i].collisionManager.blockCollider.enabled = true;
				Friends[u].collisionManager.blockCollider.enabled = true;

				Collider[] ColA = A.GetComponentsInChildren<Collider>();
				Collider[] ColB = B.GetComponentsInChildren<Collider>();
				if(!A.Equals(B)) {
					Physics.IgnoreCollision(A.GetComponent<Collider>(),B.GetComponent<Collider>(),true);

					for(int t = 0;t < ColA.Length;t++) {
						for(int z = 0;z < ColB.Length;z++) {
							if(ColA[t].enabled && ColB[z].enabled) {
								Physics.IgnoreCollision(ColA[t],ColB[z],true);
								//Debug.Log ("Collision ignored between "+ColA[t].gameObject.name+ " and " +ColB[z].gameObject.name);
							}

						}	
					}
				}

				Friends[i].collisionManager.blockCollider.enabled = false;
				Friends[u].collisionManager.blockCollider.enabled = false;

			}
		}
	}

  //
  //	bool debugHits = false;
  //
  //	CharacterEngine character;
  //
  //	// playerNr
  //	// if player than 
  //	// public int playerNr=1;
  //
  //	// health & faith 
  //	// (from base class)
  //
  //
  //	void Start () {
  //
  //
  //		character = GetComponent<CharacterEngine>();
  //
  //
  //		// register all character events
  //		character.onCharacterHitBody += OnCharacterHitBody;
  //		character.onCharacterHitBlock += OnCharacterHitBlock;
  //		character.onCharacterHitWeapon += OnCharacterHitWeapon;
  //		character.onCharacterHitItem += OnCharacterHitItem;
  //		character.onCharacterDefend += OnCharacterDefend;
  //		character.onCharacterDamage += OnCharacterDamage;
  //		character.onCharacterSearch += OncharacterSearch;
  //		character.onCharacterWeaponSwitch += OnCharacterWeaponSwitch;
  //		character.onCharacterActionAttackFast +=OnCharacterActionAttackFast;
  //		character.onCharacterActionAttackSlow +=OnCharacterActionAttackSlow;
  //
  //	}
  //
  //
  //	// gets called when character hits antoher character
  //	void OnCharacterHitBody(CharacterAttack attack) {
  //
  //		if (debugHits) {
  //			Debug.Log ("[player]hitted a body");
  //		}
  //
  //
  //	}
  //
  //
  //	// gets called when another character defended 
  //	// your attack in block position
  //	void OnCharacterHitBlock(CharacterAttack attack) {
  //
  //		if (debugHits) {
  //			Debug.Log ("[player] hitted a block");
  //		}
  //
  //
  //	}
  //
  //
  //	// gets called when character hits a weapon of enemy
  //	void OnCharacterHitWeapon(CharacterAttack attack) {
  //
  //		if (debugHits) {
  //			Debug.Log ("[player] hitted a weapon");
  //		}
  //
  //	}
  //
  //
  //	// gets called when character hits an Item
  //	void OnCharacterHitItem(CharacterAttack attack) {
  //	
  //		//if (debugHits) {
  //			Debug.Log ("[player] hitted a item");
  //		//}
  //		AddNotification ("[block.hit]", transform.position , NotificationPriority.ThrowAwayAfterProcessing); 
  //
  //		// Process Item Hitting
  //		Debug.Log ("ITEMHITTING "+attack.GetCollision().gameObject.name);
  //
  //
  //	}
  //
  //
  //	// gets called when character defends an attack
  //	void OnCharacterDefend(CharacterAttack attack) {
  //		AddNotification ("[block.hit]", transform.position , NotificationPriority.ThrowAwayAfterProcessing); 
  //
  //		if (debugHits) {
  //			Debug.Log ("[player] blocked an attack");
  //		}
  //
  //	}
  //
  //
  //	// gets called when an character got hit
  //	void OnCharacterDamage(CharacterAttack attack, string attackerName) {
  //		
  //		if (debugHits) {
  //			Debug.Log ("[player]got attacked and got damage:" + attack.GetDamage ());
  //		}
  //
  //		// add some particles here
  //		AddNotification ("[body.hit]",attackerName , transform.position , NotificationPriority.ThrowAwayAfterProcessing); 
  //
  //		// example damage: 5
  //		AddHealth ( - attack.GetDamage() * 0.05f );
  //
  //		// Debug.Log("got attacked and got damage:"+attack.GetDamage()+" "+GetHealth());
  //
  //
  //	}
  //
  //
  //	// get all characters that are in your look radius
  //	void OncharacterSearch(NearbyCharacters characters) {
  //		int friendcount, enemycount;
  //		friendcount = characters.friends.Count;
  //		enemycount = characters.enemies.Count;
  //
  //
  //	}
  //

  void OnCharacterWeaponSwitch() {
    AddNotification("[weapon.switch]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
  }

  void OnCharacterActionAttackSlow(Weapon attackingWeapon) {
    AddNotification("[slowattack." + attackingWeapon.name + "]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
    CheckSimultanAttack();
  }

  void OnCharacterActionAttackFast(Weapon attackingWeapon) {
    AddNotification("[fastattack." + attackingWeapon.name + "]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
    CheckSimultanAttack();
  }



	public override void OnAddFaith (float faithDifference)
	{
		if (faithDifference != 0) {
			AddNotification ("[changed.faith]", (faithDifference * 100).ToString (), transform.position, NotificationPriority.ThrowAwayAfterProcessing);
		}
		if (Faith == 1) {
			AddNotification("[faith.full]", faithDifference.ToString(), transform.position, NotificationPriority.ThrowAwayAfterProcessing);
			//Dont activate automatically
			//ActivateGodlyRage();
		}
	}

	// used this ...
	public override void OnAddMoney (int addsubMoney, int moneyTotal)
	{
		if (addsubMoney != 0) {
			if (addsubMoney < 0) {
				AddNotification ("[changed.money]", "" + addsubMoney +"", this.gameObject.transform.position, NotificationPriority.ThrowAwayAfterProcessing);
			} else {
				AddNotification ("[changed.money]", "+" + addsubMoney +"", this.gameObject.transform.position, NotificationPriority.ThrowAwayAfterProcessing);
			}
		}

	}

  /// <summary>
  /// Check if friends (which are players) are attacking and boost myself!
  /// </summary>
  protected override void CheckSimultanAttack()
  {
    for (int i = 0; i < this.character.GetNearbyCharacters().friends.Count; i++)
    {
      CharacterEngine friend = this.character.GetNearbyCharacters().friends[i];

      if (friend.currentAttack.IsRunning()
        && friend.currentAttack.GetDuration() < this.BoostTriggerDuration)
      {
        //Debug.Log("Simultan Attack " + friend.currentAttack.GetDuration() + " < " + this.BoostTriggerDuration + " ?");

        if (friend.type == CharacterType.NPC
            || friend.type == CharacterType.Player1
            || friend.type == CharacterType.Player2)
        {
          boostMyAttack(this);
          AddNotification(NotificationCenter.TYPE_SOUND + NotificationCenter.EVENT_HIT_BOOST, this.transform.position , NotificationPriority.Top);

          break;
        }
      }
    }
  }

  //	// die? based on die
  //	public override void OnDie() {
  //
  //		if (debugHits) {
  //			Debug.Log ("[player] OnDie()");
  //		}
  //		AddNotification ("[player.death]", transform.position , NotificationPriority.Top); 
  //		Destroy (this.gameObject);
  //
  //	}
  //

  public void UpdateStats(float health, float faith, int money) {

//    if (this.Health < 1.0f) {
//      this.Health += ((this.Health + health) > 1.0f) ? 1.0f - this.Health : health;
//    }
//    if (this.Faith < 1.0f) {
//      this.Faith += ((this.Faith + faith) > 1.0f) ? 1.0f - this.Faith : faith;
//    }
//    this.Money += money;

		AddHealth (health);
		AddFaith (faith);
		AddMoney (money);
  }


	protected override void OncharacterSearch (NearbyCharacters characters){
		base.OncharacterSearch (characters);
		if (this.character.type == CharacterType.Player1) {
			gameLogic.player1Friends=characters.friends.Count;
		}
		if (this.character.type == CharacterType.Player2) {
			gameLogic.player2Friends=characters.friends.Count;
		}
	}

	protected override void OnCharacterDamage (CharacterAttack attack)
	{
		base.OnCharacterDamage (attack);
		gameLogic.AddBonusFightingSpirit(-gameLogic.fightingSpiritOnPlayerHit);
		if (this.Health <= 0.25) {
			AddNotification ("[damage.taken]", transform.position , NotificationPriority.ThrowAwayAfterProcessing);		
		}
	}

	protected override void OnCharacterHitBody (CharacterAttack attack)
	{
		base.OnCharacterHitBody (attack);
		if(attackIsBoosted){
			gameLogic.AddBonusFightingSpirit(gameLogic.fightingSpiritTeamhitPlayer);
		}
		
	}

	public override void OnCharacterGodRage() {
		if (Faith >= 1 && !godlyRage) {
			ActivateGodlyRage ();
		}
	}

    public override void OnCharacterActivate()
    {
        AddNotification("[activate.pressed]", transform.position, NotificationPriority.ThrowAwayAfterProcessing);
    }

    public int numberOfPotions = 2;
    public float healthPerPotion = 20; 
    public override void OnCharacterUsePotion()
    {
        if (numberOfPotions > 0)
        {
            if (Health < MaxHealth) {
            numberOfPotions -= 1;
        }
            AddHealth( healthPerPotion/100);
        }
    }

    public void ActivateGodlyRage(){
		AddFaith (MaxFaith);
		HolyRage.SetActive(true);
		godlyRage = true;
		StartCoroutine (GodlyRageTimeUp (godlyRageDuration));
	}

	public void DeactivateGodlyrage(){
		//AddFaith (-Faith+MaxFaith*3/4);
        AddFaith(-Faith);
        HolyRage.SetActive(false);
		godlyRage = false;
	}

	IEnumerator isRunning(float waitTime, Vector3 oldPosition) {
		yield return new WaitForSeconds(waitTime);
		if (oldPosition != this.transform.position) {
			AddNotification("[moving]",this.transform.position,NotificationPriority.ThrowAwayAfterProcessing);
		}

		isRunning (waitTime, this.transform.position);
	}

	IEnumerator GodlyRageTimeUp(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		DeactivateGodlyrage ();
	}
	//
  protected override void FixedUpdate()
  {
    base.FixedUpdate();

    if (this.attackIsBoosted) {
      SetColor(boostColor);
    } else {
      SetColor(originalColor);
    }
		if (character.state == CharacterState.Grounded) {
			character.GetAnimator ().speed = fightingSpiritMovementModifier;
		} else {
			character.GetAnimator ().speed = 1.0f;
		}
  }

  protected override Color GetColor(int index = 0) {
    if (this.characterMeshRenderer != null) {
      Material mat = this.characterMeshRenderer.materials[index];
      return mat.GetColor("_Color");
    }
    return Color.black;
  }

  protected override void SetColor(Color color) {
    if (this.characterMeshRenderer != null) {
      Material mat = this.characterMeshRenderer.materials[0];
      mat.SetColor("_Color", color);
    }

  }

  protected override void GetFightingSpirit(){
		
		float rageMoveSpeed = rageMovementSpeedModifier;
		float rageAttackSpeed = rageAttackSpeedModifier;
		float rageAttackDamage = rageAttackDamageModifier;

		if(!rageBoostsMovementSpeed || !godlyRage){
			rageMoveSpeed=1;
		}
		if(!rageBoostsAttackSpeed|| !godlyRage){
			rageAttackSpeed=1;
		}
		if(!rageBoostsAttackDamage|| !godlyRage){
			rageAttackDamage=1;
		}		
		
		if (character.type != CharacterType.NPCEnemy) {
			if(fightingSpiritBoostsMovementSpeed){
				currentMovementModification=gameLogic.FightingSpirit*fightingSpiritMovementModifier*rageMoveSpeed;
				thirdPersonCharacter.m_MoveSpeedMultiplier=currentMovementModification;
			}
			if (fightingSpiritBoostsDamage) {
				float addToAttack = 0;
				currentDamageModification = (1 + fightingSpiritDamageModifier * (gameLogic.FightingSpirit - 1))*rageAttackDamage;
				
				if (attackIsBoosted) {
					character.baseAttackDamage = character.baseAttackDamage - lastAddedBonus;
					addToAttack = lastBaseBoost * currentDamageModification*rageAttackDamage;
					lastAddedBonus = addToAttack;
				}
				character.baseAttackDamage = currentDamageModification + addToAttack;
			}
			if (fightingSpiritBoostsAttackSpeed) {
                currentSpeedModification = rageAttackSpeed * (gameLogic.FightingSpirit) * fightingSpiritAttackSpedModifier;// only when fighting spirit is used/ 2;
				character.baseAttackSpeed = currentSpeedModification;
				
			}
		}

		//print (character.baseAttackDamage);
	}

  //
  //	void OnGUI() {
  //
  //		// GUI.Label( new Rect(0,20,200,20), "H:"+GetHealth (),gui);
  //		Vector3 screenPos = Camera.main.WorldToScreenPoint(this.transform.position+new Vector3(0.0f,2.5f,0.0f));
  //		GUI.Label( new Rect(screenPos.x,Screen.height-screenPos.y,200,20), GetStateDebug(),guiDebug);
  //
  //	}

}
