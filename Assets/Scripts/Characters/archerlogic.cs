﻿using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework;

public class archerlogic : MonoBehaviour {

	private CharacterEngine target;
	public bool debugmode = true;
	private Vector3 limitrotation;
	public float drawingspeed = 3;
	private float drawingcount;
	public float arrowspeed = 10;
  public float arrowDamage = 0.2f;

	//accuracy from 0-1
	public float accuracy =1;
	public Vector3 offset;
	public float forwardoffset = 1;
	public GameObject prefabarrow;
	private CharacterEngine charengine;


	public float searchenemytimer;
	private float searchenemycounter;

	// Use this for initialization
	void Start () {
		charengine = this.GetComponent<CharacterEngine> ();

	}
	
	// Update is called once per frame
	void Update () {

		LookAtPlayer ();

		if (target != null) {
			drawingcount += Time.smoothDeltaTime;

			if (drawingcount >= drawingspeed) {
				ShootArrow ();
				drawingcount = 0;
			}
		}

	}

	void LookAtPlayer () {

		if(target != null){
			limitrotation = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
			this.transform.LookAt(limitrotation);
		}

	}

	void ShootArrow (){
		Vector3 pos = this.transform.position + offset + this.transform.forward * forwardoffset;
		GameObject arrow = Instantiate (prefabarrow,pos,Quaternion.identity) as GameObject;
		arrow.transform.forward = this.transform.forward; 
		arrow.GetComponent<arrowscript> ().target = target.transform.position + offset + new Vector3(Random.Range(-accuracy, accuracy), Random.Range(0, accuracy), Random.Range(0, accuracy));
		arrow.GetComponent<arrowscript> ().charactertarget = target;
	  arrow.GetComponent<arrowscript>().speed = this.arrowspeed;
    arrow.GetComponent<arrowscript>().damage = this.arrowDamage;
    
    if (debugmode)
	  {
	    Debug.Log("shot " + arrow + " speed " + arrow.GetComponent<arrowscript>().speed);
	  }
	}

	void FixedUpdate () {

		if (debugmode == true) {
			//Debug.Log ("player1: " + target);
		}

		searchenemycounter += Time.fixedDeltaTime;
		if (searchenemycounter >= searchenemytimer){

			FindTarget();
			searchenemycounter = 0;
		}
	}

	void FindTarget(){
		target = null;

		if (charengine.GetNearbyCharacters().enemies.Count > 0){
				target = charengine.GetNearbyCharacters().enemies[Random.Range(0, charengine.GetNearbyCharacters().enemies.Count)];
		}

	}

}
