﻿using UnityEngine;
using System.Collections;

public class Ragdoll : MonoBehaviour
{

  public float timeToDelete = 30;
	// Use this for initialization
	void Start () {
		
		StartCoroutine(DisableColliders());
		StartCoroutine(DestroyMe(timeToDelete));
	
	}
	IEnumerator DisableColliders(){
		yield return new WaitForSeconds (6);
		Rigidbody[] rigidbodies = transform.GetComponentsInChildren<Rigidbody>();
		foreach (Rigidbody r in rigidbodies){
			r.isKinematic = true;
		}
		Collider[] colliders = transform.GetComponentsInChildren<Collider>();
		foreach (Collider c in colliders){
			c.enabled = false;
		}

	}

	IEnumerator DestroyMe(float timeToDelete) {
		yield return new WaitForSeconds (timeToDelete);
		Destroy(gameObject);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
