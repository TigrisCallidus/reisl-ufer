﻿using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework;
using GameLab.NotficationCenter;

// ? more enemies?
public enum BehaviourMode
{
	Aggressive,
	Defensive,
	Passive,
	Supportive,
	Balanced,
	AttackBlock,
	PartiallyAttack
}
public class  LogicNPC : LogicHuman
{

  public bool ShowBoostHight = false;

  [Tooltip("Color which the friendlies NPC should have when a simultanAttack is possible")]
  public Color boostHighlightColor;

//	bool debugHits = false;
//
//    CharacterEngine character;
//	public bool isFriend;
//
//
//	void Start () {
//
//        character = GetComponent<CharacterEngine>();
//
//        // register all character events
//        character.onCharacterHitBody += OnCharacterHitBody;
//        character.onCharacterHitBlock += OnCharacterHitBlock;
//        character.onCharacterHitWeapon += OnCharacterHitWeapon;
//        character.onCharacterHitItem += OnCharacterHitItem;
//        character.onCharacterDefend += OnCharacterDefend;
//        character.onCharacterDamage += OnCharacterDamage;
//        character.onCharacterSearch += OncharacterSearch;
//		character.onCharacterWeaponSwitch += OnCharacterWeaponSwitch;
//		character.onCharacterActionAttackFast +=OnCharacterActionAttackFast;
//		character.onCharacterActionAttackSlow +=OnCharacterActionAttackSlow;
//		if (character.type == CharacterType.NPC) {
//			isFriend=true;
//		}
//
//	}
//
//    // gets called when character hits antoher character
//    void OnCharacterHitBody(CharacterAttack attack) {
//
//		if (debugHits) {
//			Debug.Log ("[npc] hitted a body");
//		}
//
//
//    }
//
//    // gets called when another character defended 
//    // your attack in block position
//    void OnCharacterHitBlock(CharacterAttack attack) {
//
//		if (debugHits) {
//			Debug.Log ("[npc] hitted a block");
//		}
//    }
//
//    // gets called when character hits a weapon of enemy
//    void OnCharacterHitWeapon(CharacterAttack attack) {
//
//		if (debugHits) {
//			Debug.Log ("[npc] hitted a weapon");
//		}
//
//    }
//
//    // gets called when character hits an Item
//    void OnCharacterHitItem(CharacterAttack attack) {
//
//		// if (debugHits) {
//			Debug.Log ("[npc] hitted a item");
//		// }
//
//    }
//
//    // gets called when character defends an attack
//    void OnCharacterDefend(CharacterAttack attack) {
//
//		if (debugHits) {
//			Debug.Log ("[npc] blocked an attack");
//		}
//    }
//
//    // gets called when an character got hit
//    void OnCharacterDamage(CharacterAttack attack, string name) {
//        
//		if (debugHits) { 
//			Debug.Log ("[npc] got attacked and got damage:" + attack.GetDamage ());
//		}
//
//		// add some particles here
//
//		AddNotification ("[body.hit]", name,  attack .GetCollision().contacts[0].point , NotificationPriority.ThrowAwayAfterProcessing); 
//
//		// example damage: 5
//		AddHealth ( - attack.GetDamage() * 0.05f );
//		// Debug.Log("got attacked and got damage:"+attack.GetDamage()+" "+GetHealth());
//
//
//    }
//
//	void OnCharacterWeaponSwitch(){
//		AddNotification ("[weapon.switch]", transform.position , NotificationPriority.ThrowAwayAfterProcessing);
//	}
//	
//	void OnCharacterActionAttackSlow(Weapon attackingWeapon){
//		AddNotification ("[slowattack." +attackingWeapon.name +"]", transform.position , NotificationPriority.ThrowAwayAfterProcessing);
//	}
//	
//	void OnCharacterActionAttackFast(Weapon attackingWeapon){
//		AddNotification ("[fastattack." +attackingWeapon.name +"]", transform.position , NotificationPriority.ThrowAwayAfterProcessing);
//	}
//
//    // get all characters that are in your look radius
//    void OncharacterSearch(NearbyCharacters characters) {
//		int friendcount, enemycount;
//		friendcount = characters.friends.Count;
//		enemycount = characters.enemies.Count;
//
//    }
//
//	// die? based on die
//	public override void OnDie() {
//
//		if (debugHits) { 
//		
//			Debug.Log ("[npc] OnDie()");
//		
//		}
//
//		Destroy (this.gameObject);
//		if (!isFriend) {
//			AddNotification ("[enemy.death]", transform.position, NotificationPriority.Top); 
//		} else {
//			AddNotification ("[friend.death]", transform.position, NotificationPriority.Top);
//		}
//
//
//	}
//
//	void FixedUpdate () {
//
//	}
//
//	void OnGUI() {
//
//		Vector3 screenPos = Camera.main.WorldToScreenPoint(this.transform.position+new Vector3(0.0f,2.5f,0.0f));
//		GUI.Label( new Rect(screenPos.x,Screen.height-screenPos.y,200,20), GetStateDebug(),guiDebug);
//
//	}

	/*public BehaviourMode behaviourMode;
	public float minWaitTimeForBehaviourSwitch = 100000f;
	
	private float timeSinceBehaviourSwitch;
	private bool readyForSwitch = true;
	private int counter = 0;

	void Update()
	{
		if ((Time.time - timeSinceBehaviourSwitch) > minWaitTimeForBehaviourSwitch)
		{
			readyForSwitch = true;
		}
		if (npcBehaviour != null && readyForSwitch)
		{
			//Debug.Log("Behaviour switched!");
			switch (behaviourMode)
			{
				case BehaviourMode.Aggressive:
					AggressiveMode();
					break;

				case BehaviourMode.Defensive:
					DefensiveMode();
					break;

				case BehaviourMode.Passive:
					// Passive behaviour method (Patrolling?)
					break;

				case BehaviourMode.Supportive:
					// Supportive behaviour method
					break;

				case BehaviourMode.Balanced:
					// Balanced behaviour method
					break;

				case BehaviourMode.AttackBlock:
					SequentialMode(AIBehaviour.Attack, AIBehaviour.Block);
					break;

				case BehaviourMode.PartiallyAttack:
					SequentialMode(AIBehaviour.Attack, AIBehaviour.FightIdle);
					break;
			}
		}
	}

	public void SwitchBehaviourMode(BehaviourMode mode)
	{
		behaviourMode = mode;
	}

	private void AggressiveMode()
	{
		npcBehaviour.behaviour = AIBehaviour.Attack;
		timeSinceBehaviourSwitch = Time.time;
		readyForSwitch = false;
	}

	private void DefensiveMode()
	{
		npcBehaviour.behaviour = AIBehaviour.Block;
		timeSinceBehaviourSwitch = Time.time;
		readyForSwitch = false;
	}

	private void SupportiveMode()
	{
		
	}

	private void SequentialMode(params AIBehaviour[] desiredBehaviour)
	{
		npcBehaviour.behaviour = desiredBehaviour[counter];

		counter = (counter < desiredBehaviour.Length - 1) ? ++counter : 0;

		timeSinceBehaviourSwitch = Time.time;
		readyForSwitch = false;
	}

	private void AlternateMode(params AIBehaviour[] desiredBehaviours)
	{
		int randomIndex = Random.Range(0, desiredBehaviours.Length);
		npcBehaviour.behaviour = desiredBehaviours[randomIndex];

		timeSinceBehaviourSwitch = Time.time;
		readyForSwitch = false;
	}
	*/

	protected override void OnCharacterDamage (CharacterAttack attack)
	{
		base.OnCharacterDamage (attack);
		if (character.type == CharacterType.NPC) {
			gameLogic.AddBonusFightingSpirit(-gameLogic.fightingSpiritOnNPCHit);
		}
	}

	protected override void OnCharacterHitBody (CharacterAttack attack)
	{
		base.OnCharacterHitBody (attack);
		if (character.type == CharacterType.NPC && attackIsBoosted) {
			gameLogic.AddBonusFightingSpirit(gameLogic.fightingSpiritTeamhitNPC);
		}
	}


  /// <summary>
  /// Check if friends (which are players) are attacking and boost myself!
  /// </summary>
  protected override void CheckSimultanAttack()
  {
    if (_player1 != null)
    {
      if (_player1.GetCharacter().currentAttack.IsRunning()
        && _player1.GetCharacter().currentAttack.GetDuration() < _player1.BoostTriggerDuration) {
        boostMyAttack(_player1);
      }
    }

    if (_player2 != null)
    {
      if (_player2.GetCharacter().currentAttack.IsRunning()
        && _player2.GetCharacter().currentAttack.GetDuration() < _player2.BoostTriggerDuration) {
        boostMyAttack(_player2);
      }
    }

  }


  protected override void FixedUpdate()
  {
    base.FixedUpdate();

    if (ShowBoostHight)
    {
      try
      {

        if (this.character.type == CharacterType.NPC)
        {
          if (this.character.currentAttack.IsRunning()
              && this.character.currentAttack.GetDuration() < this._player1.BoostTriggerDuration)
          {
            SetColor(this.boostHighlightColor);
          }
          else
          {
            SetColor(this.originalColor);
          }
        }
      }
      catch
      {
        Debug.LogWarning("LogicNPC: could not color character " + gameObject.name);
      }
    }

  }

  protected override Color GetColor(int index = 0) {
    if (this.characterMeshRenderer != null) {
      Material mat = this.characterMeshRenderer.materials[index];

      return mat.GetColor("_EmissionColor");
    }
    return Color.black;
  }

  protected override void SetColor(Color color) {
    if (this.characterMeshRenderer != null) {
			Material mat;
			if(this.characterMeshRenderer.materials.Length > 1)
			{
				mat = this.characterMeshRenderer.materials [1];
				mat.SetColor("_Color", color);
			}
			else
			{
				mat = this.characterMeshRenderer.materials [0];
				mat.SetColor("_EmissionColor", color);
			}
		/*mat.EnableKeyword ("_EMISSION");
	    mat.SetColor("_EmissionColor", color);
		mat.SetColor("_EmissionMapUI", color);*/
		

    }

  }

}
