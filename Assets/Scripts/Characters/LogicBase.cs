﻿using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework;
using GameLab.NotficationCenter;
//using UnityEditor;

// priority
public enum CollectEventType {
	OnHit,
	OnWalkOver
}

public class LogicBase : MonoBehaviour {

	/*
	 * name
	 * 
	 * */
	// 
	public string avatarName = "anonymous";

	// inventory health and faith (faith to release)
	public float collectHealth = 0.0f; // food
	public float collectFaith = 0.0f;  // ...
	public int collectMoney = 0;

	/*
	 * state of the object
	 * 
	 * */

	// health & faith
	public float health = 1.0f;
  public float MaxHealth = 1.0f;

  public float Health {
		get {
			return health;
		}
		set {
			SetHealth(value);
		}
	}

	public float faith = 0.5f;
  public float MaxFaith = 1.0f;

  public float Faith {
		get {
			return faith;
		}
		set {
			SetFaith( value);
		}
	}

	[Tooltip("Sets the maximal protection you get, for full faith. The value means damage mitigation in % so 20 means 20% less damage taken.")]
	public float maxProtection=0.0f;

	public GroundType groundType;

  public int money = 0;

	public int Money {
		get {
			return money;
		}
		set {
			setMoney(value);
		}
	}

	/*
	 * plunder-collectables (if you destroy this object)
	 * 
	 * */
	CollectEventType collectOnEventType = CollectEventType.OnHit;

	// inventory items (things to plunder)
	//ArrayList collectItems = new ArrayList();

	protected GameLogic gameLogic;

	// debug style
	public GUIStyle guiDebug;

  protected virtual void Awake()
  {
    this.gameLogic = GameObject.FindObjectOfType<GameLogic>();
    //Debug.Log("me " + this + " gamelogic " + this.gameLogic);
  }

	/*
	 * Debug
	 * 
	 * */
	public string GetStateDebug() {

		return ""+avatarName+"\nH:" + Health + " M:" + Money + " F: "+ Faith +"\n["+collectOnEventType.ToString()+" H:"+collectHealth+" F:"+collectFaith+"]"; 
	}
  

  /*
	 * CheckPaused
	 * 
	 * */
  public bool CheckPaused() {

		return this.gameLogic.CheckPaused( );
		
	}

	/*
	 * AddNotification
	 * 
	 * */
	public void AddNotification( string key, Vector3 point, NotificationPriority np ) {
		
		AddNotification( key, "", point, np );
		
	}

	public virtual void AddNotification( string key, string notificationArgument, Vector3 point, NotificationPriority np ) {

		gameLogic.AddNotification( key, notificationArgument, this.name , point, np );
	}

	public void PlayFootStep()
	{
		switch(groundType)
		{
			case GroundType.Dirt:
				AddNotification("sound.footstep.dirt", this.transform.position, NotificationPriority.ThrowAwayAfterProcessing);
				break;
			case GroundType.Water:
				AddNotification("sound.footstep.water", this.transform.position, NotificationPriority.ThrowAwayAfterProcessing);
				break;

		}
	}


	/*
	 * Money
	 * 
	 * */
	public void AddMoney( int addsubMoney ) {

		money = money + addsubMoney;
		OnAddMoney ( addsubMoney, Money );

	}

	void setMoney (int newMoney)
	{
		AddMoney (newMoney - money);
	}

	/*
	 * 
	 * Health
	 * 
	 * */

	public void SetHealth( float newHaith ) {

		AddHealth (newHaith - Health);

	}

	// use for adding or removing health 
	public void AddHealth ( float addHealth ) {

		if (addHealth != 0) {
			// Debug.Log ("AddHealth("+addhealth+")");
			if (addHealth < 0) {
				//print ("addhealth: "+ addHealth + " maxprotection: " + maxProtection + " Faith: "+ Faith);
				addHealth =  addHealth- (maxProtection/100 * Faith*addHealth);
			}

			health = health + addHealth;
			if (health <= 0.0f) {

				health = 0.0f;
				// health ... 
				OnDie ();

			} else {
		
				if (health >= MaxHealth) {
					addHealth = addHealth - health + MaxHealth;
					health = MaxHealth;
					// health ... 
				}

				OnAddHealth (addHealth);

			}
		}
	}


	public float GetHealth(  ) {
		
		return Health;
		
	}

	public void SetFaith( float newFaith ) {
		
		AddFaith (newFaith - Faith);
		
	}
	
	// use for adding or removing health 
	public void AddFaith ( float addFaith ) {

		// Debug.Log ("AddHealth("+addhealth+")");		
		faith = faith + addFaith;

		if (faith <= 0.0f) {			
			faith = 0.0f;			 
		}

		if (faith >= MaxFaith) {			
			faith = MaxFaith;
		}

		OnAddFaith (addFaith);
	}



	public float GetFaith()
	{
		return faith;
	}

	public int GetMoney()
	{
		return Money;
	}


	/*
	 * inventory items
	 * 
	 * */
	public void AddItem( ) {

//		arrItems.Add ();

	}

  /*
	 * events
	 * 
	 * */


  void OnDrawGizmos() {

    if (this.gameObject.tag.Equals("1PlayerOnly"))
    {
      Gizmos.color = Color.green;
      Gizmos.DrawLine(this.transform.position, this.transform.position + Vector3.up * 4);
    }

    if (this.gameObject.tag.Equals("2PlayerOnly")) {
      Gizmos.color = Color.yellow;
      Gizmos.DrawLine(this.transform.position, this.transform.position + Vector3.up * 4);
    }

  }


  // on add money
  public virtual void OnAddMoney( int addsubMoney, int moneyTotal ) {



	}

	public virtual void OnAddHealth( float healthDifference ) {
		
		
		
	}

	public virtual void OnAddFaith( float faithDifference ) {
		
		
		
	}


	// health: on die
	public virtual void OnDie() {

		// Debug.Log ("Dying now.");


	}


}
