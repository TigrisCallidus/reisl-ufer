﻿using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using DarkTonic.MasterAudio;
using GameLab.HackAndSlashFramework;
using GameLab.NotficationCenter;
using Utils.Helpers;
using Random = UnityEngine.Random;

public enum ChickenState
{
  Idle,
  Walk,
  Run,
  GotHit,
  Dead
}

public class LogicChicken : LogicBase, IItem
{

  public Material[] skins;

  public float MaxIdle = 5;
  public float MinIdle = 1;
  private float currentIdle = 0;
  private float currentIdleSpeed = 0;

  public ChickenState CurrentState;
  private Animator animator;

  private Vector3 _CurrentRigidPos;
  private Vector3 _CurrentPosRounded;
  private Vector3 _NextPosRounded;
  private Vector3 nextMovePos;

  public float SightRadius = 5;
  public float MoveRadius = 3;

  public float MoveSpeed = 2;
  public float RunSpeed = 5;
  public float MoveMargin = 0.2f;  
  public int roundDigits = 1;

  public bool FleeFromPlayer = true;

  [Tooltip("The GameObject which is created after death")]
  public GameObject PrefabSpawnedGameObject;

  [Tooltip("How long it takes until the 'PrefabSpawnedGameObject' is created after death")]
  public float DeathDelay = 2;

  [Tooltip("If TRUE the drops are random depending on the chance")]
  public bool UseLuckDrop = false;
  [Range(0,1)]
  public float LuckChance = 0.5f;
  
  private bool ImBored = true;
  private bool ImDead = false;
  private bool spawnWalkSound = false;

  private NavMeshAgent agent;

	// Use this for initialization
	protected void Start ()
	{
	  agent = this.GetComponent<NavMeshAgent>();

    this.GetComponentInChildren<SkinnedMeshRenderer>().material = this.skins[Random.Range(0, this.skins.Count())];
	  animator = this.GetComponentInChildren<Animator>();
	  SetRandomIdle();
	}

  private void SetRandomIdle()
  {
    currentIdle = Random.Range(MinIdle, MaxIdle);
    currentIdleSpeed = Random.Range(0.1f, 1);
  }



  private void SetRandomMovePos()
  {
      SetNextMovePosition(this.transform.position +
                          new Vector3(Random.Range(-this.MoveRadius, this.MoveRadius), 0, Random.Range(-this.MoveRadius, this.MoveRadius)));
  }

  private void SetNextMovePosition(Vector3 pos)
  {
    this.nextMovePos = pos;
    //this.agent.SetDestination(this.nextMovePos);

    _NextPosRounded = new Vector3(GameLogic.GetRoundFloat(nextMovePos.x, roundDigits), 0, GameLogic.GetRoundFloat(nextMovePos.z, roundDigits));
  }

  void Update()
  {

  }

  void FixedUpdate ()
  {
    StateMachine();

    if (!ImDead) {
      if (ImBored) {
        StartCoroutine(IAmBored());
      }

      SwitchState();
    }
  }

  private void StateMachine() {

    switch (CurrentState) {
      case ChickenState.Idle:
        //animator.SetBool(ChickenState.Idle.ToString(), true);
        break;

      case ChickenState.Walk:
        Move(this.MoveSpeed);
        break;

      case ChickenState.Run:
        Move(this.RunSpeed);
        break;

      case ChickenState.GotHit:
        animator.SetBool(ChickenState.GotHit.ToString(), true);
        break;

      case ChickenState.Dead:
        animator.SetBool(ChickenState.Dead.ToString(), true);
        break;
    }

  }

  private bool IsClose(Vector3 pos)
  {
    if (Mathf.Abs(pos.x - this._CurrentPosRounded.x) <= this.MoveMargin)
    {
      if (Mathf.Abs(pos.z - this._CurrentPosRounded.z) <= this.MoveMargin)
      {
        return true;
      }
    }

    return false;
  }

  private void Move(float Speed)
  {
    _CurrentRigidPos = this.GetComponent<Rigidbody>().position;
    _CurrentPosRounded = new Vector3(GameLogic.GetRoundFloat(_CurrentRigidPos.x, roundDigits), 0, GameLogic.GetRoundFloat(_CurrentRigidPos.z, roundDigits));


    if (IsClose(_NextPosRounded))
    {
    // Debug.DrawLine(this._CurrentPosRounded, _NextPosRounded, Color.green);
      this.CurrentState = ChickenState.Idle;
      this.ImBored = true;
      this.GetComponent<Rigidbody>().velocity = Vector3.zero;
      //SetNextMovePosition(_NextPosRounded);
    } else {
     // Debug.DrawLine(this._CurrentPosRounded, _NextPosRounded, Color.red);
      this.transform.LookAt(this.nextMovePos);
      this.agent.SetDestination(this.nextMovePos);
      this.GetComponent<Rigidbody>().MovePosition(this.agent.nextPosition);
    }
  }


  private void SwitchState()
  {
    if (!animator.GetBool(this.CurrentState.ToString()))
    {
      animator.SetBool(this.CurrentState.ToString(), true);

      if (this.CurrentState == ChickenState.Idle)
      {
        StartCoroutine(PlaySound(currentIdleSpeed * 0.5f, currentIdleSpeed, "idle"));
        animator.speed = currentIdleSpeed;
      } else {
        animator.speed = 1;
      }

      if (this.CurrentState == ChickenState.Walk)
      {
        this.agent.speed = this.MoveSpeed;
        StartCoroutine(PlaySound(1,3, "walk"));
        //this.agent.updatePosition = true;
      }
      else
      {
        //this.agent.updatePosition = false;
      }

      if (this.CurrentState == ChickenState.Run)
      {
        this.agent.speed = this.RunSpeed;
        StartCoroutine(PlaySound(0.25f, 0.75f, "run"));
        //this.agent.updatePosition = true;
      }
      else
      {
        //this.agent.updatePosition = false;
      }

      for (int i = 0; i < 4; i++)
      {
        if (this.CurrentState != (ChickenState) i)
        {
          animator.SetBool(((ChickenState) i).ToString(), false);
        }
      }
    }
  }


  private void OnTriggerEnter(Collider coll)
  {
    //Debug.Log("OnTriggerEnter " + coll.gameObject);
    if (FleeFromPlayer && !this.ImDead
      && coll.gameObject.GetComponent<LogicHuman>() != null)
    {
      ImBored = false;

      Vector3 directionToPlayer = coll.gameObject.transform.position - this.transform.position;
      directionToPlayer.Normalize();
      SetNextMovePosition(-directionToPlayer * Random.Range(0.5f,this.SightRadius));
      _NextPosRounded = new Vector3(GameLogic.GetRoundFloat(nextMovePos.x, 2), 0, GameLogic.GetRoundFloat(nextMovePos.z, 2));

      CurrentState = ChickenState.Run;

      SwitchState();
    }

  }


  public void OnCharacterHit(CharacterAttack attack) {
  //public override void OnAddHealth(float healthDifference)
  //{
    //Debug.Log(this + " got damage " + attack.GetDamage());
    AddHealth(-attack.GetDamage());
    if (this.Health <= 0) {
      CurrentState = ChickenState.Dead;
      Dead();
    } else {
      CurrentState = ChickenState.GotHit;
      StartCoroutine(ResetAnimationBool("GotHit", 0.25f));
    }
  }

  private void Dead()
  {
    ImDead = true;
    ImBored = false;

    this.agent.SetDestination(this.transform.position);
    this.GetComponent<CapsuleCollider>().enabled = false;
    this.GetComponent<Rigidbody>().useGravity = false;
    this.GetComponent<Rigidbody>().isKinematic = true;
    //agent.updatePosition = false;

    SwitchState();

    StartCoroutine(SpawnObject());
  }

  private IEnumerator ResetAnimationBool(string boolName, float time)
  {
    yield return new WaitForSeconds(time);
    this.animator.SetBool(boolName, false);
    this.CurrentState = ChickenState.Idle;
  }

  private IEnumerator PlaySound(float minTime, float maxTime, string sound)
  {
    if (!this.spawnWalkSound){
      this.spawnWalkSound = true;
      yield return new WaitForSeconds(Random.Range(minTime, maxTime));
      /*
      AddNotification(NotificationCenter.TYPE_SOUND + ".chicken." + sound, this.transform.position,
        NotificationPriority.ThrowAwayAfterProcessing);
        */
      MasterAudio.PlaySound3DAtTransformAndForget("chicken." + sound, this.transform);
      this.spawnWalkSound = false;
    }
  }

  private IEnumerator IAmBored() {
    this.ImBored = false;

    yield return new WaitForSeconds(this.currentIdle);

    if (this.CurrentState == ChickenState.Idle)
    {
      SetRandomIdle();
      RollForRandomState();
    }

      this.ImBored = true;
  }

  // only the normal State (Idle & Walk)
  private void RollForRandomState() {

    CurrentState = (ChickenState)Random.Range(0, 2);

    if (CurrentState == ChickenState.Walk) {
      SetRandomMovePos();
    }
  }

  private IEnumerator SpawnObject()
  {
      yield return new WaitForSeconds(DeathDelay);

      if (this.UseLuckDrop)
      {
        if (Random.Range(0.0f, 1f) <= this.LuckChance)
        {
          DropObject();
        }
      }
      else
      {
        DropObject();
      }
    
  }

  private void DropObject()
  {
    if (PrefabSpawnedGameObject != null)
    {
      GameObject go = Instantiate(PrefabSpawnedGameObject , this.transform.position , Quaternion.identity) as GameObject;

      if (go.GetComponent<TriggerEnterNotification>().gameLogic == null)
      {
        //workaround because sometimes in the TriggerBase the Awake() doesn't work...
        go.GetComponent<TriggerEnterNotification>().gameLogic = this.gameLogic;
      }

    }
    this.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
    Destroy(this.gameObject, 0.5f);
  }

}
