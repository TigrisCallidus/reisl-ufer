﻿using UnityEngine;
using System.Collections;

public class NamingScript : MonoBehaviour {

    public GUIText myName;
    private const int numberofnames = 34;
    string[] names = new string[numberofnames]{"Uolf",
            "Theo",
"Vitus",
"Werni",
"Beatus",
"Urban",
"Berhardin",
"Gaetan",
"Florianus",
"Niklaus",
"Walo",
"Leonz",
"Rudolf",
"Christen",
"Hans",
"Cunrad",
"Lorentz",
"Paulus",
"Michel",
"Eberhart",
"Bernhard",
"Jobst",
"Diether",
"Ludwig",
"Veit",
"Kilian",
"Jeronimus",
"Frantz",
"Oswald",
"Sigi",
"Benedict",
"Vallentin",
"Wenzel",
"Wichart"
};
// Use this for initialization
void Start () {
        myName = this.gameObject.GetComponent<GUIText>();
        myName.text = getRandomName();

    }

    string getRandomName()
    {
        string text = "";
        float tmp=Random.Range(0, numberofnames - 1);
        tmp = Mathf.Floor(tmp);
        text = names[(int)tmp];
        return text;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
