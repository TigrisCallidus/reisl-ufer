using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework;

// take action from argument ( leveleditor create with gameelement gameobject )

public class ActionNotification : TriggerBase {

	void DoArgumentNotification() {
		
		Debug.Log ("[ActionNotification] Do Notification: "+NotificationName);
		gameLogic.AddNotification (""+NotificationName, this.gameObject.transform.position );

	}

}
