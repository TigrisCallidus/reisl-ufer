﻿using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework;
using GameLab.NotficationCenter;

public class StartMessage : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject gl = GameObject.Find ("_GameLogic");
		if(gl){
			GameLogic gameLogic = gl.GetComponent<GameLogic>();
			gameLogic.AddNotification ("[message]", transform.position , NotificationPriority.Top);
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
