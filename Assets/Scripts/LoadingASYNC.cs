﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingASYNC : MonoBehaviour {

	private Text loadingString;
	public void LoadScene(ISceneState sceneState) {
		StartCoroutine ("LoadLevelAsync", sceneState);
		loadingString = GetComponent<Text> ();
		loadingString.text = sceneState.LoadingString;
		//Debug.Log ("We're in state " + sceneState.ToString ());

	}

	IEnumerator LoadLevelAsync(ISceneState sState) {

		AsyncOperation async = Application.LoadLevelAsync(sState.UnityScene);

		yield return async;

	}
	

}
