﻿using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework;
using GameLab.NotficationCenter;


public class Barrel : MonoBehaviour {

	Rigidbody rB;
	public float speed = 1;
	public Transform modelT;
	public float damage = 0.5f;

	bool isRolling = false;

	// Use this for initialization
	void Awake () {
		rB = GetComponent<Rigidbody>();
	}

	public void StartRolling(){
		isRolling = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(isRolling){
			rB.MovePosition(rB.position+transform.forward*speed);
			modelT.Rotate(0,0,-speed*50);
		}
	}

	void OnCollisionEnter(Collision inC){
		LogicPlayer logicPlayer = inC.gameObject.GetComponent<LogicPlayer>();
		if (logicPlayer != null) {
			logicPlayer.AddHealth(-damage);
			inC.gameObject.GetComponent<Rigidbody>().AddForce(transform.forward*10000);

			GameObject gl = GameObject.Find ("_GameLogic");
			if(gl){
				GameLogic gameLogic = gl.GetComponent<GameLogic>();
				gameLogic.AddNotification ("[npcenemy.death]", "", inC.contacts[0].point, NotificationPriority.ThrowAwayAfterProcessing);
			}
			//inC.gameObject.GetComponent<CharacterEngine>().OnCharacterDamage(CharacterAttack.
		}
	}
}
