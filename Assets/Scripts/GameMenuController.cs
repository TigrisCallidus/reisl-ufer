﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameMenuController : MonoBehaviour {

	// Use this for initialization
	void Awake ()
	{
    Toggle toggle = GameMenuCanvas.instance.transform.GetComponentInChildren<Toggle>();   
		if (toggle != null) {
			toggle.isOn = SceneStateManager.instance.Is2Player;
		}
  }

  // Update is called once per frame
  void Update () {
	
	}

	public void ShowPanel(GameObject PanelObjectName) {
		//GameObject panel = GameObject.Find ("PanelObjectName");
		GameMenuCanvas.instance.ShowPanel (PanelObjectName);
		PanelObjectName.transform.GetChild (0).GetChild (0).gameObject.GetComponent<Button> ().Select ();
  }



  public void Toggle2Player(){
    SceneStateManager.instance.Is2Player = !SceneStateManager.instance.Is2Player;
  }

	public void Resume() {
		SceneStateManager.instance.changeToLastState(false);
	}

	public void Settings(string firstSelectedElement) {
		//GameMenuCanvas.instance.ShowPanel (GameMenuCanvas.instance.SettingsPanel);
		//GameObject.Find (firstSelectedElement).GetComponent<Button> ().Select ();
	}

	public void Credits(string firstSelectedElement) {
		GameMenuCanvas.instance.ShowPanel (GameMenuCanvas.instance.CreditsPanel);
		GameObject.Find (firstSelectedElement).GetComponent<Button> ().Select ();
	}

	public void LevelSelection(string firstSelectedElement) {
		GameMenuCanvas.instance.ShowPanel (GameMenuCanvas.instance.LevelSelectionPanel);
		GameObject.Find (firstSelectedElement).GetComponent<Button> ().Select ();
	}

	public void SequenceSelection(string firstSelectedElement) {
		GameMenuCanvas.instance.ShowPanel (GameMenuCanvas.instance.SequencePanel);
		GameObject.Find (firstSelectedElement).GetComponent<Button> ().Select ();
	}

	public void Controls(string firstSelectedElement) {
		GameMenuCanvas.instance.ShowPanel (GameMenuCanvas.instance.ControlsPanel);
		GameObject.Find (firstSelectedElement).GetComponent<Button> ().Select ();
	}


	public void TutorialAlpine() {
		SceneStateManager.instance.changeState ( new LevelAlpine());
	}

	public void TutorialCombat() {
		SceneStateManager.instance.changeState ( new TutorialState());
	}

	public void LevelZurich() {
		SceneStateManager.instance.changeState (new Level00Zurich());
	}

	public void MainMenu() {
		SceneStateManager.instance.changeState (new MenuScreenState());
	}



	public void Level1() {
		SceneStateManager.instance.changeState ( new Level1State());
    //SceneStateManager.instance.changeState(new IntroductionState());
  }

  public void Sequence1() {
		SceneStateManager.instance.Is2Player = false;
		SceneStateManager.instance.changeState ( new Sequence1State());
	}

	public void Sequence1for2() {
		SceneStateManager.instance.Is2Player = true;
		SceneStateManager.instance.changeState ( new Sequence1State());
	}

	public void Level2() {
		SceneStateManager.instance.changeState ( new Level2State());
	}
	
	public void Sequence2() {
		SceneStateManager.instance.changeState ( new Sequence2State());
	}

	public void Level3() {
		SceneStateManager.instance.changeState ( new Level3State());
	}
	
	public void Sequence3() {
		SceneStateManager.instance.changeState ( new Sequence3State());
	}

	public void Level4() {
		SceneStateManager.instance.changeState ( new Level4State());
	}
	
	public void Sequence4() {
		SceneStateManager.instance.changeState ( new Sequence4State());
	}

	public void QuitGame() {
		SceneStateManager.instance.changeState ( new QuitGameState());
		
	}
	

	public void GoBackToMenu() {
		if (SceneStateManager.instance.state == null || SceneStateManager.instance.state.ToString() == typeof(MenuScreenState).Name) {
			GameMenuCanvas.instance.ShowPanel (GameMenuCanvas.instance.MenuPanel);
			GameMenuCanvas.instance.MenuPanel.transform.GetChild (0).GetChild (0).gameObject.GetComponent<Button> ().Select ();
		}

		if (SceneStateManager.instance.state != null && SceneStateManager.instance.state.ToString() == typeof(StartMenuScreenState).Name) {
			GameMenuCanvas.instance.ShowPanel (GameMenuCanvas.instance.StartMenuPanel);
			GameMenuCanvas.instance.StartMenuPanel.transform.GetChild (0).GetChild (0).gameObject.GetComponent<Button> ().Select ();
		}
		
	}
}
