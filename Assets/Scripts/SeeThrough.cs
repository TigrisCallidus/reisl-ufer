﻿using UnityEngine;
using System.Collections;

public class SeeThrough : MonoBehaviour {

	public float SeeThroughTime;

	private Renderer singleRenderer;
	private Renderer[] Renderers;
	// Use this for initialization
	void Start () {
		if (GetComponent<Renderer> () != null) {
			singleRenderer = GetComponent<Renderer>();
		}

		if (GetComponentsInChildren<Renderer> () != null) {
			Renderers = GetComponentsInChildren<Renderer>();
		}

		if (singleRenderer != null) {
			singleRenderer.enabled = false;
		}

		if (Renderers != null && Renderers.Length > 0) {
			DisableRenderers(Renderers);
		}

		Destroy (this, SeeThroughTime);
	}


	void DisableRenderers(Renderer[] rend) {
		foreach (Renderer R in rend) {
			R.enabled = false;
		}
	}

	void EnableRenderers(Renderer[] rend) {
		foreach (Renderer R in rend) {
			R.enabled = true;
		}
	}
	
	// Update is called once per frame
	void OnDestroy () {
		if (singleRenderer != null) {
			singleRenderer.enabled = true;
		}

		if (Renderers != null && Renderers.Length > 0) {
			EnableRenderers(Renderers);
		}
	}
}
