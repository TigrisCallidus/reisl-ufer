﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	/// <summary>
	/// Wether the spawner is active or not
	/// </summary>
	public bool IsSpawning;

	public bool SpawnFirstImmediately;

	public int currentEnemyCountOnBattlefield;

	/// <summary>
	/// Raises the enemy died event.
	/// </summary>
	public virtual void OnEnemyDied() {
		currentEnemyCountOnBattlefield--;
	}
}
