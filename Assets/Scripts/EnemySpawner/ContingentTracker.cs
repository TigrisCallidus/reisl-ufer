﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Tracks the contingent of Enemies on the battlefield, spawned from a spawner.
/// </summary>
public class ContingentTracker : MonoBehaviour {

	public EnemySpawner MotherSpawner;


	void OnDestroy() {
		//Notify MotherSpawner
		MotherSpawner.OnEnemyDied ();
	}

}
