﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Utils.Helpers;
using GameLab.HackAndSlashFramework;

/// <summary>
/// Iterates through Waves-List and spawns the defined enemies.
/// </summary>
public class WaveSpawner : EnemySpawner {
	

	/// <summary>
	/// The spawn radius.
	/// </summary>
	public float SpawnRadius = 2F;


	/// <summary>
	/// The wave intervall.
	/// </summary>
	public int WaveIntervall = 5;

	/// <summary>
	/// The first waypoint.
	/// </summary>
	private Transform firstWaypoint;

	/// <summary>
	/// The waves defined as List of Lists
	/// </summary>
	public List<WaveList> Waves;

	/// <summary>
	/// The current wave Index.
	/// </summary>
	private int currentWaveIndex = -1;

	/// <summary>
	/// The time since the last wave.
	/// </summary>
	private float timeSinceLastWave = 0;

	/// <summary>
	/// Only spawns new wave when all enemies from current wave dead.
	/// </summary>
	public bool OnlySpawnWhenAllDead;


	private bool canSpawnWave = true;

	private int enemiesDeadPerWave = 0;

	/// <summary>
	/// Occurs when on hold input activated.
	/// </summary>
	public delegate void ActuateOnSpawnerFinished(object sender);
	
	/// <summary>
	/// Occurs when on hold input activated.
	/// </summary>
	public event ActuateOnSpawnerFinished OnSpawnerFinished;

	void Awake() {
		//Get first Waypoint from child Object
		firstWaypoint = transform.GetChild (0).transform;
		if (SpawnFirstImmediately) {
			timeSinceLastWave = WaveIntervall-0.01F;
		}
	}

	// Use this for initialization
	void Start () {

	}




	void FixedUpdate() {
		if ((timeSinceLastWave >= WaveIntervall && !OnlySpawnWhenAllDead) || (canSpawnWave && timeSinceLastWave >= WaveIntervall)) {
			bool hasWave = GoToNextWave ();
			if (hasWave) {
				SpawnWave (currentWaveIndex);
				canSpawnWave = false;
			}
			timeSinceLastWave = 0;
		} 

		if (IsSpawning) {
			timeSinceLastWave += Time.deltaTime;
		}
	}


	/// <summary>
	/// Gos to next wave.
	/// </summary>
	/// <returns><c>true</c>, if to next wave was gone, <c>false</c> otherwise.</returns>
	bool GoToNextWave() {
		bool res = true;
		currentWaveIndex++;
		if (currentWaveIndex >= Waves.Count) {
			//End
			res = false;
			Debug.Log ("No more waves to spawn");
		}

		return res;
	}


	bool HasNextWave() {
		bool res = true;

		if (currentWaveIndex+1 >= Waves.Count) {
			//End
			res = false;
			Debug.Log ("No more waves to spawn");
		}
		
		return res;
	}


	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Raises the enemy died event.
	/// </summary>
	public override void OnEnemyDied() {
		enemiesDeadPerWave++;
		canSpawnWave = IsWaveDead ();
		if (!HasNextWave() && canSpawnWave) {
			OnSpawnerFinished(this);
		}
	}


	private bool IsWaveDead() {
		bool res = false;
		int enemieCountOfCurrentWave = Waves [currentWaveIndex].Enemies.Count;
		if(enemiesDeadPerWave >= enemieCountOfCurrentWave) {
			res = true;
			timeSinceLastWave = 0;
			enemiesDeadPerWave = 0;
		}

		return res;

	}

	/// <summary>
	/// Spawns the wave.
	/// </summary>
	/// <param name="waveIndex">Wave index.</param>
	void SpawnWave(int waveIndex) {
		List<GameObject> enemies = Waves [waveIndex].Enemies;
		Transform[] iniWaypoints = new Transform[1];
		iniWaypoints [0] = firstWaypoint;

		foreach (GameObject enemy in enemies) {
			//Spawn Enemy
			GameObject _e = Instantiate(enemy,getRandomSpawnPosition(),Quaternion.identity) as GameObject;

			if(_e.GetComponent<CharacterEngineAI>() != null) {
				_e.GetComponent<CharacterEngineAI>().patrolWayPoints = iniWaypoints;
				_e.GetComponent<CharacterEngineAI>().patrol = true;
				} else {
				Debug.LogError(_e.name + " has no CharacterEngineAI. First Waypoint could not be set.");
			}

			_e.AddComponent<ContingentTracker>();
			_e.GetComponent<ContingentTracker>().MotherSpawner = this;



		}
	}


	/// <summary>
	/// Gets the random spawn position.
	/// </summary>
	/// <returns>The random spawn position.</returns>
	Vector3 getRandomSpawnPosition() {
		Vector3 res = transform.position;
		float rand = Random.Range (0, 360);
		res = res + VectorHelper.SphericalToCartesian (SpawnRadius, rand, 0);

		return res;
	}

}

[System.Serializable]
	/// <summary>
	/// Wave list.
	/// </summary>
	public class WaveList
	{
		public List<GameObject> Enemies;
	}

	
