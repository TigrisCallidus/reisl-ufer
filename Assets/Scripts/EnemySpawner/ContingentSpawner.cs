﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Utils.Helpers;
using GameLab.HackAndSlashFramework;

/// <summary>
/// Spawns a defined amount of enemies and keeps track of the amount on battlefield.
/// Only spawns allowed number of enemies to match the maximum contingend of spawned Enemies.
/// </summary>
public class ContingentSpawner : EnemySpawner {
	

	/// <summary>
	/// The spawn radius.
	/// </summary>
	public float SpawnRadius;

	public Transform spawnPoint;
	public bool useSpawnPoint = false;

	/// <summary>
	/// The max enemies on battlefield.
	/// </summary>
	public int MaxEnemiesOnBattlefield = 1;

	/// <summary>
	/// The list enemies to be spawned
	/// </summary>
	public List<GameObject> Enemies;

	/// <summary>
	/// The first waypoint the enemies will head to.
	/// </summary>
	private Transform firstWaypoint;

	/// <summary>
	/// The wave intervall.
	/// </summary>
	public int SpawnIntervall = 5;

	/// <summary>
	/// The time since last spawn.
	/// </summary>
	private float timeSinceLastSpawn;

	/// <summary>
	/// Raises the enemy died event.
	/// </summary>
	public override void OnEnemyDied() {
		this.currentEnemyCountOnBattlefield--;
	}

	
	void Awake() {
		firstWaypoint = transform.GetChild (0).transform;
		if (SpawnFirstImmediately) {
			timeSinceLastSpawn = SpawnIntervall-0.01F;
		}
	}


	void FixedUpdate() {
		if (timeSinceLastSpawn >= SpawnIntervall) {
			SpawnEnemies();
			timeSinceLastSpawn = 0;
		}
		
		if (IsSpawning && MaxEnemiesOnBattlefield - currentEnemyCountOnBattlefield > 0) {
			timeSinceLastSpawn += Time.deltaTime;
		}
	}


	/// <summary>
	/// Spawns the enemies.
	/// </summary>
	void SpawnEnemies() {
		Transform[] iniWaypoints = new Transform[1];
		iniWaypoints [0] = firstWaypoint;

		int spawnCount = MaxEnemiesOnBattlefield - currentEnemyCountOnBattlefield;
		for (int i = 0; i < spawnCount; i++) {
			GameObject _e = Instantiate(getRandomEnemy(),getRandomSpawnPosition(),Quaternion.identity) as GameObject;
			currentEnemyCountOnBattlefield++;

			if(_e.GetComponent<CharacterEngineAI>() != null) {
				_e.GetComponent<CharacterEngineAI>().patrolWayPoints = iniWaypoints;
				_e.GetComponent<CharacterEngineAI>().patrol = true;
			}


			_e.AddComponent<ContingentTracker>();
			_e.GetComponent<ContingentTracker>().MotherSpawner = this;

		}
	}

	/// <summary>
	/// Gets a random enemy.
	/// </summary>
	/// <returns>A random enemy.</returns>
	GameObject getRandomEnemy() {
		if (Enemies.Count > 0) {
			int max = Enemies.Count;
			int rand = Random.Range (0, max);

			return Enemies [rand];
		} else {
			Debug.LogError("No Enemies defined. Could not spawn them.");
			return null;
		}

	}

	/// <summary>
	/// Gets a random spawn position.
	/// </summary>
	/// <returns>A random spawn position.</returns>
	Vector3 getRandomSpawnPosition() {
		if (useSpawnPoint && spawnPoint != null)
		{
			return spawnPoint.position;
		}
		Vector3 res = transform.position;
		float rand = Random.Range (0, 360);
		res = res + VectorHelper.SphericalToCartesian (SpawnRadius, rand, 0);
		
		return res;
	}

}
