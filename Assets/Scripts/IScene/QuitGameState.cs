using UnityEngine;
using System.Collections;

public class QuitGameState : SceneState  {

  new public string UnityScene { get { return ""; }}
  new public string LoadingString { get { return "I'm leaving..."; }}
  new public int SequenceDuration { get { return 0; }}
  new public SceneState AutomaticNextScene { get { return null; }}

	#region ISceneState implementation

	public override void enterState (SceneStateManager sceneManager)
	{
		Application.Quit ();
	}

	public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
		
	}

	public override void update (SceneStateManager sceneManager)
	{

	}

	public override void lateUpdate (SceneStateManager sceneManager)
	{
	}

	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}

	public override void onGUI (SceneStateManager sceneManager)
	{
	}

	public override void exitState (SceneStateManager sceneManager)
	{
	}

  public override void onLevelWasLoaded(int lvl, SceneStateManager sceneManager) {
  }


  #endregion


}
