﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;
using GameLab.HackAndSlashFramework;
using GameLab.NotficationCenter;
using InControl;

public class ShopState : SceneState
{
  new public string UnityScene { get { return "shop"; }}
  new public string LoadingString { get { return "Loading your awesome Shop"; }}
  new public int SequenceDuration { get { return 65; }}
  new public SceneState AutomaticNextScene { get { return null; }}

	public InputHandler controls;

  private InputDevice _device;

  #region ISceneState implementation

  public override void enterState (SceneStateManager sceneManager)
	{
		Pausable.PauseAll ();
	    PlayerHUD.Instance.ShowShop(true);
	}

	public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
		
	}

	public override void update (SceneStateManager sceneManager)
	{
	  if (GameLogic.Instance.playerInShop != null)
	  {
	    if (GameLogic.Instance.playerInShop.GetCharacter().type == CharacterType.Player1)
	    {
	      _device = InputManager.Devices[0];
	    }

	      if (GameLogic.Instance.playerInShop.GetCharacter().type == CharacterType.Player2) {
	        _device = InputManager.Devices[1];
	      }

	    if (_device != null)
	    {
		if (_device.GetControl(InputControlType.Action1).WasPressed || Input.GetKeyDown(KeyCode.A))
	      {
	        BuyHealth(GameLogic.Instance.playerInShop);
	      }

		if (_device.GetControl(InputControlType.Action2).WasPressed || Input.GetKeyDown(KeyCode.B)) {
          BuyFaith(GameLogic.Instance.playerInShop);
        }

		if (_device.GetControl(InputControlType.Action4).WasPressed || Input.GetKeyDown(KeyCode.Y)) {
          BuySpirit(GameLogic.Instance.playerInShop);
        }

		if (_device.GetControl(InputControlType.Action3).WasPressed || Input.GetKeyDown(KeyCode.X)) {
			Debug.Log ("Exit shop");
			SceneStateManager.instance.changeToLastState(false,true);
        }
	  }
    }

	}

	public override void lateUpdate (SceneStateManager sceneManager)
	{
	}

	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}

	public override void onGUI (SceneStateManager sceneManager)
	{
	}

  private void BuyHealth(LogicPlayer player){

    if (player.Money >= GameLogic.Instance.HealthPrice){
      player.AddMoney(-GameLogic.Instance.HealthPrice);
      player.AddHealth(GameLogic.Instance.HealthMultiplierOfMax * player.MaxHealth);
      GameLogic.Instance.AddNotification(NotificationCenter.TYPE_SOUND + ".shop.buy", player.transform.position);
    }

  }

  private void BuyFaith(LogicPlayer player) {

    if (player.Money >= GameLogic.Instance.FaithPrice) {
      player.AddMoney(-GameLogic.Instance.FaithPrice);
      player.AddFaith(GameLogic.Instance.FaithMultiplierOfMax * player.MaxFaith);
      GameLogic.Instance.AddNotification(NotificationCenter.TYPE_SOUND + ".shop.buy", player.transform.position);
    }

  }

  private void BuySpirit(LogicPlayer player) {

    if (player.Money >= GameLogic.Instance.FaithPrice) {
      player.AddMoney(-GameLogic.Instance.FaithPrice);
      player.AddFaith(GameLogic.Instance.FaithMultiplierOfMax * player.MaxFaith);
      GameLogic.Instance.AddNotification(NotificationCenter.TYPE_SOUND + ".shop.buy", player.transform.position);
    }

  }

  public override void exitState (SceneStateManager sceneManager)
  {
   		PlayerHUD.Instance.ShowShop(false);
		Pausable.ResumeAll ();
  }

  public override void onLevelWasLoaded(int lvl, SceneStateManager sceneManager)
  {

  }

	#endregion


}
