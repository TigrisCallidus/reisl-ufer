﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class StartMenuScreenState : SceneState, ISceneState {

  new public string UnityScene { get { return ""; }}
  new public string LoadingString { get { return "Loading Start Menu..."; }}
  new public int SequenceDuration { get { return 65; }}
  new public SceneState AutomaticNextScene { get { return null; }}

	private Button first;
  #region ISceneState implementation


  /*
	private int ButtonWidth = 180;
	private int ButtonHeight = 30;

	private int topPadding = 30;
  */

  private List<ISceneState> stateList;
		

	public override void enterState (SceneStateManager sceneManager)
	{
		GameMenuCanvas.instance.ShowPanel (GameMenuCanvas.instance.StartMenuPanel);
		GameMenuCanvas.instance.StartMenuPanel.transform.GetChild (0).GetChild (0).GetComponent<Button> ().Select ();
		//Pause all Pausables

		Pausable.PauseAll ();
		Cursor.visible = true; 


		//first = GameObject.Find ("Settings").GetComponent<Button> ();
		//first.Select ();
		//SceneStateManager.LastCheckpoint = Vector3.zero;
	}

	public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
		
	}

	public override void update (SceneStateManager sceneManager)
	{
		if(InControl.InputManager.ActiveDevice.GetControl(InControl.InputControlType.Start).WasPressed || Input.GetKeyDown(KeyCode.Escape)) {
			SceneStateManager.instance.changeToLastState(false);
		}
	}

	public override void lateUpdate (SceneStateManager sceneManager)
	{
	}

	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}

	public override void onGUI (SceneStateManager sceneManager)
	{
		/*
		int i = 0;
		foreach (ISceneState state in stateList) {
			if(GUI.Button(new Rect(Screen.width/2 - ButtonWidth/2,topPadding+(i*(ButtonHeight+10)),ButtonWidth,ButtonHeight),state.ToString())) {
				SceneStateManager.instance.changeState(state,false);
			}
			   i++;
		}
		if(GUI.Button(new Rect(Screen.width/2 - ButtonWidth/2,topPadding+(i*(ButtonHeight+10)),ButtonWidth,ButtonHeight),"Go to last state")) {
			SceneStateManager.instance.changeState(SceneStateManager.instance.lastState,false);
		}
		*/
	}

	public override void exitState (SceneStateManager sceneManager)
	{
		GameMenuCanvas.instance.HideAllPanels ();
		Pausable.ResumeAll ();
		//Resume all Pausable
		Cursor.visible = false;

  }

  void ISceneState.onLevelWasLoaded(int lvl, SceneStateManager sceneManager) {
  }


	#endregion


}
