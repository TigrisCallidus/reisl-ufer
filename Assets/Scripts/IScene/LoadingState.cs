﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadingState : SceneState, ISceneState {

  new public string UnityScene { get { return "LoadingScreen"; }}
  new public string LoadingString { get { return "Loading the loading state...no wait..."; }}
  new public int SequenceDuration { get { return 0; }}
  new public SceneState AutomaticNextScene { get { return null; }}

	#region ISceneState implementation

	private bool sequenceFound, sequencePlaying;

	private RawImage Sequence;
	private MovieTexture movie;

	private ISceneState sceneStateObject;
	private bool LoadingInitalized;

	public override void enterState (SceneStateManager sceneManager)
	{
		Cursor.visible = false; 
	}

	public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
		sceneStateObject = state;
		//Pausable.ClearAll ();
		Application.LoadLevel ("LoadingScreen");
	}


	public override void update (SceneStateManager sceneManager)
	{
		if (!LoadingInitalized) {
			if(GameObject.FindObjectOfType<LoadingASYNC>() != null) {
				LoadingASYNC loader = GameObject.FindObjectOfType<LoadingASYNC>().GetComponent<LoadingASYNC>();
				loader.LoadScene(sceneStateObject);
				LoadingInitalized = true;
				SceneStateManager.instance.changeState (sceneStateObject,false,false);
			}
		}
	}

	public override void lateUpdate (SceneStateManager sceneManager)
	{

	}

	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}

	public override void onGUI (SceneStateManager sceneManager)
	{
	}

	public override void exitState (SceneStateManager sceneManager)
	{
	}


	public override void onLevelWasLoaded(int lvl, SceneStateManager sceneManager) 
	{

	}


  #endregion


  }
