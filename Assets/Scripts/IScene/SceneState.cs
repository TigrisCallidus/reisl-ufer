﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameLab.HackAndSlashFramework;

public class SceneState : ISceneState {

	public string UnityScene { get { return ""; }}
	public string LoadingString { get { return "Loading..."; }}
	public int SequenceDuration { get { return 0; }}
	public SceneState AutomaticNextScene { get { return null; }}
    public bool isCamp { get { return false; } }

    #region ISceneState implementation

    public virtual void enterState (SceneStateManager sceneManager)
	{
	}

	public virtual void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
		
	}

	public virtual void update (SceneStateManager sceneManager)
	{
	}

	public virtual void lateUpdate (SceneStateManager sceneManager)
	{
	}

	public virtual void fixedUpdate (SceneStateManager sceneManager)
	{
	}

	public virtual void onGUI (SceneStateManager sceneManager)
	{
	}

	public virtual void exitState (SceneStateManager sceneManager)
	{
	}

  	public virtual void onLevelWasLoaded(int lvl, SceneStateManager sceneManager)
 	{
 	}

	public CharacterEngine[] GetFriendsFromCharacters(CharacterEngine[] chars) {
		List<CharacterEngine> res = new List<CharacterEngine> ();
		for (int i = 0; i < chars.Length; i++) {
			if(chars[i].type == CharacterType.NPC || chars[i].type == CharacterType.Player1 || chars[i].type == CharacterType.Player2) {
				res.Add(chars[i]);
			}
		}
		
		return res.ToArray();
	}

	public void deactivatePlayerPointLights() {
		CharacterEngine[] chars = GetFriendsFromCharacters (GameObject.FindObjectsOfType<CharacterEngine> ());
		Light[] lights;
		foreach(CharacterEngine c in chars) {
			lights = c.GetComponentsInChildren<Light>();
			foreach(Light l in lights) {
				l.enabled = false;
			}
			
		}
	}


  #endregion


}
