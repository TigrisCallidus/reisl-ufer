﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;
using GameLab.HackAndSlashFramework;

public class Sequence2State : SceneState, ISceneState
{
  new public string UnityScene { get { return "Sequence2"; }}
  new public string LoadingString { get { return "Loading Sequence 2..."; }}
  new public int SequenceDuration { get { return 65; }}
  new public SceneState AutomaticNextScene { get { return new Level00Zurich(); }}

	public InputHandler controls;
	
	
	#region ISceneState implementation
	
	public override void enterState (SceneStateManager sceneManager)
	{
		if (GameMenuCanvas.instance != null) {
			GameMenuCanvas.instance.HideAllPanels ();
		}
		Cursor.visible = false;
	}
	
	public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
		
	}
	
	public override void update (SceneStateManager sceneManager)
	{

	}
	
	public override void lateUpdate (SceneStateManager sceneManager)
	{
	}
	
	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}
	
	public override void onGUI (SceneStateManager sceneManager)
	{
	}
	
	public override void exitState (SceneStateManager sceneManager)
	{
	}
	
	public override void onLevelWasLoaded(int lvl, SceneStateManager sceneManager)
	{
		
		
		
	}
	
	#endregion
	
	
}

