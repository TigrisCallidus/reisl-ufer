using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;
using GameLab.HackAndSlashFramework;

public class SceneStateManager : MonoBehaviour {

	public bool showGUI = false;
  public bool Is2Player = false;

    public bool goesToMenu = false;

	public string CurrentScene;

  public GameObject prefab_MasterAudio;

    public int numberOfHealthpots = 0;
    public string Companian1Name="";
    public string Companian2Name = "";
    public string Companian3Name = "";
    public string Companian4Name = "";
    int numberOfCompanions=0;
    public int money=0;




    /// <summary>
    /// The _instance.
    /// </summary>
    private static SceneStateManager _instance;
	
	/// <summary>
	/// Gets the instance.
	/// </summary>
	/// <value>The instance.</value>
	public static SceneStateManager instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<SceneStateManager>();
				
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}
			
			return _instance;
		}
	}

	/// <summary>
	/// Gets or sets the last checkpoint.
	/// </summary>
	/// <value>The last checkpoint.</value>
	public static Vector3 LastCheckpointCamera {
		get {
			return lastCheckpointCamera;
		}
		set {
			lastCheckpointCamera = value;
		}
		
	}
	
	/// <summary>
	/// The last checkpoint.
	/// </summary>
	private static Vector3 lastCheckpointCamera = Vector3.zero;

	/// <summary>
	/// Gets or sets the last checkpoint.
	/// </summary>
	/// <value>The last checkpoint.</value>
	public static Vector3 LastCheckpoint {
		get {
			return lastCheckpoint;
		}
		set {
			lastCheckpoint = value;
		}
		
	}
	
	/// <summary>
	/// The last checkpoint.
	/// </summary>
	private static Vector3 lastCheckpoint = Vector3.zero;


	private ISceneState _state = null;
	public ISceneState state {
		get{return _state;}
	}

	private ISceneState _lastState = null;
	public ISceneState lastState {
		get{return _lastState;}
	}


  private GUI currentSceneState;
  public LogicPlayer player1;


  public bool AmbientRelaxedIsPlaying = false;
  public bool AmbientBattleIsPlaying = false;
  public bool AmbientWindIsPlaying = false;



  void Awake() {
		Ini ();

	}

	void Ini() {
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}

    MasterAudio Ma = GameObject.FindObjectOfType<MasterAudio>();
	  if (Ma == null)
	  {
	    InitMasterAudio();
	  }
	  //Find my current state
	}


	// Use this for initialization
	void Start () {
        //_state = new Level1State ();
        //changeState (new Level1State (),false,false);
        //state.enterState (this);
        if (goesToMenu)
        {
            changeState(new TitleScreenState(), false, false);
        }

	}
	
	// Update is called once per frame
	void Update () {
		if (state != null) {
			state.update (this);
      CurrentScene = state.UnityScene;
    }
  }

	void FixedUpdate() {

		if (state != null) {
			state.fixedUpdate (this);
		}
        CharacterEngine[] players = GameObject.FindObjectsOfType<CharacterEngine>();

        players = GetFriendsFromCharacters(players);

        foreach (CharacterEngine player in players)
        {
            if (player.type == CharacterType.Player1)
            {
                LogicPlayer player1 = player.GetComponent<LogicPlayer>();
                this.money = player1.money;

            }
        }
    }

	void LateUpdate() {
		if (state != null) {
			state.lateUpdate (this);
		}	}

	void OnGUI() {
		if (state != null) {
			state.onGUI (this);
		}
		if (showGUI) {
			if (_state != null) {
				GUI.Label (new Rect (Screen.width / 2 - 65, Screen.height - 60, 130, 30), _state.ToString ());
			}

			if (_lastState != null) {
				GUI.Label (new Rect (Screen.width / 2 - 65, Screen.height - 30, 130, 30), _lastState.ToString ());
			}
		}

	}

	void OnLevelWasLoaded(int index) {

        
        Ini ();
		GoToCheckPoint ();
        SetCompanions();
        SetPlayerPotions();
        

        //Debug.Log("onlvl loaded " + state + " lvl " + Application.loadedLevelName);
        if (state != null)
	  {
	    state.onLevelWasLoaded(index, this);
	  }
	}


	public void changeStateAutonomously(string arg, bool loadAsynchron = true,bool setLastState = true) {
		switch (arg) {
		case "MenuScreen":
			changeState(new MenuScreenState(),setLastState,loadAsynchron);
			break;

		case "TitleScreen":
			changeState(new TitleScreenState(),setLastState,loadAsynchron);
			break;

		case "level01":
			changeState(new Level1State(),setLastState,loadAsynchron);
			break;

		case "level00_tutorial":
			changeState(new TutorialState(),setLastState,loadAsynchron);
			break;

		case "level00_zurich":
			changeState(new Level00Zurich(),setLastState,loadAsynchron);
			break;

		case "level00_alpine":
			changeState(new LevelAlpine(),setLastState,loadAsynchron);
			break;	

		case "level02":
			changeState(new Level2State(),setLastState,loadAsynchron);
			break;
			
		case "level03":
			changeState(new Level3State(),setLastState,loadAsynchron);
			break;
			
		case "level04":
			changeState(new Level4State(),setLastState,loadAsynchron);
			break;
			
		case "sequence1":
			changeState(new Sequence1State(),setLastState,loadAsynchron);
			break;
			
		case "sequence2":
			changeState(new Sequence2State(),setLastState,loadAsynchron);
			break;
			
		case "sequence3":
			changeState(new Sequence3State(),setLastState,loadAsynchron);
			break;
			
		case "Sequence4":
			changeState(new Sequence4State(),setLastState,loadAsynchron);
			break;
			
		case "Menu":
			changeState(new MenuScreenState(),setLastState,loadAsynchron);
			break;
			
		case "Title":
			changeState(new TitleScreenState(),setLastState,loadAsynchron);
			break;
		}
	}

	
	void GoToCheckPoint() {
        
        CharacterEngine[] players = GameObject.FindObjectsOfType<CharacterEngine> ();
		players = GetFriendsFromCharacters (players);
		if (lastCheckpoint != Vector3.zero) {
			foreach(CharacterEngine player in players) {
				player.gameObject.transform.position = lastCheckpoint;
			}
			Camera.main.transform.position = lastCheckpointCamera;
		}
	}

    public void SetPlayerPotions()
    {
        CharacterEngine[] players = GameObject.FindObjectsOfType<CharacterEngine>();
        players = GetFriendsFromCharacters(players);
        if (lastCheckpoint != Vector3.zero)
        {
            foreach (CharacterEngine player in players)
            {
                if (player.type == CharacterType.Player1)
                {
                    LogicPlayer player1 = player.GetComponent<LogicPlayer>();
                    player1.numberOfPotions = this.numberOfHealthpots;
                    player1.GetCharacter().onUsePotion += UsedPotion;
                    player1.money = this.money;

                } 
            }
        }
    }

    public void SetCompanions()
    {
        GameObject temp;
        GameObject tempname;
        temp = GameObject.Find("companion1");
        if (Companian1Name != "")
        {
            tempname = GameObject.Find("companion1name");
            temp.SetActive(true);
            tempname.GetComponent<GUIText>().text = Companian1Name;
        }
        temp = GameObject.Find("companion2");
        if (Companian2Name != "")
        {
            tempname = GameObject.Find("companion2name");
            temp.SetActive(true);
            tempname.GetComponent<GUIText>().text = Companian2Name;
        }
        temp = GameObject.Find("companion3");
        if (Companian3Name != "")
        {
            tempname = GameObject.Find("companion3name");
            temp.SetActive(true);
            tempname.GetComponent<GUIText>().text = Companian3Name;
        }
        temp = GameObject.Find("companion4");
        if (Companian4Name != "")
        {
            tempname = GameObject.Find("companion4name");
            temp.SetActive(true);
            tempname.GetComponent<GUIText>().text = Companian4Name;
        }
    }

    void UsedPotion()
    {
        this.numberOfHealthpots -= 1;
        if (numberOfHealthpots < 0)
        {
            numberOfHealthpots = 0;
        }
    }

    public void AddMercenarie(string aName)
    {
        numberOfCompanions += 1;
        switch (numberOfCompanions)
        {

            case 1:
                Companian1Name = aName;
                break;
            case 2:
                Companian2Name = aName;
                break;
            case 3:
                Companian3Name = aName;
                break;
            case 4:
                Companian4Name = aName;
                break;

            // default
            default:
                Debug.LogError("To many mercenaries");
                break;
        }

    }

    /// <summary>
    /// Changes the scene state.
    /// </summary>
    /// <param name="newState">New state.</param>
    /// <param name="setLastState">If set to <c>true</c> set last state.</param>
    public void changeState(ISceneState newState, bool setLastState = true, bool loadAsynchron = true)
	{
		
		if(_state != null)
		{
			_state.exitState(this);
		}

		if (setLastState && _state != null) {
			_lastState = _state;
			//Debug.Log ("Last state " + _lastState);
		}
        if (newState.isCamp)
        {
            Companian1Name = "";
            Companian2Name = "";
            Companian3Name = "";
            Companian4Name = "";
            numberOfCompanions = 0;
        }

		if (loadAsynchron) {
			_state = new LoadingState ();

			//Reset checkpoints
			lastCheckpoint = Vector3.zero;
			lastCheckpointCamera = Vector3.zero;

			//enter new state
			_state.enterStateAndLoad (this, newState);
		} else {
			_state = newState;
			_state.enterState(this);
		}
	}

	/// <summary>
	/// Changes the state the to last.
	/// </summary>
	public void changeToLastState(bool loadAsynchron = true, bool setLastState = true)
	{
		_state.exitState (this);
		ISceneState tempLastState = _state;


		if (loadAsynchron) {
			_state = new LoadingState ();
			//enter new state
			_state.enterStateAndLoad (this, _lastState);
		} else {
			_state = _lastState;

			if(setLastState) {
				_lastState = tempLastState;
			}

			_state.enterState(this);
		}


	}


	CharacterEngine[] GetFriendsFromCharacters(CharacterEngine[] chars) {
		List<CharacterEngine> res = new List<CharacterEngine> ();
		for (int i = 0; i < chars.Length; i++) {
			//Uncomment to also bring back Player2
			//if(chars[i].type == CharacterType.Player1 || chars[i].type == CharacterType.Player2) {

			if(chars[i].type == CharacterType.Player1 || chars[i].type == CharacterType.Player2) {
				res.Add(chars[i]);
			}
		}
		
		return res.ToArray();
	}

  private void InitMasterAudio() {
    GameObject mA = Instantiate(this.prefab_MasterAudio) as GameObject;
    DontDestroyOnLoad(mA);
    //Debug.Log("Init from MasterAudio from " + this);
  }

  public void PlayMenuAmbient(bool play)
  {
    PlayAmbient("ambient.relaxed", play);
  }

  public IEnumerator TriggerAmbient(string soundName)
  {
    // short w8 until everything is loaded
    yield return new WaitForSeconds(0.5f);

    MasterAudio.PlaySound(soundName);
  }

  private void CheckAmbientsArePlaying() {
    AmbientBattleIsPlaying = (MasterAudio.GetGroupInfo("ambient.battle").Group.ActiveVoices > 1);
    AmbientRelaxedIsPlaying = (MasterAudio.GetGroupInfo("ambient.relaxed").Group.ActiveVoices > 1);
    AmbientWindIsPlaying = (MasterAudio.GetGroupInfo("ambient.wind").Group.ActiveVoices > 1);
  }

  private void PlayAmbient(string ambientSound, bool play) {
    if (play) {
      MasterAudio.PlaySound(ambientSound);
    } else {
      MasterAudio.StopAllOfSound(ambientSound);
    }
  }

  /*
    private IEnumerator TriggerAmbient() {
      YieldInstruction _loopYield = new WaitForSeconds(1);

      while (true) {
        CheckAmbientsArePlaying();

        if (!AmbientRelaxedIsPlaying) {
          PlayAmbient("ambient.relaxed", true);
        } else {
          PlayAmbient("ambient.relaxed", false);
        }

        yield return _loopYield;
      }

    }



    */

}
