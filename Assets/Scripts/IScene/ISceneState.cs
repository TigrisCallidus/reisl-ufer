﻿using UnityEngine;
using System.Collections;

public interface ISceneState 
{
	string UnityScene { get; }
	string LoadingString { get; }
	int SequenceDuration { get; }
	SceneState AutomaticNextScene { get; }
    bool isCamp { get; }

    //string UnityScene { get; }
    void enterState(SceneStateManager sceneManager);
	void enterStateAndLoad(SceneStateManager sceneManager,ISceneState state);
	void update(SceneStateManager sceneManager);
	void lateUpdate(SceneStateManager sceneManager);
	void fixedUpdate(SceneStateManager sceneManager);
	void onGUI(SceneStateManager sceneManager);
	void exitState(SceneStateManager sceneManager);
    void onLevelWasLoaded(int lvl, SceneStateManager sceneManager);
}
