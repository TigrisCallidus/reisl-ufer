using UnityEngine;
using System.Collections;

public class CreditScreenState : SceneState {

  #region ISceneState implementation

  new public string LoadingString { get { return "Loading Credits..."; }}

  new public string UnityScene { get { return "CreditScreen"; }}
  new public int SequenceDuration { get { return 0; }}
  new public SceneState AutomaticNextScene { get { return null; }}

	public override void enterState (SceneStateManager sceneManager)
	{

	}

	public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
	
	}


	public override void update (SceneStateManager sceneManager)
	{

	}

	public override void lateUpdate (SceneStateManager sceneManager)
	{
	}

	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}

	public override void onGUI (SceneStateManager sceneManager)
	{
	}

	public override void exitState (SceneStateManager sceneManager)
	{
	}

  public override void onLevelWasLoaded(int lvl, SceneStateManager sceneManager) {
  }

  #endregion


}
