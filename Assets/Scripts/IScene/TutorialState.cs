﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;
using GameLab.HackAndSlashFramework;

public class TutorialState : SceneState, ISceneState
{

  new public string UnityScene { get { return "level00_zurich_day"; }}
  new public string LoadingString { get { return "Loading Neumarkt..."; }}
  new public int SequenceDuration { get { return 65; }}
  new public SceneState AutomaticNextScene { get { return null; }}

	public InputHandler controls;
	
	
	#region ISceneState implementation
	
	public override void enterState (SceneStateManager sceneManager)
	{
		GameMenuCanvas.instance.HideAllPanels ();
		deactivatePlayerPointLights ();
		Cursor.visible = false;
	}
	
	public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
		
	}
	
	public override void update (SceneStateManager sceneManager)
	{
		if(InControl.InputManager.ActiveDevice.GetControl(InControl.InputControlType.Start).WasPressed || Input.GetKeyDown(KeyCode.Escape)) {
			SceneStateManager.instance.changeState(new StartMenuScreenState(),true,false);
		}
	}
	
	public override void lateUpdate (SceneStateManager sceneManager)
	{
	}
	
	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}
	
	public override void onGUI (SceneStateManager sceneManager)
	{
	}
	
	public override void exitState (SceneStateManager sceneManager)
	{
	}
	
	void ISceneState.onLevelWasLoaded(int lvl, SceneStateManager sceneManager)
	{
		
		/*if (Application.loadedLevelName == UnityScene){
			
			if (MasterAudio.GetGroupInfo("ambient.wind").Group.ActiveVoices < 1){
				sceneManager.StartCoroutine(sceneManager.TriggerAmbient("ambient.wind"));
			}
			if (MasterAudio.GetGroupInfo("ambient.relaxed").Group.ActiveVoices < 1) {
				sceneManager.StartCoroutine(sceneManager.TriggerAmbient("ambient.relaxed"));
			}
		}*/
		
	}
	
	#endregion
	
	
}
