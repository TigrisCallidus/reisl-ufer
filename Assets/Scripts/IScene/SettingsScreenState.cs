﻿using UnityEngine;
using System.Collections;

public class SettingsScreenState : SceneState {

  #region ISceneState implementation
  new public string UnityScene { get { return "SettingScreen"; }}
  new public string LoadingString { get { return "Loading Settings..."; }}
  new public int SequenceDuration { get { return 65; }}
  new public SceneState AutomaticNextScene { get { return null; }}

	public override void enterState (SceneStateManager sceneManager)
	{

	}

	public override void update (SceneStateManager sceneManager)
	{

	}

	public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
		
	}

	public override void lateUpdate (SceneStateManager sceneManager)
	{
	}

	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}

	public override void onGUI (SceneStateManager sceneManager)
	{
	}

	public override void exitState (SceneStateManager sceneManager)
	{
	}

  public override void onLevelWasLoaded(int lvl, SceneStateManager sceneManager) {
  }

  #endregion


}
