﻿using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework;
public class Level00Zurich : SceneState, ISceneState {

  #region ISceneState implementation

  new public string UnityScene { get { return "level00_zurich_night"; }}
  new public string LoadingString { get { return "Loading Zürich..."; }}
  new public int SequenceDuration { get { return 0; }}
  new public SceneState AutomaticNextScene { get { return null; }}


	public override void enterState (SceneStateManager sceneManager)
	{
    //Debug.Log("enterState: " + this + " " + this.UnityScene);
		deactivatePlayerPointLights ();

    sceneManager.PlayMenuAmbient(false);
  }

  public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) 
	{
	}

	public override void update (SceneStateManager sceneManager)
	{
		if(InControl.InputManager.ActiveDevice.GetControl(InControl.InputControlType.Start).WasPressed || Input.GetKeyDown(KeyCode.Escape)) {
			SceneStateManager.instance.changeState(new StartMenuScreenState(),true,false);
		}
	}

	public override void lateUpdate (SceneStateManager sceneManager)
	{
	}

	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}

	public override void onGUI (SceneStateManager sceneManager)
	{
	}

	public override void exitState (SceneStateManager sceneManager)
	{
	}

  public override void onLevelWasLoaded(int lvl, SceneStateManager sceneManager) {
  }

  #endregion


}
