﻿using UnityEngine;
using System.Collections;

public class TitleScreenState : SceneState {

  #region ISceneState implementation
  new public string UnityScene { get { return "MenuScreen"; }}
  new public string LoadingString { get { return "Loading Title-Screen..."; }}
  new public int SequenceDuration { get { return 65; }}
	new public SceneState AutomaticNextScene { get { return new MenuScreenState();; }}

	public override void enterState (SceneStateManager sceneManager)
	{
		if(!GameMenuCanvas.instance.TitleScreenPanel.activeSelf) {
			GameMenuCanvas.instance.ShowPanel(GameMenuCanvas.instance.TitleScreenPanel);
		}

	  sceneManager.PlayMenuAmbient(true);
	}

	public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
		
	}

	public override void update (SceneStateManager sceneManager)
	{
		if (Input.anyKey) {
			SceneStateManager.instance.changeState(AutomaticNextScene,true,false);
		}
	}

	public override void lateUpdate (SceneStateManager sceneManager)
	{
	}

	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}

	public override void onGUI (SceneStateManager sceneManager)
	{
	}

	public override void exitState (SceneStateManager sceneManager)
	{
	}

  public override void onLevelWasLoaded(int lvl, SceneStateManager sceneManager) {

  }

  #endregion


}
