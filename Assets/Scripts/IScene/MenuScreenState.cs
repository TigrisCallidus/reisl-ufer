﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuScreenState : SceneState, ISceneState {

  new public string UnityScene { get { return "MenuScreen"; }}
  new public string LoadingString { get { return "Loading Menu..."; }}
  new public int SequenceDuration { get { return 0; }}
  new public SceneState AutomaticNextScene { get { return null; }}

  #region ISceneState implementation


  /*
	private int ButtonWidth = 180;
	private int ButtonHeight = 30;

	private int topPadding = 30;
  */

  private List<ISceneState> stateList;
		

	public override void enterState (SceneStateManager sceneManager)
	{
		GameMenuCanvas.instance.ShowPanel (GameMenuCanvas.instance.MenuPanel);
		SceneStateManager.LastCheckpoint = Vector3.zero;
		Cursor.visible = true;
		GameObject.Find ("Start game").GetComponent<UnityEngine.UI.Button> ().Select ();

	}

	public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
		
	}

	public override void update (SceneStateManager sceneManager)
	{


		if (InControl.InputManager.ActiveDevice.GetControl (InControl.InputControlType.Action2).WasReleased || Input.GetKeyUp (KeyCode.Escape)) {
			SceneStateManager.instance.changeState(new TitleScreenState(),true,false);
		}

	}

	public override void lateUpdate (SceneStateManager sceneManager)
	{
	}

	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}

	public override void onGUI (SceneStateManager sceneManager)
	{
		/*
		int i = 0;
		foreach (ISceneState state in stateList) {
			if(GUI.Button(new Rect(Screen.width/2 - ButtonWidth/2,topPadding+(i*(ButtonHeight+10)),ButtonWidth,ButtonHeight),state.ToString())) {
				SceneStateManager.instance.changeState(state,false);
			}
			   i++;
		}
		if(GUI.Button(new Rect(Screen.width/2 - ButtonWidth/2,topPadding+(i*(ButtonHeight+10)),ButtonWidth,ButtonHeight),"Go to last state")) {
			SceneStateManager.instance.changeState(SceneStateManager.instance.lastState,false);
		}
		*/
	}

	public override void exitState (SceneStateManager sceneManager)
	{
		//GameMenuCanvas.instance.HideAllPanels ();

  }

  void ISceneState.onLevelWasLoaded(int lvl, SceneStateManager sceneManager) {
  }


	#endregion


}
