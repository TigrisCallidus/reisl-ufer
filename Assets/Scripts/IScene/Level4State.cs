﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class Level4State : SceneState, ISceneState {

  new public string UnityScene { get { return "level_campsite_filipe"; }}
  new public string LoadingString { get { return "Loading campsite..."; }}
  new public int SequenceDuration { get { return 0; }}
  new public SceneState AutomaticNextScene { get { return null; }}
  new public bool isCamp { get { return true; } }

    #region ISceneState implementation

    public override void enterState(SceneStateManager sceneManager) {
		GameMenuCanvas.instance.HideAllPanels ();
		Cursor.visible = false;
	}

	public override void enterStateAndLoad (SceneStateManager sceneManager, ISceneState state) {
		
	}

  public override void update (SceneStateManager sceneManager)
	{
		if(InControl.InputManager.ActiveDevice.GetControl(InControl.InputControlType.Start).WasPressed) {

			SceneStateManager.instance.changeState(new StartMenuScreenState(),true,false);
		}
	}

	public override void lateUpdate (SceneStateManager sceneManager)
	{
	}

	public override void fixedUpdate (SceneStateManager sceneManager)
	{
	}

	public override void onGUI (SceneStateManager sceneManager)
	{
	}

	public override void exitState (SceneStateManager sceneManager)
	{
	}

	public override void onLevelWasLoaded(int lvl, SceneStateManager sceneManager) {

    if (Application.loadedLevelName == UnityScene) {

      if (MasterAudio.GetGroupInfo("ambient.wind").Group.ActiveVoices < 1) {
        sceneManager.StartCoroutine(sceneManager.TriggerAmbient("ambient.wind"));
      }
      if (MasterAudio.GetGroupInfo("ambient.relaxed").Group.ActiveVoices < 1) {
        sceneManager.StartCoroutine(sceneManager.TriggerAmbient("ambient.relaxed"));
      }

    }
  }

  #endregion


}
