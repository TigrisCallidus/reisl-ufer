﻿using UnityEngine;
using System.Collections;

public class Init : MonoBehaviour {

    [Range(30,90)]
    public int targetFrameRate = 60;
    [Range(0, 2)]
    public int vSync = 0;

    void Awake() {

        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = this.targetFrameRate;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
