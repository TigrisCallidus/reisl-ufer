﻿using UnityEngine;
using System.Collections;

public enum GroundType {
	Dirt,
	Water,
	Wood,
	Cement,
	Snow
}

public class GroundProperties : MonoBehaviour {

	public GroundType type;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision collision)
	{
		LogicPlayer logicPlayer = collision.gameObject.GetComponent<LogicPlayer>();
		if (logicPlayer != null)
		{
			logicPlayer.groundType = type;
		}
	}
}
