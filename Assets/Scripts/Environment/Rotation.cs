﻿using UnityEngine;
using System.Collections;

public enum Axis {

	X,
	Y, 
	Z
}

public class Rotation : MonoBehaviour {

	public Axis RotationAxis;
	public float RotationSpeed = 0.1f;
	
	private Vector3 AppliedRotation;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		Rotate (RotationAxis, RotationSpeed);
	}

	void Rotate( Axis axis, float speed)
	{
		switch (axis) 
		{
		case Axis.X:
			AppliedRotation = new Vector3(speed, 0f, 0f);
			break;
		case Axis.Y:
			AppliedRotation = new Vector3(0f, speed, 0f);
			break;
		case Axis.Z:
			AppliedRotation = new Vector3(0f, 0f, speed);
			break;
		}

		transform.Rotate(AppliedRotation);

	}
}
