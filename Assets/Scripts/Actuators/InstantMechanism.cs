﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Instantly reacts to defined Actuators and changes GameObjects from Active to Inactive or vice-versa.
/// </summary>
public class InstantMechanism : Mechanism {

	/// <summary>
	/// The active representation.
	/// </summary>
	public GameObject ActiveRepresentation;
	
	/// <summary>
	/// The inactive representation.
	/// </summary>
	public GameObject InactiveRepresentation;

	public bool UpdateCollider = false;

	void Awake() {
		UpdateRepresentation ();
	}

	public override void UpdateRepresentation() {

		ActiveRepresentation.SetActive(this.IsActive);
		InactiveRepresentation.SetActive(!this.IsActive);
		if (UpdateCollider)GetComponent<Collider>().enabled = !this.IsActive;

	}

}

