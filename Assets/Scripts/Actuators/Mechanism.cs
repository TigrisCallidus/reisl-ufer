﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Base Mechanism class.
/// </summary>
public abstract class Mechanism : MonoBehaviour {

	/// <summary>
	/// The actuators.
	/// </summary>
	public List<Actuator> Actuators;

	/// <summary>
	/// Wether it is active or not.
	/// </summary>
	public bool IsActive {
		get { return isActive; }
		set {
				isActive = value;
				UpdateRepresentation();
			}
	}

	/// <summary>
	/// Wether it is active or not.
	/// </summary>
	private bool isActive;




	void Awake() {
		UpdateRepresentation();
	}

	/// <summary>
	/// Updates the mechanism.
	/// </summary>
	public virtual void UpdateMechanism() {
		bool res = true;
		foreach (Actuator A in Actuators) {
			if(!A.IsActive) {
				res = false;
			}
		}
		IsActive = res;
	}


	/// <summary>
	/// Updates the representation.
	/// </summary>
	public virtual void UpdateRepresentation() {

	}
}
