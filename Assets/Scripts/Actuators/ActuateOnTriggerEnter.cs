﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Gets actuated on trigger enter.
/// </summary>
[RequireComponent(typeof(BoxCollider))]
public class ActuateOnTriggerEnter : Actuator {

	/// <summary>
	/// The allowed layers which can teleport.
	/// </summary>
	public List<int> AllowedLayers;

	void OnTriggerEnter(Collider Col) {
		//Check if Object is allwed to teleport
		if (AllowedLayers.Contains (Col.gameObject.layer)) {
			this.IsActive = true;
		}
	}

}
