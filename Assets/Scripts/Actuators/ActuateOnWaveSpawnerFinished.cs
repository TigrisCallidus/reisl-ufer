﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Gets actuated on trigger enter.
/// </summary>
[RequireComponent(typeof(WaveSpawner))]
public class ActuateOnWaveSpawnerFinished : Actuator {

	private WaveSpawner waveSpawner;

	protected override void Awake()
	{
	  base.Awake();
		waveSpawner = GetComponent<WaveSpawner> ();
	}

	void Start() {
		waveSpawner.OnSpawnerFinished += Actuate;
	}

	private void Actuate(object sender) {
		this.IsActive = true;
		//Debug.Log ("Spawner finished");
	}



}
