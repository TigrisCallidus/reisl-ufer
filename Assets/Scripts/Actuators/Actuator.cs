﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Base Actuator class. 
/// </summary>
public abstract class Actuator : LogicBase  {

	/// <summary>
	/// Gets or sets a value indicating whether this instance is active.
	/// </summary>
	/// <value><c>true</c> if this instance is active; otherwise, <c>false</c>.</value>
	public virtual bool IsActive {
		get { return isActive; }
		set {
			isActive = value;
			foreach(Mechanism M in Mechanisms) {
				M.UpdateMechanism();
			}
			UpdateRepresentation();
		}
	}

	/// <summary>
	/// The is active.
	/// </summary>
	private bool isActive;

	/// <summary>
	/// The mechanisms.
	/// </summary>
	public List<Mechanism> Mechanisms;

	/*
	/// <summary>
	/// The active representation.
	/// </summary>
	public GameObject ActiveRepresentation;

	/// <summary>
	/// The inactive representation.
	/// </summary>
	public GameObject InactiveRepresentation;
	*/

	/// <summary>
	/// Updates the representation.
	/// </summary>
	public virtual void UpdateRepresentation() {
		if (isActive) {
			//Show active Representation of Actuator (Lever for example);


		} else {
			//Show inactive Representation;
		}
	}
	
}
