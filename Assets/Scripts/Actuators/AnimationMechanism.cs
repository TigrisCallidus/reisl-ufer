﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Reacts to defined Actuators and Plays defined Animations depending on IsActive boolen.
/// </summary>
[RequireComponent(typeof(Animator))]
public class AnimationMechanism : Mechanism {
	

	public AnimationClip ActivationAnimation;
	public AnimationClip DeactivationAnimation;

	private Animator animator;


	void Awake() {
		animator = GetComponent<Animator> ();
		UpdateRepresentation ();
	}



	// <summary>
	/// Updates the representation.
	/// </summary>
	public override void UpdateRepresentation() {
		if (this.IsActive) {
			//Play activation Animation
			animator.Play (ActivationAnimation.name);
		} else {
			//Play deactivation Animation
			animator.Play (DeactivationAnimation.name);
		}
		
	}
}
