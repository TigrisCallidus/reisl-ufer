﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Reacts to defined Actuators and Plays defined Animations depending on IsActive boolen.
/// </summary>
[RequireComponent(typeof(EnemySpawner))]
public class SpawnMechanism : Mechanism {

	private EnemySpawner spawner;

	void Awake() {
		spawner = GetComponent<EnemySpawner> ();
		UpdateRepresentation ();
	}



	// <summary>
	/// Updates the representation.
	/// </summary>
	public override void UpdateRepresentation() {
		spawner.IsSpawning = this.IsActive;
	}
}
