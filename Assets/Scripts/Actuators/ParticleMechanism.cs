﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Reacts to defined Actuators and Plays defined Animations depending on IsActive boolen.
/// </summary>
[RequireComponent(typeof(ParticleSystem))]
public class ParticleMechanism : Mechanism {

	private ParticleSystem ps;

	void Awake() {
		ps = GetComponent<ParticleSystem> ();
		UpdateRepresentation ();
	}



	// <summary>
	/// Updates the representation.
	/// </summary>
	public override void UpdateRepresentation() {
		if (this.IsActive) {
			ps.Play ();
		}
	}
}
