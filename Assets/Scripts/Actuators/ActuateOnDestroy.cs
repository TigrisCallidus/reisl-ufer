﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Gets actuated on destroy.
/// </summary>
public class ActuateOnDestroy : Actuator {

	void OnDestroy() {
		this.IsActive = true;
	}

}
