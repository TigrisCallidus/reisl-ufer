﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Adds Force to a Rigidbody if activated.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class ForcePushMechanism : Mechanism {

	public Vector3 Force;
	public Vector3 Torque;
	public ForceMode forceMode;
	public bool ConstantForce;

	void Awake() {
		UpdateRepresentation ();
	}

	public override void UpdateRepresentation() {
		Rigidbody R = GetComponent<Rigidbody> ();
		if (this.IsActive) {
			R.isKinematic = false;
			if(Force != Vector3.zero) {
				R.AddForce(Force*R.mass,forceMode);
			}
			if(Torque != Vector3.zero) {
				R.AddTorque(Torque,forceMode);
			}
		}

	}

	void FixedUpdate() {
		if (ConstantForce && this.IsActive) {
			Rigidbody R = GetComponent<Rigidbody> ();

			R.AddForce(Force*R.mass,forceMode);
			if(Torque != Vector3.zero) {
				R.AddTorque(Torque,forceMode);
			}
		}
	}

}

