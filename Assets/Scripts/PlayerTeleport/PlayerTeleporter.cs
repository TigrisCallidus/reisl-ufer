﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider))]
public class PlayerTeleporter : MonoBehaviour {

	/// <summary>
	/// The Target node which also needs a PlayerTeleporter Script
	/// </summary>
	public PlayerTeleporter TargetNode;

	/// <summary>
	/// Wether it is a one way teleporter or not. Default is false.
	/// </summary>
	public bool OneWayTeleport = false;


	/// <summary>
	/// Indicates wether the teleporter is active or not
	/// </summary>
	[HideInInspector]
	public bool CanTeleportNow;


	/// <summary>
	/// The allowed layers which can teleport.
	/// </summary>
	public List<int> AllowedLayers;


	// Use this for initialization
	void Awake () {
		CanTeleportNow = true;
		if (OneWayTeleport) {
			TargetNode.GetComponent<Collider>().enabled = false;
		}
	}


	void OnTriggerEnter(Collider Col) {
		//Check if Object is allwed to teleport
		if (AllowedLayers.Contains (Col.gameObject.layer) && CanTeleportNow) {
			TargetNode.CanTeleportNow = false;
			Col.gameObject.transform.position = TargetNode.transform.position;
			Col.gameObject.transform.rotation = TargetNode.transform.rotation;
		}
	}

	void OnTriggerExit(Collider Col) {
		if (AllowedLayers.Contains (Col.gameObject.layer)) {
			CanTeleportNow = true;
		}
	}
}
