﻿/*
 * +================================================================================+
 *      __  ___  ______________  __  __  ______________   ____
 *     / / / / |/ /  _/_  __/\ \/ / / / / /_  __/  _/ /  / __/
 *    / /_/ /    // /  / /    \  / / /_/ / / / _/ // /___\ \
 *    \____/_/|_/___/ /_/     /_/  \____/ /_/ /___/____/___/
 *
 * +================================================================================+
 *
 *    "THE BEER-WARE LICENSE"
 *    -----------------------
 *
 *    As long as you retain this notice you can do whatever you want with this
 *    stuff. If you meet any of the authors some day, and you think this stuff is
 *    worth it, you can buy them a beer in return.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *    THE SOFTWARE.
 *
 * +================================================================================+
 *
 *    Authors:
 *    David Krummenacher (dave@timbookedtwo.com)
 *
 */

using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Pausable behaviour. Let's you pause and resume a game object.
/// Pauses animators, rigidbodies, particle effects, audio sources and mono behaviours on the object.
/// </summary>
public class Pausable : MonoBehaviour {

	/// <summary>
	/// A list of all pausables.
	/// </summary>
	private static List<Pausable> Pausables = new List<Pausable>();

	[Tooltip("Determines wheter to deactivate the renderer on pause.")]
	/// <summary>
	/// Determines wheter to deactivate the renderer on pause.
	/// </summary>
	public bool DeactivateRenderer = false;

	/// <summary>
	/// Toggles the all pausables states.
	/// </summary>
	public static void TogglePauseAll() {
		foreach (Pausable pausable in Pausables) pausable.TogglePause();
	}

	/// <summary>
	/// Pauses all pausables.
	/// </summary>
	public static void PauseAll() {
		foreach (Pausable pausable in Pausables) pausable.Pause();
	}

	/// <summary>
	/// Resumes all pausables.
	/// </summary>
	public static void ResumeAll() {
		foreach (Pausable pausable in Pausables) pausable.Resume();
	}

	/// <summary>
	/// Clears all.
	/// </summary>
	public static void ClearAll() {
		/*
		ResumeAll ();
			foreach (Pausable pausable in Pausables) {
				lock (pausable.monoBehaviourEnabled) {
					pausable.monoBehaviourEnabled.Clear ();
				}
			}

		Pausable.Pausables.Clear ();
		*/
	}

	/// <summary>
	/// If the game object paused.
	/// </summary>
	private bool paused = false;
	/// <summary>
	/// Gets a value indicating whether this instance is paused.
	/// </summary>
	/// <value><c>true</c> if this instance is paused; otherwise, <c>false</c>.</value>
	public bool IsPaused {
		get { return paused; }
	}

	/// <summary>
	/// Temporary storage for the animator speed.
	/// </summary>
	private float animatorSpeed;
	/// <summary>
	/// Indicates whether the temporary values of the animator have been set.
	/// </summary>
	private bool animatorHasBeenSet = false;
	/// <summary>
	/// Temporary storage for the rigidbody velocity.
	/// </summary>
	private Vector3 rigidbodyVelocity;
	/// <summary>
	/// Temporary storage for the rigidbody angular velocity.
	/// </summary>
	private Vector3 rigidbodyAngularVelocity;
	/// <summary>
	/// Temporary storage that dermines wheter the rigidbody was kinematic.
	/// </summary>
	private bool rigidbodyIsKinematic;
	/// <summary>
	/// Indicates whether the temporary values of the rigidbody have been set.
	/// </summary>
	private bool rigidbodyHasBeenSet = false;
	/// <summary>
	/// Temporary storage that dermines wheter the particle system was playing.
	/// </summary>
	private bool particleSystemIsPlaying;
	/// <summary>
	/// Indicates whether the temporary values of the paricle system have been set.
	/// </summary>
	private bool particleSystemHasBeenSet = false;
	/// <summary>
	/// Temporary storage that dermines wheter the audio source was playing.
	/// </summary>
	private bool audioSourceIsPlaying;
	/// <summary>
	/// Indicates whether the temporary value of the audio source have been set.
	/// </summary>
	private bool audioSourceHasBeenSet = false;
	/// <summary>
	/// An array of all mono behaviours enabled state.
	/// </summary>
	private Dictionary<MonoBehaviour, bool> monoBehaviourEnabled = new Dictionary<MonoBehaviour, bool>();
	/// <summary>
	/// If the renderer is visible.
	/// </summary>
	private bool rendererIsEnabled;
	/// <summary>
	/// If the renderer has been set.
	/// </summary>
	private bool rendererHasBeenSet = false;

	void OnEnable() {
		// Add yourself to the pausables
		if (!Pausables.Contains(this)) Pausables.Add(this);
	}

	void OnDisable() {
		// Remove yourself from the pausables
		if (Pausables.Contains(this)) Pausables.Remove(this);
	}

	void OnDestroy() {
		if (Pausables.Contains(this)) Pausables.Remove(this);
	}



	/// <summary>
	/// Toggles the pause state.
	/// </summary>
	public void TogglePause() {
		if (paused) Resume();
		else Pause();
	}

	/// <summary>
	/// Pauses this instance.
	/// </summary>
	public void Pause() {
		if (gameObject.activeSelf) {
			// Pause animator if possible
			Animator animator = GetComponent<Animator> ();
			if (animator != null) {
				animatorSpeed = animator.speed;
				animatorHasBeenSet = true;

				animator.speed = 0f;
			}

			// Pause rigidbody if possible
			Rigidbody rigidbody = GetComponent<Rigidbody> ();
			if (rigidbody != null) {
				rigidbodyVelocity = rigidbody.velocity;
				rigidbodyAngularVelocity = rigidbody.angularVelocity;
				rigidbodyIsKinematic = rigidbody.isKinematic;
				rigidbodyHasBeenSet = true;

				rigidbody.velocity = Vector3.zero;
				rigidbody.angularVelocity = Vector3.zero;
				rigidbody.isKinematic = true;
			}

			// Pause particle system if possible
			ParticleSystem particlesSystem = GetComponent<ParticleSystem> ();
			if (particlesSystem != null) {
				particleSystemIsPlaying = particlesSystem.isPlaying;
				particleSystemHasBeenSet = true;

				if (particleSystemIsPlaying)
					particlesSystem.Pause (true);
			}

			// Pause audio source if possible
			AudioSource audioSource = GetComponent<AudioSource> ();
			if (audioSource != null) {
				audioSourceIsPlaying = audioSource.isPlaying;
				audioSourceHasBeenSet = true;

				if (audioSourceIsPlaying)
					audioSource.Pause ();
			}

			// Pause mono behaviours if possible
			lock(monoBehaviourEnabled) {
				monoBehaviourEnabled.Clear ();
				foreach (MonoBehaviour behaviour in GetComponents<MonoBehaviour>()) {
					monoBehaviourEnabled.Add (behaviour, behaviour.enabled);
					if (behaviour == this)
						continue;
					behaviour.enabled = false;
				}
			}

			// Deactivate renderer if necessary
			if (DeactivateRenderer) {
				Renderer renderer = GetComponent<Renderer> ();
				if (renderer != null) {
					rendererIsEnabled = renderer.enabled;
					renderer.enabled = false;
					rendererHasBeenSet = true;
				}
			}

			// Indicate that this instance is paused
			paused = true;
		}
	}

	/// <summary>
	/// Resumes this instance.
	/// </summary>
	public void Resume() {
		if (gameObject.activeSelf) {
			// Resume animator if possible
			Animator animator = GetComponent<Animator> ();
			if (animator != null && animatorHasBeenSet) {
				animator.speed = animatorSpeed;
				animatorHasBeenSet = false;
			}

			// Resume rigidbody if possible
			Rigidbody rigidbody = GetComponent<Rigidbody> ();
			if (rigidbody != null && rigidbodyHasBeenSet) {
				rigidbody.velocity = rigidbodyVelocity;
				rigidbody.angularVelocity = rigidbodyAngularVelocity;
				rigidbody.isKinematic = rigidbodyIsKinematic;
				rigidbodyHasBeenSet = false;
			}

			// Resume particle system if possible
			ParticleSystem particlesSystem = GetComponent<ParticleSystem> ();
			if (particlesSystem != null && particleSystemHasBeenSet) {
				if (particleSystemIsPlaying)
					particlesSystem.Play (true);
				particleSystemHasBeenSet = false;
			}

			// Pause audio source if possible
			AudioSource audioSource = GetComponent<AudioSource> ();
			if (audioSource != null && audioSourceHasBeenSet) {
				if (audioSourceIsPlaying)
					audioSource.Play ();
				audioSourceHasBeenSet = false;
			}

			// Deactivate renderer if necessary
			Renderer renderer = GetComponent<Renderer> ();
			if (renderer != null && rendererHasBeenSet) {
				renderer.enabled = rendererIsEnabled;
				rendererHasBeenSet = false;
			}

			// Resume mono behaviours if possible
			lock(monoBehaviourEnabled) {
				foreach (MonoBehaviour behaviour in GetComponents<MonoBehaviour>()) {
				if (behaviour == this)
					continue;
				if (monoBehaviourEnabled.ContainsKey (behaviour)) {
					behaviour.enabled = monoBehaviourEnabled [behaviour];
				} else {
					Debug.Log (ToString () + "MonoBehaviours list for enabling states isn't available. Consider calling resume only after calling pause.");
				}
			}
			monoBehaviourEnabled.Clear ();
			}
		
			// Indicate that this instance isn't paused
			paused = false;
		}
	}


}
