﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GameLab.HackAndSlashFramework;


public class PlayerHUD : iTweenable {

  #region Singleton

  private static PlayerHUD instance = null;

  public static PlayerHUD Instance {
    get {
      return instance;
    }
  }

  #endregion

  #region public_variables

  public static readonly string UI_HEALTH = "Health";
  public static readonly string UI_FAITH = "Faith";
  public static readonly string UI_PLAYER = "Player";
  public static readonly string UI_MONEY = "Money";
  public static readonly string UI_GODRAGE = "Godrage";
  public static readonly string UI_GODRAGE_READY = "GodragePC";


  public static readonly string UI_BG = "Background";
  public static readonly string UI_BAR = "Bar";
  public static readonly string UI_ICON = "Icon";
  public static readonly string UI_TEXT = "Text";

  public static readonly string UI_P1 = "UI_p1";
  public static readonly string UI_P2 = "UI_p2";
	public static readonly string UI_FIGHTSPIRIT = "UI_FightingSpirit";
  public static readonly string UI_SHOP = "UI_shop";

  public static readonly string UI_COMMENTS = "UI_Comments";
  public static readonly string COMMENT_1 = "UI_1er";
  public static readonly string COMMENT_2 = "UI_2er";
  public static readonly string COMMENT_3 = "UI_3er";

	public static readonly string UI_COMMENTATOR = "UI_Commentator";

    public GameObject GodRageCanBeactivated;
    public Text Questtext;
    public Text HealthPotNumber;
    public Text NumberOfMoney;
    bool godRageShown = true;


  public static readonly string P1_BLOODY = "p1BloodyIndicator";
  public static readonly string P2_BLOODY = "p2BloodyIndicator";

  [Tooltip("How long is a one liner comment showed, the SlideTiming is only the half for the oneliner")]
  public float CommentTime1 = 3;

  [Tooltip("How long is a two liner comment showed")]
  public float CommentTime2 = 6;

  [Tooltip("How long is a three liner comment showed")]
  public float CommentTime3 = 10;

  [Range(1, 3)]
  public float SlideTiming = 2;

  public iTween.EaseType SlideInEaseType;

  #endregion


  #region private_variables

  private float YOutPos = 200;

  private Transform uiPlayer2;
  private Transform uiPlayer1;
  private Transform uiP1GodRageIndicator;
  private Transform uiP2GodRageIndicator;

    private Transform uiPlayer1GodrageReady;

  private Transform uiP1HitIndicator;
  private Transform uiP2HitIndicator;

  private Transform uiComments;
  protected RectTransform uiCommentsRect;
  protected Vector3 CommentOriginalPos;

  private Transform uiCommentator;
  protected Vector3 CommentatorOriginalPos;

	private Transform p1Health;
  private Transform p1Faith;
  private Transform p1Money;

  private Transform fightingSpiritButtonUI;

  private GameObject player2Pnl;
  private Transform p2Health;
  private Transform p2Faith;
  private Transform p2Money;


  private float maxFaith = 1;
  private float maxHealth = 1;
  private float maxFightingSprit = 3;

  private LogicPlayer player1Script;
  private LogicPlayer player2Script;

  private GameLogic gameLogic;
  private CharacterEngine characterEngine;

  private Transform uiShop;


  #endregion

  void Awake()
  {
    instance = this;
  }

  // Use this for initialization
  void Start() {

    gameLogic = GameObject.FindObjectOfType<GameLogic>();

    LogicPlayer[] gos = GameObject.FindObjectsOfType<LogicPlayer>();
    foreach (LogicPlayer go in gos) {
      if (go.GetCharacter().type == CharacterType.Player1) {
        this.player1Script = go;
      }

      if (go.GetCharacter().type == CharacterType.Player2) {
        this.player2Script = go;
      }

    }

    LinkUI();

    StartCoroutine(SlideCommentUI(false, 0));
	  StartCoroutine (SlideCommentator (false, 0));
    StartCoroutine(base.CheckTweenQueue());

    //ShowCommmentator("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,");
  }

  private void LinkUI() {

    this.uiPlayer1GodrageReady= GetUIElement(this.gameObject, UI_GODRAGE_READY);

    this.uiPlayer1 = GetUIElement(this.gameObject, UI_P1);
    this.uiPlayer2 = GetUIElement(this.gameObject, UI_P2);

    this.uiComments = GetUIElement(this.gameObject, UI_COMMENTS);
    this.uiCommentsRect = this.uiComments.GetComponent<RectTransform>();
    CommentOriginalPos = this.uiCommentsRect.anchoredPosition3D;

    this.uiShop = GetUIElement(this.gameObject, UI_SHOP);
    toggleGoActive(this.uiShop.gameObject);

    uiP1GodRageIndicator = GetUIElement(GetUIElement(this.uiPlayer1.gameObject, UI_PLAYER).gameObject, UI_BAR);
    uiP2GodRageIndicator = GetUIElement(GetUIElement(this.uiPlayer2.gameObject, UI_PLAYER).gameObject, UI_BAR);

    uiP1HitIndicator = GetUIElement(this.gameObject, P1_BLOODY);
    uiP2HitIndicator = GetUIElement(this.gameObject, P2_BLOODY);


    //this.fightingSpiritButtonUI = GetUIElement(this.gameObject, UI_FIGHTSPIRIT);

    this.p1Faith = GetUIElement(this.uiPlayer1.gameObject, UI_FAITH);
    this.p1Health = GetUIElement(this.uiPlayer1.gameObject, UI_HEALTH);
    this.p1Money = GetUIElement(this.uiPlayer1.gameObject, UI_MONEY);

    this.p2Faith = GetUIElement(this.uiPlayer2.gameObject, UI_FAITH);
    this.p2Health = GetUIElement(this.uiPlayer2.gameObject, UI_HEALTH);
    this.p2Money = GetUIElement(this.uiPlayer2.gameObject, UI_MONEY);
	
	this.uiCommentator = GetUIElement (this.gameObject, UI_COMMENTATOR);
	CommentatorOriginalPos = this.uiCommentator.GetComponent<RectTransform>().anchoredPosition3D;

    if (this.player1Script == null && this.uiPlayer1.gameObject.activeSelf)
    {
      toggleGoActive(this.uiPlayer1.gameObject);
    }
    if (this.player1Script != null) {
      player1Script.GetCharacter().onCharacterDamage += P1OnDamage;
    }

    if (this.player2Script == null && this.uiPlayer2.gameObject.activeSelf) {
      toggleGoActive(this.uiPlayer2.gameObject);
    }
    if (this.player2Script != null) {
      player2Script.GetCharacter().onCharacterDamage += P2OnDamage;
    }

    toggleGoActive(this.uiP1HitIndicator.gameObject);
    toggleGoActive(this.uiP2HitIndicator.gameObject);

  }


  protected Transform GetUIElement(GameObject itemUI, string elementName) {
    return itemUI.transform.FindChild(elementName);
  }

  // Update is called once per frame
  void FixedUpdate()
  {
    updatePlayers();

    updateShop();

    checkGodRage();
  }

  private void checkGodRage()
  {

    if (this.uiPlayer1.gameObject.activeSelf) {
      if (uiP1GodRageIndicator.gameObject.activeSelf != this.player1Script.godlyRage)
      {
        toggleGoActive(uiP1GodRageIndicator.gameObject);
      }
    }

    if (this.uiPlayer2.gameObject.activeSelf)
    {
      if (uiP2GodRageIndicator.gameObject.activeSelf != this.player2Script.godlyRage)
      {
        toggleGoActive(uiP2GodRageIndicator.gameObject);
      }
    }

  }

  private void toggleGoActive(GameObject go)
  {
      go.SetActive(!go.activeSelf);
  }

  private void P1OnDamage(CharacterAttack attack){

    if (this.uiPlayer1.gameObject.activeSelf){
      StartCoroutine(ShowBloodyIndicator(uiP1HitIndicator));
    }

  }

  private void P2OnDamage(CharacterAttack attack) {

    if (this.uiPlayer2.gameObject.activeSelf)
    {
      StartCoroutine(ShowBloodyIndicator(uiP2HitIndicator));
    }

  }

  private IEnumerator ShowBloodyIndicator(Transform indicator, float time = 1)
  {
    if (!indicator.gameObject.activeSelf)
    {
      toggleGoActive(indicator.gameObject);

      yield return new WaitForSeconds(time);

      toggleGoActive(indicator.gameObject);
    }
  }


  public void ShowShop(bool visible)
  {
    this.uiShop.gameObject.SetActive(visible);
  }

  private void updateShop()
  {
    /*
    this.uiShop.gameObject.SetActive(gameLogic.playerInShop != null);

    if (gameLogic.playerInShop != null){
      
    }
*/
  }


  private void updatePlayers() {

    if (this.uiPlayer1.gameObject.activeSelf)
    {
      adjustBar(p1Faith.gameObject, this.player1Script.GetFaith()/maxFaith);
      adjustBar(p1Health.gameObject, this.player1Script.GetHealth()/maxHealth);
     // adjustMoney(p1Money.gameObject, player1Script.Money);
            if (this.player1Script.GetFaith() == maxFaith)
            {
                print(0);
                if (!godRageShown)
                {
                    GodRageCanBeactivated.SetActive(true);
                }
                //uiPlayer1GodrageReady.gameObject.SetActive(true);
            } else
            {
                if (godRageShown)
                {
                    GodRageCanBeactivated.SetActive(false);
                    //uiPlayer1GodrageReady.gameObject.SetActive(true);
                }
            }
            NumberOfMoney.text = this.player1Script.Money.ToString();
            HealthPotNumber.text = this.player1Script.numberOfPotions.ToString();
        }

    if (this.uiPlayer2.gameObject.activeSelf)
    {
      adjustBar(p2Faith.gameObject, this.player2Script.GetFaith()/maxFaith);
      adjustBar(p2Health.gameObject, this.player2Script.GetHealth()/maxHealth);
      adjustMoney(p2Money.gameObject, player2Script.Money);
    }

    //adjustFightingSpirit (fightingSpiritButtonUI.gameObject, gameLogic.FightingSpirit);
	}

  private void adjustBar(GameObject uiGo, float pct)
  {
   GetUIElement(uiGo, UI_BAR).gameObject.GetComponent<Image>().fillAmount = pct;
  }

	private void adjustMoney(GameObject uiGo, int money){
		GetUIElement (uiGo, UI_TEXT).gameObject.GetComponent<Text> ().text = money.ToString ();
	}

	private void adjustFightingSpirit(GameObject uiGo, float fightSpirit)
	{
	  //float tempSpirit = (fightSpirit - 100)/(maxFightingSprit - 100);
    //Debug.Log("fightSpirit " + fightSpirit + " tempSpirit " + tempSpirit);
    //Mathf.Lerp(1, maxFightingSprit, )
	  GetUIElement(uiGo, UI_BAR).gameObject.GetComponent<Image>().fillAmount = fightSpirit / maxFightingSprit;
    //GetUIElement(uiGo, UI_TEXT).gameObject.GetComponent<Text> ().text = fightSpirit.ToString();
	}

  #region UI_Animations

  private IEnumerator FadeSprite(SpriteRenderer sprRenderer, float fadeTime = 0.5f) {

    YieldInstruction w8 = new WaitForEndOfFrame();
    float count = 0;

    while (count < fadeTime) {
      //sprRenderer.co
      count += Time.deltaTime;

      yield return w8;
    }

  }

    public void ShowQuest(string comment)
    {
        /*
        if (comment.Length > 200)
        {
            base.tweenQueue.Enqueue(ToggleComment_3(comment, float.MaxValue));
        }
        else if (comment.Length > 100)
        {
            base.tweenQueue.Enqueue(ToggleComment_2(comment, float.MaxValue));
        }
        else {
            base.tweenQueue.Enqueue(ToggleComment_1(comment, float.MaxValue));
        }
        */
        Questtext.text = comment;

    }

    public void ShowHint (string comment)
	{
    if (comment.Length > 200) {
      base.tweenQueue.Enqueue(ToggleComment_3(comment, CommentTime3));
    } else if (comment.Length > 100) {
      base.tweenQueue.Enqueue(ToggleComment_2(comment, CommentTime2));
    } else {
      base.tweenQueue.Enqueue(ToggleComment_1(comment, CommentTime1));
    }

  }

  public void ShowCommmentator(string comment)
  {
	
    //Debug.Log("comment.Length " + comment.Length);
	
    if (comment.Length > 200) {
			base.tweenQueue.Enqueue(ToggleCommentator( CommentTime3));
    } else if (comment.Length > 100){
			base.tweenQueue.Enqueue(ToggleCommentator( CommentTime2));
    } else {
			base.tweenQueue.Enqueue(ToggleCommentator( CommentTime1));
    }


  }

	private IEnumerator ToggleCommentator(float showTime) {
		yield return StartCoroutine(CommentatorTiming(showTime, this.SlideTiming * 0.5f));
	}

  private IEnumerator ToggleComment_1(string comment, float showTime)
  {
    SwitchCommentLines(COMMENT_1);
    GetUIElement(this.uiComments.gameObject, COMMENT_1).GetComponent<Text>().text = comment;

    yield return StartCoroutine(CommentTiming(showTime, this.SlideTiming * 0.5f));
  }

  private IEnumerator ToggleComment_2(string comment, float showTime) {
    SwitchCommentLines(COMMENT_2);
    GetUIElement(this.uiComments.gameObject, COMMENT_2).GetComponent<Text>().text = comment;

    yield return StartCoroutine(CommentTiming(showTime, this.SlideTiming));
  }

  private IEnumerator ToggleComment_3(string comment, float showTime) {
    SwitchCommentLines(COMMENT_3);
    GetUIElement(this.uiComments.gameObject, COMMENT_3).GetComponent<Text>().text = comment;

    yield return StartCoroutine(CommentTiming(showTime, this.SlideTiming));
  }

	private IEnumerator CommentTiming(float showTime, float slideTiming) {
		
		yield return StartCoroutine(SlideCommentUI(true, slideTiming));
		
		yield return new WaitForSeconds(showTime);
		
		yield return StartCoroutine(SlideCommentUI(false, slideTiming));
	}

	private IEnumerator CommentatorTiming(float showTime, float slideTiming) {
		
		yield return StartCoroutine(SlideCommentator(true, slideTiming));
		
		yield return new WaitForSeconds(showTime);
		
		yield return StartCoroutine(SlideCommentator(false, slideTiming));
	}
	
	
  private void SwitchCommentLines(string nameConstant)
  {
    for (int i = 0; i < this.uiComments.childCount; i++)
    {
      if (this.uiComments.GetChild(i).name.Equals(nameConstant)){
        this.uiComments.GetChild(i).gameObject.SetActive(true);
      } else{
        this.uiComments.GetChild(i).gameObject.SetActive(false);
      }
    }
  }

	public IEnumerator SlideCommentator(bool MoveIn, float time) {
		RectTransform rectTrans = uiCommentator.GetComponent<RectTransform> ();

		Vector3 from = uiCommentator.GetComponent<RectTransform> ().anchoredPosition3D;
		Vector3 pos;
		if (MoveIn) {
			pos = CommentatorOriginalPos;
		} else {
			pos = CommentatorOriginalPos + new Vector3(0, this.YOutPos, 0);
		}
		
		//Debug.Log("pos " + pos + " origialpos " + CommentOriginalPos);
		
		if (time <= 0) {
			rectTrans.anchoredPosition3D = pos;
			//backgroundRect.anchoredPosition3D = pos;
		} else {
			iTween.ValueTo(this.gameObject, iTween.Hash(iT.ValueTo.from, from.y,
			                                            iT.ValueTo.to, pos.y,
			                                            iT.ValueTo.time, time,
			                                            iT.ValueTo.onupdate, "SetCommentatorUIPosition",
			                                            iT.ValueTo.easetype, SlideInEaseType));
			yield return new WaitForSeconds(time);
		}
		
		yield return null;
	}

	
	public IEnumerator SlideCommentUI(bool MoveIn, float time) {

    Vector3 from = uiCommentsRect.anchoredPosition3D;
    Vector3 pos;
    if (MoveIn) {
      pos = CommentOriginalPos;
    } else {
      pos = CommentOriginalPos + new Vector3(0, this.YOutPos, 0);
    }

    //Debug.Log("pos " + pos + " origialpos " + CommentOriginalPos);

    if (time <= 0) {
      uiCommentsRect.anchoredPosition3D = pos;
      //backgroundRect.anchoredPosition3D = pos;
    } else {
      iTween.ValueTo(this.gameObject, iTween.Hash(iT.ValueTo.from, from.y,
                                                  iT.ValueTo.to, pos.y,
                                                  iT.ValueTo.time, time,
                                                  iT.ValueTo.onupdate, "SetUICommentPosition",
                                                  iT.ValueTo.easetype, SlideInEaseType));
      yield return new WaitForSeconds(time);
    }

    yield return null;
  }

	protected void SetUICommentPosition(float newValue) {
		uiCommentsRect.anchoredPosition3D = new Vector3(uiCommentsRect.anchoredPosition3D.x,
		                                                newValue,
		                                                uiCommentsRect.anchoredPosition3D.z);
	}

	protected void SetCommentatorUIPosition(float newValue) {
		RectTransform rectTrans = uiCommentator.GetComponent<RectTransform> ();
		rectTrans.anchoredPosition3D = new Vector3(rectTrans.anchoredPosition3D.x,
		                                                newValue,
		                                           rectTrans.anchoredPosition3D.z);
	}
	
	
	public IEnumerator FadeText(float from, float to, float fadeTime, iTween.EaseType easeType) {

    if (fadeTime == 0) {

      setAlpha(to);

      yield return null;
    } else {

      iTween.ValueTo(this.gameObject, iTween.Hash(iT.ValueTo.from, from,
                                          iT.ValueTo.to, to,
                                          iT.ValueTo.time, fadeTime,
                                          iT.ValueTo.onupdate, "setAlpha",
                                          iT.ValueTo.easetype, easeType));

      yield return new WaitForSeconds(fadeTime);
    }
  }

  private void setAlpha(float newAlpha) {
    /*
    Color currentColor;
    if (this.hasMesh) {
      currentColor = this.myMesh.GetComponent<Renderer>().material.color;
      this.myMesh.GetComponent<Renderer>().material.color = new Color(currentColor.r, currentColor.g, currentColor.b, newAlpha);
    }
    if (this.hasGUIText) {
      currentColor = this.myText.color;
      this.myText.color = new Color(currentColor.r, currentColor.g, currentColor.b, newAlpha);
    }
    if (this.hasUIText) {
      currentColor = this.myTextUI.color;
      this.myTextUI.color = new Color(currentColor.r, currentColor.g, currentColor.b, newAlpha);
    }
    */

  }


  #endregion

}
