﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MinorNotificationGUI : MonoBehaviour {

	public Text notificationText;
	public int displayedNotifications = 3;

	private string[] lastNotifications;
	private int elements = 0;

	// Use this for initialization
	void Start () {
		lastNotifications = new string[displayedNotifications];
		this.gameObject.SetActive(false);
	}
	
	public void AddMinorNotification(string notification)
	{
		if (elements == 0)
		{
			this.gameObject.SetActive(true);
			lastNotifications[elements] = notification;
			elements++;
			UpdateView();
		}

		else if (elements < displayedNotifications)
		{
			for (int i = elements; i > 0; i--)
			{
				lastNotifications[i] = lastNotifications[i - 1];
			}
			lastNotifications[0] = notification;
			elements++;
			UpdateView();
		}
		else
		{
			for (int i = displayedNotifications-1; i > 0; i--)
			{
				lastNotifications[i] = lastNotifications[i - 1];
			}
			lastNotifications[0] = notification;
			UpdateView();
		}
	}

	private void UpdateView()
	{
		notificationText.text = "";
		for (int i = 0; i < displayedNotifications; i++)
		{
			notificationText.text += lastNotifications[i] + "\n";
		}
	}
}
