﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class MajorNotificationSprites {

	// Only for naming purposes in inspector
	public string elementName;
	public NotificationSprite spriteName;
	public Sprite sprite;

}
