﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

// Collection of all images 
public enum NotificationSprite
{
	Tutorial_walking,
	Tutorial_hitting,
}

public class MajorNotificationGUI : MonoBehaviour {

	// Public variables
	public Text title;
	public Text description;
	public Image image;

	public MajorNotificationSprites[] sprites;

	public float displaytime = 10.0f;

	void Start()
	{
		this.gameObject.SetActive(false);
	}

	public void ShowNotification(string title, string description, NotificationSprite sprite)
	{
		this.gameObject.SetActive(true);
		this.title.text = title;
		this.description.text = description;
		this.image.sprite = GetSprite(sprite);
		StartCoroutine("Wait");
	}

	private Sprite GetSprite(NotificationSprite sprite)
	{
		for (int i = 0; i < sprites.Length; i++)
		{
			if (sprites[i].spriteName == sprite)
			{
				return sprites[i].sprite;
			}
		}
		return null;
	}

	IEnumerator Wait()
	{
		yield return new WaitForSeconds(displaytime);
		this.gameObject.SetActive(false);
	}
}
