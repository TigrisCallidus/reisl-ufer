using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;

public class iTweenable : MonoBehaviour
{

	public bool isTweening = false;
	public iTween.EaseType standardTween = iTween.EaseType.easeInOutSine;

	protected Queue<IEnumerator> tweenQueue = new Queue<IEnumerator> ();
  private YieldInstruction _tweenQueueInstruction;

    #region iTween_coroutines

	public IEnumerator ValueTo (float from, float to, float animationTime, string onUpdateCallBack, iTween.EaseType easeType = iTween.EaseType.linear, GameObject otherGo = null, iTween.LoopType looptype = iTween.LoopType.none)
	{
		isTweening = true;
		GameObject goToTween = this.gameObject;
		if (otherGo != null) {
			goToTween = otherGo;
		}
		//parameters.Length == 0 ? null : parametersArray

		if (animationTime <= 0) {
			Type thisType = this.GetType ();
			MethodInfo theMethod = thisType.GetMethod (onUpdateCallBack);
			//ParameterInfo[] param = theMethod.GetParameters ();
			System.Object[] param = new object[] { to };
			theMethod.Invoke (this, param);
		} else {

			iTween.ValueTo (goToTween, iTween.Hash (iT.ValueTo.from, from,
                                                    iT.ValueTo.to, to,
                                                    iT.ValueTo.time, animationTime,
                                                    iT.ValueTo.onupdate, onUpdateCallBack,
                                                    iT.ValueTo.easetype, easeType,
			                                        iT.ValueTo.looptype, looptype));

			yield return new WaitForSeconds (animationTime);
		}

		isTweening = false;
	}

	public IEnumerator Fade (float from, float to, float fadeTime, iTween.EaseType easeType = iTween.EaseType.linear, GameObject otherGo = null)
	{
		isTweening = true;
		GameObject goToTween = this.gameObject;
		if (otherGo != null) {
			goToTween = otherGo;
		}
		if (fadeTime <= 0) {
			setAlpha (to);
		} else {

			iTween.ValueTo (goToTween, iTween.Hash (iT.ValueTo.from, from,
                                                  iT.ValueTo.to, to,
                                                  iT.ValueTo.time, fadeTime,
                                                  iT.ValueTo.onupdate, "setAlpha",
                                                  iT.ValueTo.easetype, easeType));

			yield return new WaitForSeconds (fadeTime);
		}

		isTweening = false;
	}

	private void setAlpha (float newAlpha)
	{
		Color currentColor = this.GetComponent<Renderer> ().material.color;
		this.GetComponent<Renderer> ().material.color = new Color (currentColor.r, currentColor.g, currentColor.b, newAlpha);
	}


	public IEnumerator MoveToAnimation (float time, Vector3 toPos, bool local = true, iTween.EaseType easeType = iTween.EaseType.linear, iTween.LoopType loopType = iTween.LoopType.none, GameObject otherGo = null, bool ignoreTimeScale = false)
	{
		isTweening = true;
		GameObject goToTween = this.gameObject;
		if (otherGo != null) {
			goToTween = otherGo;
		}
		iTween.MoveTo (goToTween, iTween.Hash (iT.MoveTo.position, toPos,
                                                 iT.MoveTo.time, time,
                                                 iT.MoveTo.islocal, local,
                                                 iT.MoveTo.looptype, loopType,
                                                 iT.MoveTo.easetype, easeType,
		                                       	"ignoretimescale", ignoreTimeScale));

		yield return new WaitForSeconds (time);

		isTweening = false;
	}

	public IEnumerator MoveToAnimationLookAt (float time, Vector3 toPos, Transform lookAt, Vector3 lookAtVector, bool local = true, iTween.EaseType easeType = iTween.EaseType.linear, iTween.LoopType loopType = iTween.LoopType.none, GameObject otherGo = null, bool ignoreTimeScale = false)
	{
		isTweening = true;
		GameObject goToTween = this.gameObject;
		if (otherGo != null) {
			goToTween = otherGo;
		}
		if (lookAt != null) {
			iTween.MoveTo (goToTween, iTween.Hash (iT.MoveTo.position, toPos,
		                                       iT.MoveTo.time, time,
		                                       iT.MoveTo.islocal, local,
		                                       iT.MoveTo.looktarget, lookAt,
		                                       iT.MoveTo.looktime, time * 0.3f,
		                                       iT.MoveTo.looptype, loopType,
		                                       iT.MoveTo.easetype, easeType,
		                                       "ignoretimescale", ignoreTimeScale));
		} else {
			iTween.MoveTo (goToTween, iTween.Hash (iT.MoveTo.position, toPos,
			                                       iT.MoveTo.time, time,
			                                       iT.MoveTo.islocal, local,
			                                       iT.MoveTo.looktarget, lookAtVector,
			                                       iT.MoveTo.looktime, time * 0.3f,
			                                       iT.MoveTo.looptype, loopType,
			                                       iT.MoveTo.easetype, easeType,
			                                       "ignoretimescale", ignoreTimeScale));
		}
		
		yield return new WaitForSeconds (time);
		
		isTweening = false;
	}

	public IEnumerator RotateToAnimation (float time, Vector3 toRotation, bool local = true, iTween.EaseType easeType = iTween.EaseType.linear, iTween.LoopType loopType = iTween.LoopType.none, GameObject otherGo = null, bool ignoreTimeScale = false)
	{
		isTweening = true;
		GameObject goToTween = this.gameObject;
		if (otherGo != null) {
			goToTween = otherGo;
		}
		iTween.RotateTo (goToTween, iTween.Hash (iT.RotateTo.rotation, toRotation,
                                                   iT.RotateTo.time, time,
                                                   iT.RotateTo.islocal, local,
                                                   iT.RotateTo.looptype, loopType,
                                                   iT.RotateTo.easetype, easeType,
													"ignoretimescale", ignoreTimeScale));

		yield return new WaitForSeconds (time);

		isTweening = false;
	}

	public IEnumerator RotateFromAnimation (float time, Vector3 fromRotation, bool local = true, iTween.EaseType easeType = iTween.EaseType.linear, iTween.LoopType loopType = iTween.LoopType.none, GameObject otherGo = null, bool ignoreTimeScale = false)
	{
		isTweening = true;
		GameObject goToTween = this.gameObject;
		if (otherGo != null) {
			goToTween = otherGo;
		}
		iTween.RotateFrom (goToTween, iTween.Hash (iT.RotateFrom.rotation, fromRotation,
		                                           iT.RotateFrom.time, time,
		                                           iT.RotateFrom.islocal, local,
		                                           iT.RotateFrom.looptype, loopType,
		                                           iT.RotateFrom.easetype, easeType,
		                                         "ignoretimescale", ignoreTimeScale));
		
		yield return new WaitForSeconds (time);
		
		isTweening = false;
	}

	public IEnumerator ScaleToAnimation (float time, Vector3 toScale, iTween.EaseType easeType = iTween.EaseType.linear, iTween.LoopType loopType = iTween.LoopType.none, GameObject otherGo = null, bool ignoreTimeScale = false)
	{
		isTweening = true;
		GameObject goToTween = this.gameObject;
		if (otherGo != null) {
			goToTween = otherGo;
		}
		if (time <= 0) {
			this.transform.localScale = toScale;
			yield return new WaitForEndOfFrame ();
		} else {

			//Debug.Log (this.name + " storyAnimating " + this.storyAnimating + " scale: " + this.transform.localScale);

			iTween.ScaleTo (goToTween, iTween.Hash (iT.ScaleTo.scale, toScale,
                                                  iT.ScaleTo.time, time,
                                                  iT.ScaleTo.looptype, loopType,
                                                  iT.ScaleTo.easetype, easeType,
													"ignoretimescale", ignoreTimeScale));

			yield return new WaitForSeconds (time);
		}

		isTweening = false;
	}

	public IEnumerator ShakeAndScale (float shakeScaleRatio, float time, float strength, Vector3 scaleTo)
	{
		float scaleTime = 1 - shakeScaleRatio;
		float shakeTime = 1 - scaleTime;

		yield return StartCoroutine (ShakeScaleAnimation (shakeTime, new Vector3 (strength, strength, strength)));
		StartCoroutine (ScaleToAnimation (scaleTime, scaleTo));
	}

	public IEnumerator ShakeRotationAnimation (float time, Vector3 shakeAmount, bool loop = false, iTween.LoopType loopType = iTween.LoopType.none, GameObject otherGo = null)
	{
		isTweening = true;
		GameObject goToTween = this.gameObject;
		if (otherGo != null) {
			goToTween = otherGo;
		}
		iTween.ShakeRotation (goToTween, iTween.Hash (iT.ShakeRotation.amount, shakeAmount,
                                                        iT.ShakeRotation.time, time,
                                                        iT.ShakeRotation.looptype, loopType));

		yield return new WaitForSeconds (time);

		isTweening = false;
	}

	public IEnumerator ShakePositionAnimation (float time, Vector3 shakeAmount, iTween.LoopType loopType = iTween.LoopType.none, GameObject otherGo = null)
	{
		isTweening = true;
		GameObject goToTween = this.gameObject;
		if (otherGo != null) {
			goToTween = otherGo;
		}
		iTween.ShakePosition (goToTween, iTween.Hash (iT.ShakePosition.amount, shakeAmount,
                                                        iT.ShakePosition.time, time,
                                                        iT.ShakePosition.looptype, loopType));

		yield return new WaitForSeconds (time);

		isTweening = false;
	}

	public IEnumerator ShakeScaleAnimation (float time, Vector3 shakeAmount, bool loop = false, iTween.LoopType loopType = iTween.LoopType.none, GameObject otherGo = null)
	{
		isTweening = true;
		GameObject goToTween = this.gameObject;
		if (otherGo != null) {
			goToTween = otherGo;
		}
		iTween.ShakeScale (goToTween, iTween.Hash (iT.ShakeScale.amount, shakeAmount,
                                                     iT.ShakeScale.time, time,
                                                     iT.ShakeScale.looptype, loopType));

		yield return new WaitForSeconds (time);

		isTweening = false;
	}

	public IEnumerator WaitForTweenToFinish ()
	{
		while (true) {
			if (!isTweening) {
				break;
			}
			yield return new WaitForEndOfFrame ();
		}

	}

	public static IEnumerator WaitForFrames (int howMayFrames)
	{
		if (howMayFrames > 0) {
			for (int i = 0; i < howMayFrames; i++) {
				yield return new WaitForEndOfFrame ();
			}
		}

	}

	protected virtual IEnumerator CheckTweenQueue ()
	{
    _tweenQueueInstruction = new WaitForSeconds(Time.fixedDeltaTime * 5);

    while (this) {
			if (tweenQueue.Count > 0 && !isTweening) {
				yield return StartCoroutine (tweenQueue.Dequeue ());
			}
      yield return _tweenQueueInstruction;
    }

	}

    #endregion
}

