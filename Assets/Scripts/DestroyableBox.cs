﻿using UnityEngine;
using System.Collections;
using GameLab.HackAndSlashFramework;
using GameLab.NotficationCenter;

public class DestroyableBox : LogicBase, IItem {
	 
	// Use this for initialization
	void Start () {
	
		// collectOnEventType = CollectEventType.OnHit;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI() {
		if (gameLogic.showStats) {
			// GUI.Label( new Rect(0,20,200,20), "H:"+GetHealth (),gui);
			Vector3 screenPos = Camera.main.WorldToScreenPoint (this.transform.position + new Vector3 (0.0f, 2.0f, 0.0f));
			GUI.Label (new Rect (screenPos.x, Screen.height - screenPos.y, 200, 20), GetStateDebug (), guiDebug);
		}
	}

	public void OnCharacterHit(CharacterAttack attack) {

		// version 1:
		// hit on player 

		// version 2:
		// hitted by a player

		//Debug.Log(gameObject.name + " " + attack.GetCharacter().name + " hitted me!");

		// is character i n attack?
		// attack.GetCharacter().type
		//Destroy(gameObject);
		AddHealth ( - attack.GetDamage() * 0.05f);
	}

	public override void OnDie() {

		//AddNotification ("["+gameObject.name+".death]", transform.position , NotificationPriority.Top);
		
		//AddNotification ("[chest.hit]", transform.position , NotificationPriority.Top); 
		AddNotification ("effect.object.destroy", transform.position , NotificationPriority.ThrowAwayAfterProcessing); 
		Destroy (this.gameObject);
		
	}
}
