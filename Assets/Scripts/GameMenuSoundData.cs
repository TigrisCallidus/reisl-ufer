﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.MasterAudio;

[RequireComponent(typeof(AudioSource))]
public class GameMenuSoundData : MonoBehaviour {

	public AudioClip[] SoundClips;

	private AudioSource AS;

	void Start() {
		AS = GetComponent<AudioSource> ();
		AS.spatialBlend = 0;
	}


	public void PlaySound(int i) {
		//Play Soundclip
		//MasterAudio.PlaySound3DAndForget (SoundClips [i].name, Camera.main.transform, false, 1);
		//Debug.Log ("Play sound");
		AS.PlayOneShot (SoundClips [i]);


	}

}