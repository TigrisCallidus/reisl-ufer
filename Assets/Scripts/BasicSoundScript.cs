﻿using UnityEngine;
using System.Collections;

public class BasicSoundScript : MonoBehaviour {

	string activeClipName = "";
	int randomIndex = 0;
	public AudioClip ChosenAudio;

	public AudioClip HitChest;
	public AudioClip HitBody;
	public AudioClip HitBlock;

	public AudioClip HitDestroyable;

	public AudioClip ChangeWeapon;

	public AudioClip FriendDeath;
	public AudioClip EnemyDeath;
	public AudioClip PlayerDeath;

	public AudioClip FastDagger;
	public AudioClip SlowSword;

	public AudioClip FastHalberd;
	public AudioClip SlowHalberd;

	public AudioClip[] FootstepsDirt;
	public AudioClip[] FootstepsWater;

	private float volume= 0.5f;
	private AudioSource source;
	private bool hasStartet=false;

	// Use this for initialization
	void Start () {
	
	}

	void Awake () {
		
		source = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update () {
		if (!source.isPlaying) {
			if (hasStartet) {
				Destroy(this.gameObject);
			} 
		} 
	}

	public void SetSound(string clip){

		// Debug.Log ("[BasicSoundScript].SetSound("+clip+") { NOT FOUND } ");

		// active clip
		activeClipName = clip;

		// switch 
		switch (clip) {

		case "hit.body":
			ChosenAudio=HitBody;

			break; 

		case "hit.block":
			ChosenAudio=HitBlock;
			
			break; 

		case "hit.chest":
			ChosenAudio=HitChest;
			
			break; 
		
		// hit destroy
		case "hit.destroyable":
			ChosenAudio=HitDestroyable;
			break;

		case "weapon.switch":
			ChosenAudio=ChangeWeapon;
			
			break; 

		case "friend.death":
			ChosenAudio=FriendDeath;
			
			break;

		case "enemy.death":
			ChosenAudio=EnemyDeath;
			
			break;

		case "player.death":
			ChosenAudio=PlayerDeath;
			
			break;

		case "fastattack.dagger":
			ChosenAudio=FastDagger;
			
			break;

		case "slowattack.sword":
			ChosenAudio=SlowSword;

			break;


		case "fastattack.halberdfront":
			ChosenAudio=FastHalberd;
			
			break;

		case "slowtattack.halberd":
			ChosenAudio=SlowHalberd;
			
			break;
			
		case "footstep.dirt":
			randomIndex = Random.Range(0, FootstepsDirt.Length);
			ChosenAudio = FootstepsDirt[randomIndex];
			break;

		case "footstep.water":
			randomIndex = Random.Range(0, FootstepsWater.Length);
			ChosenAudio = FootstepsWater[randomIndex];
			break;

			
			// default
		default:
			 Debug.LogError ("[BasicSoundScript].SetSound("+clip+") { NOT FOUND } ");
			break;

		}

	}

	public void Play(){

		if (ChosenAudio == null) {
			Debug.Log ("[BasicSoundScript].SetSound("+activeClipName+") { CLIP NOT FOUND } ");
		}

		if (ChosenAudio != null) {
		
			hasStartet = true;
			source.clip = ChosenAudio;
			source.volume = volume;
			source.Play ();
		
		}
	
	}
}
