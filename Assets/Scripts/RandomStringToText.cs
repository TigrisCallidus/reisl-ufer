﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(Text))]
public class RandomStringToText : MonoBehaviour {

	public List<string> RandomStrings;
	private Text placeholder;
	// Use this for initialization
	void Start () {
		placeholder = GetComponent<Text> ();
		int rand = Random.Range (0, RandomStrings.Count - 1);
		placeholder.text = RandomStrings [rand];
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
