﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameLab.HackAndSlashFramework;
using GameLab.NotficationCenter;

[RequireComponent(typeof(BoxCollider))]
public class ImmovableDamageDealer : LogicBase {

	/// <summary>
	/// The allowed layers.
	/// </summary>
	public List<int> AllowedLayers;

	/// <summary>
	/// The push back force.
	/// </summary>
	public float PushBackForce;

	/// <summary>
	/// The damage it deals per Hit.
	/// </summary>
	public float Damage;

	public float damageRecoveringSpeed = 1f;

	/// <summary>
	/// The collider
	/// </summary>
	//private BoxCollider col;


	/// <summary>
	/// TWether to deal damage OntriggerStay or not.
	/// </summary>
	public bool DealDamageOnStay = false;


	private bool damageDealt = false;

	/// <summary>
	/// The cool down time in seconds.
	/// </summary>
	public float CoolDownTime = 0;

	private float coolDownCounter = 0;
	private ImmovableAttack attack;

	void Start() {
		//col = GetComponent<BoxCollider> ();
	}

	void Update() {
		if (damageDealt) {
			coolDownCounter += Time.deltaTime;
		}

		if (coolDownCounter >= CoolDownTime) {
			coolDownCounter = 0;
			damageDealt = false;
		}
	}

	void PushBack(GameObject victim, Vector3 pushDir) {
		Rigidbody r = victim.GetComponent<Rigidbody> ();

		//Push victim back
		r.AddRelativeForce (pushDir * r.mass * PushBackForce, ForceMode.Impulse);
	}

	void DealDamage(LogicHuman victim) {
		attack.Run (this);
		victim.GetCharacter().OnImmovableDamage(attack);
	}


	void OnCollisionEnter(Collision col) {
	if (!DealDamageOnStay && !damageDealt) {	
			Vector3 victimPos, victimDir, myPos;
			myPos = transform.position;
		
		
			Collider[] AllColliders = GetAllColliders(col.collider);
			foreach(Collider C in AllColliders) {
				if (AllowedLayers.Contains (C.gameObject.layer)) {
					//Check if Collider should take demage
					victimPos = C.gameObject.transform.position;
					victimDir = victimPos - myPos;
					//Debug.Log ("Damage dealt to " + C.gameObject.name);
					PushBack (C.gameObject, victimDir);
					DealDamage (C.gameObject.GetComponent<LogicHuman> ());
					break;
				}
			}
		}
	}

	Collider[] GetAllColliders(Collider col) {
		Collider[] res = col.GetComponentsInChildren<Collider>();
		return res;
	}



	void OnCollisionStay(Collision col) {
		if (DealDamageOnStay) {	
			Vector3 victimPos, victimDir, myPos;
			myPos = transform.position;
			Collider[] AllColliders = GetAllColliders(col.collider);
			foreach(Collider C in AllColliders) {
				if (AllowedLayers.Contains (C.gameObject.layer)) {
					//Check if Collider should take demage
					victimPos = C.gameObject.transform.position;
					victimDir = victimPos - myPos;
					//Debug.Log ("Damage dealt to " + C.gameObject.name);
					PushBack (C.gameObject, victimDir);
					DealDamage (C.gameObject.GetComponent<LogicHuman> ());
					break;
				}
			}
		}
	}

}
